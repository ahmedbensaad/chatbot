var clientConfig = {
    target: 'web',
    entry: './app/client.js',
    output: {
        path: __dirname + "/public/js",
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader"
                    }
                ]
            },
            {
                test: /\.css$/,
                use: ['css-loader']
            }
        ]
    },
    devServer: {
        port: 3000
    },
    devtool: 'inline-source-map'
};

var serverConfig = {
    target: 'node',
    entry: './app/server.js',
    output: {
        filename: 'server.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader"
                    }
                ]
            }
        ]
    },
    devServer: {
        port: 3000
    },
    devtool: 'inline-source-map'
};


module.exports = [clientConfig, serverConfig];