/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/client.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/bubbles.js":
/*!************************!*\
  !*** ./app/bubbles.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// core function
function Bubbles(container, self, options) {
  // options
  options = typeof options !== "undefined" ? options : {};
  animationTime = options.animationTime || 200; // how long it takes to animate chat bubble, also set in CSS
  typeSpeed = options.typeSpeed || 5; // delay per character, to simulate the machine "typing"
  widerBy = options.widerBy || 2; // add a little extra width to bubbles to make sure they don't break
  sidePadding = options.sidePadding || 10; // padding on both sides of chat bubbles
  recallInteractions = options.recallInteractions || 0; // number of interactions to be remembered and brought back upon restart
  inputCallbackFn = options.inputCallbackFn || false; // should we display an input field?

  var standingAnswer = "start"; // remember where to restart senario if interrupted

  var _senario = {}; // local memory for conversation JSON object
  //--> NOTE that this object is only assigned once, per session and does not change for this
  // 		constructor name during open session.

  // local storage for recalling conversations upon restart
  var localStorageCheck = function () {
    var test = "chat-bubble-storage-test";
    try {
      localStorage.setItem(test, test);
      localStorage.removeItem(test);
      return true;
    } catch (error) {
      console.error("Your server does not allow storing data locally. Most likely it's because you've opened this page from your hard-drive. For testing you can disable your browser's security or start a localhost environment.");
      return false;
    }
  };
  var localStorageAvailable = localStorageCheck() && recallInteractions > 0;
  var interactionsLS = "chat-bubble-interactions";
  var interactionsHistory = localStorageAvailable && JSON.parse(localStorage.getItem(interactionsLS)) || [];

  // prepare next save point
  interactionsSave = function (say, reply) {
    if (!localStorageAvailable) return;
    // limit number of saves
    if (interactionsHistory.length > recallInteractions) interactionsHistory.shift(); // removes the oldest (first) save to make space

    // do not memorize buttons; only user input gets memorized:
    if (
    // `bubble-button` class name signals that it's a button
    say.includes("bubble-button") &&
    // if it is not of a type of textual reply
    reply !== "reply reply-freeform" &&
    // if it is not of a type of textual reply or memorized user choice
    reply !== "reply reply-pick")
      // ...it shan't be memorized
      return;

    // save to memory
    interactionsHistory.push({ say: say, reply: reply });
  };

  // commit save to localStorage
  interactionsSaveCommit = function () {
    if (!localStorageAvailable) return;
    localStorage.setItem(interactionsLS, JSON.stringify(interactionsHistory));
  };

  // set up the stage
  container.classList.add("bubble-container");
  var bubbleWrap = document.createElement("div");
  bubbleWrap.className = "bubble-wrap";
  container.appendChild(bubbleWrap);

  // install user input textfield
  this.typeInput = function (callbackFn) {
    var inputWrap = document.createElement("div");
    inputWrap.className = "input-wrap";
    var inputText = document.createElement("textarea");
    inputText.setAttribute("placeholder", "Ask me anything...");
    inputWrap.appendChild(inputText);
    inputText.addEventListener("keypress", function (e) {
      // register user input
      if (e.keyCode == 13) {
        e.preventDefault();
        typeof bubbleQueue !== false ? clearTimeout(bubbleQueue) : false; // allow user to interrupt the bot
        var lastBubble = document.querySelectorAll(".bubble.say");
        lastBubble = lastBubble[lastBubble.length - 1];
        lastBubble.classList.contains("reply") && !lastBubble.classList.contains("reply-freeform") ? lastBubble.classList.add("bubble-hidden") : false;
        addBubble('<span class="bubble-button bubble-pick">' + this.value + "</span>", function () {}, "reply reply-freeform");
        // callback
        typeof callbackFn === "function" ? callbackFn({
          input: this.value,
          senario: _senario,
          standingAnswer: standingAnswer
        }) : false;
        this.value = "";
      }
    });
    container.appendChild(inputWrap);
    bubbleWrap.style.paddingBottom = "100px";
    inputText.focus();
  };
  inputCallbackFn ? this.typeInput(inputCallbackFn) : false;

  // init typing bubble
  var bubbleTyping = document.createElement("div");
  bubbleTyping.className = "bubble-typing imagine";
  for (dots = 0; dots < 3; dots++) {
    var dot = document.createElement("div");
    dot.className = "dot_" + dots + " dot";
    bubbleTyping.appendChild(dot);
  }
  bubbleWrap.appendChild(bubbleTyping);

  // accept JSON & create bubbles
  this.talk = function (senario, here) {
    // all further .talk() calls will append the conversation with additional blocks defined in senario parameter
    _senario = Object.assign(_senario, senario); // POLYFILL REQUIRED FOR OLDER BROWSERS

    this.reply(_senario[here]);
    here ? standingAnswer = here : false;
  };

  var iceBreaker = false; // this variable holds answer to whether this is the initative bot interaction or not
  this.reply = function (turn) {
    iceBreaker = typeof turn === "undefined";
    turn = !iceBreaker ? turn : _senario.start;
    questionsHTML = "";
    if (!turn) return;
    if (turn.reply !== undefined) {
      turn.reply.reverse();
      for (var i = 0; i < turn.reply.length; i++) {
        ;(function (el, count) {
          questionsHTML += '<span class="bubble-button" style="animation-delay: ' + animationTime / 2 * count + 'ms" onClick="' + self + ".answer('" + el.answer + "', '" + el.question + "');this.classList.add('bubble-pick')\">" + el.question + "</span>";
        })(turn.reply[i], i);
      }
    }
    orderBubbles(turn.says, function () {
      bubbleTyping.classList.remove("imagine");
      questionsHTML !== "" ? addBubble(questionsHTML, function () {}, "reply") : bubbleTyping.classList.add("imagine");
    });
  };
  // navigate "answers"
  this.answer = function (key, content) {
    var func = function (key) {
      typeof window[key] === "function" ? window[key]() : false;
    };
    _senario[key] !== undefined ? (this.reply(_senario[key]), standingAnswer = key) : func(key);

    // add re-generated user picks to the history stack
    if (_senario[key] !== undefined && content !== undefined) {
      interactionsSave('<span class="bubble-button reply-pick">' + content + "</span>", "reply reply-pick");
    }
  };

  // api for typing bubble
  this.think = function () {
    bubbleTyping.classList.remove("imagine");
    this.stop = function () {
      bubbleTyping.classList.add("imagine");
    };
  };

  // "type" each message within the group
  var orderBubbles = function (q, callback) {
    var start = function () {
      setTimeout(function () {
        callback();
      }, animationTime);
    };
    var position = 0;
    for (var nextCallback = position + q.length - 1; nextCallback >= position; nextCallback--) {
      ;(function (callback, index) {
        start = function () {
          addBubble(q[index], callback);
        };
      })(start, nextCallback);
    }
    start();
  };

  // create a bubble
  var bubbleQueue = false;
  var addBubble = function (say, posted, reply, live) {
    reply = typeof reply !== "undefined" ? reply : "";
    live = typeof live !== "undefined" ? live : true; // bubbles that are not "live" are not animated and displayed differently
    var animationTime = live ? this.animationTime : 0;
    var typeSpeed = live ? this.typeSpeed : 0;
    // create bubble element
    var bubble = document.createElement("div");
    var bubbleContent = document.createElement("span");
    bubble.className = "bubble imagine " + (!live ? " history " : "") + reply;
    bubbleContent.className = "bubble-content";
    bubbleContent.innerHTML = say;
    bubble.appendChild(bubbleContent);
    bubbleWrap.insertBefore(bubble, bubbleTyping);
    // answer picker styles
    if (reply !== "") {
      var bubbleButtons = bubbleContent.querySelectorAll(".bubble-button");
      for (var z = 0; z < bubbleButtons.length; z++) {
        ;(function (el) {
          if (!el.parentNode.parentNode.classList.contains("reply-freeform")) el.style.width = el.offsetWidth - sidePadding * 2 + widerBy + "px";
        })(bubbleButtons[z]);
      }
      bubble.addEventListener("click", function () {
        for (var i = 0; i < bubbleButtons.length; i++) {
          ;(function (el) {
            el.style.width = 0 + "px";
            el.classList.contains("bubble-pick") ? el.style.width = "" : false;
            el.removeAttribute("onclick");
          })(bubbleButtons[i]);
        }
        this.classList.add("bubble-picked");
      });
    }
    // time, size & animate
    wait = live ? animationTime * 2 : 0;
    minTypingWait = live ? animationTime * 6 : 0;
    if (say.length * typeSpeed > animationTime && reply == "") {
      wait += typeSpeed * say.length;
      wait < minTypingWait ? wait = minTypingWait : false;
      setTimeout(function () {
        bubbleTyping.classList.remove("imagine");
      }, animationTime);
    }
    live && setTimeout(function () {
      bubbleTyping.classList.add("imagine");
    }, wait - animationTime * 2);
    bubbleQueue = setTimeout(function () {
      bubble.classList.remove("imagine");
      var bubbleWidthCalc = bubbleContent.offsetWidth + widerBy + "px";
      bubble.style.width = reply == "" ? bubbleWidthCalc : "";
      bubble.style.width = say.includes("<img src=") ? "50%" : bubble.style.width;
      bubble.classList.add("say");
      posted();

      // save the interaction
      interactionsSave(say, reply);
      !iceBreaker && interactionsSaveCommit(); // save point

      // animate scrolling
      containerHeight = container.offsetHeight;
      scrollDifference = bubbleWrap.scrollHeight - bubbleWrap.scrollTop;
      scrollHop = scrollDifference / 200;
      var scrollBubbles = function () {
        for (var i = 1; i <= scrollDifference / scrollHop; i++) {
          ;(function () {
            setTimeout(function () {
              bubbleWrap.scrollHeight - bubbleWrap.scrollTop > containerHeight ? bubbleWrap.scrollTop = bubbleWrap.scrollTop + scrollHop : false;
            }, i * 5);
          })();
        }
      };
      setTimeout(scrollBubbles, animationTime / 2);
    }, wait + animationTime * 2);
  };

  // recall previous interactions
  for (var i = 0; i < interactionsHistory.length; i++) {
    addBubble(interactionsHistory[i].say, function () {}, interactionsHistory[i].reply, false);
  }
}

// below functions are specifically for WebPack-type project that work with import()

// this function automatically adds all HTML and CSS necessary for chat-bubble to function
function prepHTML(options) {
  // options
  var options = typeof options !== "undefined" ? options : {};
  var container = options.container || "chat"; // id of the container HTML element
  var relative_path = options.relative_path || "./";

  // make HTML container element
  window[container] = document.createElement("div");
  window[container].setAttribute("id", container);
  document.body.appendChild(window[container]);
}

// exports for es6
if (true) {
  exports.Bubbles = Bubbles;
  exports.prepHTML = prepHTML;
}

/***/ }),

/***/ "./app/client.js":
/*!***********************!*\
  !*** ./app/client.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _bubbles_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bubbles.js */ "./app/bubbles.js");
/* harmony import */ var _bubbles_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_bubbles_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _senario_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./senario.js */ "./app/senario.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/************************************************************************/
/******* CONVENIENCE METHODS AVAILABLE FOR ES6 BUILD ENVIRONMENTS *******/
/************************************************************************/

// the URL of where you've installed the component; you may need to change this:





// this is a convenience script that builds all necessary HTML,
// imports all scripts and stylesheets; your container DIV will
// have a default `id="chat"`;
// you can specify a different ID with:
// `container: "my_chatbox_id"` option
Object(_bubbles_js__WEBPACK_IMPORTED_MODULE_0__["prepHTML"])({ relative_path: "./" });

localStorage.setItem("chatSession", false);
console.log(localStorage.getItem('chatSession'));
var BOT_USER = "<strong >Bot</strong >: ";

/************************************************************************/
/********************** NGP chatbot Implementation **********************/
/************************************************************************/

var chatWindow = new _bubbles_js__WEBPACK_IMPORTED_MODULE_0__["Bubbles"](document.getElementById("chat"), "chatWindow", {
    // the one that we care about is inputCallbackFn()
    // this function returns an object with some data that we can process from user input
    // and understand the context of it
    // this is an example function that matches the text user typed to one of the answer bubbles
    // this function does no natural language processing
    // this is where you may want to connect this script to NLC backend.
    inputCallbackFn: function (o) {
        if (localStorage.getItem("chatSession") == "false") {
            // if no live session is on
            // add error conversation block & recall it if no answer matched
            var miss = function () {
                let replies = o.senario[o.standingAnswer].reply;
                console.log(replies);
                if (replies == false) replies = [{
                    question: "Live Agent",
                    answer: "liveagent"
                }];
                chatWindow.talk({
                    "i-dont-get-it": {
                        says: [BOT_USER + "Sorry, I don't get it 😕. type \"<strong>Live Agent</strong>\" if you want to chat with a human 👨"],
                        reply: replies
                    }
                }, "i-dont-get-it");
            };
            // do this if answer found
            var match = function (key) {
                setTimeout(function () {
                    chatWindow.talk(_senario_js__WEBPACK_IMPORTED_MODULE_1__["default"], key); // restart current senario from point found in the answer
                }, 600);
            };
            // sanitize text for search function
            var strip = function (text) {
                return text.toLowerCase().replace(/[\s.,\/#!$%\^&\*;:{}=\-_'"`~()]/g, "");
            };
            // search function
            var found = false;
            o.senario[o.standingAnswer].reply.forEach(function (e, i) {
                strip(e.question).includes(strip(o.input)) && o.input.length > 0 ? found = e.answer : found ? null : found = false;
            });
            found ? match(found) : miss();
        } else {
            sendMessage(o.input);
        }
    }
}); // done setting up chat-bubble

window.chatWindow = chatWindow;

window.liveagent = function () {
    initSession();
    chatWindow.talk(_senario_js__WEBPACK_IMPORTED_MODULE_1__["default"], "connectToLiveAgent"); // the conversation can be easily restarted from here.
    console.log("Connecting...");
};

function initSession() {
    axios__WEBPACK_IMPORTED_MODULE_2___default.a.get("/initSession").then(response => {
        let data = response.data;
        if (data.response.statusCode == 200) {
            console.log('body', data.response.body);
            localStorage.setItem('sessionId', data.response.body.id);
            localStorage.setItem('sessionKey', data.response.body.key);
            localStorage.setItem('sessionAffinityToken', data.response.body.affinityToken);
        } else {
            console.log('error', data.response.error);
        }
    }).then(() => {
        console.log(localStorage.getItem('sessionId'));
        axios__WEBPACK_IMPORTED_MODULE_2___default.a.post("/initChatSession", {
            sessionId: localStorage.getItem('sessionId'),
            sessionKey: localStorage.getItem('sessionKey'),
            sessionAffinityToken: localStorage.getItem('sessionAffinityToken')
        }).then(response => {
            if (response.data.response.body == "OK") {
                console.log("Live Agent chat session created");
                localStorage.setItem("chatSession", true);
            }
        });
    }).then(() => {
        pullMessages();
    }).catch(error => {
        console.log(error);
    });
}

/**
 * Send Chat Message
 */
function sendMessage(message) {
    axios__WEBPACK_IMPORTED_MODULE_2___default.a.post("/sendChatMessage", {
        message: message,
        sessionKey: localStorage.getItem('sessionKey'),
        sessionAffinityToken: localStorage.getItem('sessionAffinityToken')
    }).then(response => {
        console.log(response.data.response);
        if (response.data.response.body == "OK") {
            console.log("Chat message sent");
        }
    });
}

/**
 * Pulling messages
 */
function pullMessages() {
    axios__WEBPACK_IMPORTED_MODULE_2___default.a.get("/getChatMessages", {
        params: {
            sessionKey: localStorage.getItem('sessionKey'),
            sessionAffinityToken: localStorage.getItem('sessionAffinityToken')
        }
    }).then(response => {
        if (response.error) {
            initSession();
            return;
        }
        console.log("getChatMessages", response.data);
        let messages = [];
        try {
            messages = response.data.response.body.messages;
        } catch (e) {
            console.log(e.message);
        }
        console.log(messages);
        if (messages && messages.length > 0) {
            _senario_js__WEBPACK_IMPORTED_MODULE_1__["default"].liveAgentMessage = { says: [], reply: [] };
            messages.forEach(item => {
                if (item.type == "ChatEstablished") {
                    _senario_js__WEBPACK_IMPORTED_MODULE_1__["default"].liveAgentMessage.says.push(BOT_USER + "Live Chat 💬 established with <strong>" + item.message.name + "</strong>");
                } else if (item.type == "ChatMessage") {
                    _senario_js__WEBPACK_IMPORTED_MODULE_1__["default"].liveAgentMessage.says.push("<strong>" + item.message.name + "</strong> : " + item.message.text);
                } else if (item.type == "ChatEnded") {
                    _senario_js__WEBPACK_IMPORTED_MODULE_1__["default"].liveAgentMessage.says.push(BOT_USER + "Live Chat 💬 ended ❌");
                    localStorage.setItem("chatSession", false);
                } else if (item.type == "ChatRequestFail") {
                    initSession();
                    return;
                }
            });
            chatWindow.talk(_senario_js__WEBPACK_IMPORTED_MODULE_1__["default"], "liveAgentMessage");
        }

        pullMessages();
    });
}

// pass JSON to your function and you're done!
chatWindow.talk(_senario_js__WEBPACK_IMPORTED_MODULE_1__["default"]);

/***/ }),

/***/ "./app/senario.js":
/*!************************!*\
  !*** ./app/senario.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var BOT_USER = "<strong >Bot</strong >: ";

let senario = {
    start: {
        says: [BOT_USER + "Hi", BOT_USER + "Nice project 👍. Do you already know how to finance it?"],
        reply: [{
            question: "Yes, Thank you",
            answer: "askAnything"
        }, {
            question: "Not Really",
            answer: "notReally"
        }]
    },
    askAnything: {
        says: [BOT_USER + "Ok, Is there anything else I can do for you?"],
        reply: []
    },
    notReally: {
        says: [BOT_USER + "Do you want me to help you?"],
        reply: [{
            question: "Yes",
            answer: "yesHelpMe"
        }, {
            question: "No",
            answer: "askAnything"
        }]
    },
    yesHelpMe: {
        says: [BOT_USER + "Thank you! We can propose you a loan for the total value of you travel.", "For the 3 000€ you need, we can propose you a global rate of 4%", "255€ of monthly repayment", "a total cost of 64€", "or if you are ready to raise the challenge, we can offer you a tremendous discount.", "What are you up to : \"Direct offer\" or \"Challenge\"?"],
        reply: [{
            question: "Direct offer",
            answer: "directOffer"
        }, {
            question: "Challenge",
            answer: "challenge"
        }]
    },
    challenge: {
        says: [BOT_USER + "Great 👌", "Your mission shall you accept it, is to save 1 000€ in the next 6 months. Are you ready?"],
        reply: [{
            question: "Yes",
            answer: "yesAcceptChallengeOffer"
        }, {
            question: "No",
            answer: "askAnything"
        }]
    },
    yesAcceptChallengeOffer: {
        says: [BOT_USER + "Cool! I see that you have already 100€ available right now. Do you want me to spare them now?"],
        reply: [{
            question: "Yes",
            answer: "yesSave"
        }, {
            question: "No",
            answer: "askAnything"
        }]
    },
    yesSave: {
        says: [BOT_USER + "Ok done. Already 10% of your challenge completed... Continue the good job 👍", "Is there anything else I can do for you?"],
        reply: []
    },
    connectToLiveAgent: {
        says: [BOT_USER + "Please wait ⌛ while connecting you to a human 📡"],
        reply: []
    }
};

/* harmony default export */ __webpack_exports__["default"] = (senario);

/***/ }),

/***/ "./node_modules/axios/index.js":
/*!*************************************!*\
  !*** ./node_modules/axios/index.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./lib/axios */ "./node_modules/axios/lib/axios.js");

/***/ }),

/***/ "./node_modules/axios/lib/adapters/xhr.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/adapters/xhr.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var settle = __webpack_require__(/*! ./../core/settle */ "./node_modules/axios/lib/core/settle.js");
var buildURL = __webpack_require__(/*! ./../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");
var parseHeaders = __webpack_require__(/*! ./../helpers/parseHeaders */ "./node_modules/axios/lib/helpers/parseHeaders.js");
var isURLSameOrigin = __webpack_require__(/*! ./../helpers/isURLSameOrigin */ "./node_modules/axios/lib/helpers/isURLSameOrigin.js");
var createError = __webpack_require__(/*! ../core/createError */ "./node_modules/axios/lib/core/createError.js");
var btoa = (typeof window !== 'undefined' && window.btoa && window.btoa.bind(window)) || __webpack_require__(/*! ./../helpers/btoa */ "./node_modules/axios/lib/helpers/btoa.js");

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();
    var loadEvent = 'onreadystatechange';
    var xDomain = false;

    // For IE 8/9 CORS support
    // Only supports POST and GET calls and doesn't returns the response headers.
    // DON'T do this for testing b/c XMLHttpRequest is mocked, not XDomainRequest.
    if ("development" !== 'test' &&
        typeof window !== 'undefined' &&
        window.XDomainRequest && !('withCredentials' in request) &&
        !isURLSameOrigin(config.url)) {
      request = new window.XDomainRequest();
      loadEvent = 'onload';
      xDomain = true;
      request.onprogress = function handleProgress() {};
      request.ontimeout = function handleTimeout() {};
    }

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request[loadEvent] = function handleLoad() {
      if (!request || (request.readyState !== 4 && !xDomain)) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        // IE sends 1223 instead of 204 (https://github.com/axios/axios/issues/201)
        status: request.status === 1223 ? 204 : request.status,
        statusText: request.status === 1223 ? 'No Content' : request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED',
        request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__(/*! ./../helpers/cookies */ "./node_modules/axios/lib/helpers/cookies.js");

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ?
          cookies.read(config.xsrfCookieName) :
          undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (config.withCredentials) {
      request.withCredentials = true;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/axios.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/axios.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");
var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");
var Axios = __webpack_require__(/*! ./core/Axios */ "./node_modules/axios/lib/core/Axios.js");
var defaults = __webpack_require__(/*! ./defaults */ "./node_modules/axios/lib/defaults.js");

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(utils.merge(defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__(/*! ./cancel/Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");
axios.CancelToken = __webpack_require__(/*! ./cancel/CancelToken */ "./node_modules/axios/lib/cancel/CancelToken.js");
axios.isCancel = __webpack_require__(/*! ./cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__(/*! ./helpers/spread */ "./node_modules/axios/lib/helpers/spread.js");

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/Cancel.js":
/*!*************************************************!*\
  !*** ./node_modules/axios/lib/cancel/Cancel.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/CancelToken.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/cancel/CancelToken.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__(/*! ./Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/isCancel.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/cancel/isCancel.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};


/***/ }),

/***/ "./node_modules/axios/lib/core/Axios.js":
/*!**********************************************!*\
  !*** ./node_modules/axios/lib/core/Axios.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var defaults = __webpack_require__(/*! ./../defaults */ "./node_modules/axios/lib/defaults.js");
var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var InterceptorManager = __webpack_require__(/*! ./InterceptorManager */ "./node_modules/axios/lib/core/InterceptorManager.js");
var dispatchRequest = __webpack_require__(/*! ./dispatchRequest */ "./node_modules/axios/lib/core/dispatchRequest.js");

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = utils.merge({
      url: arguments[0]
    }, arguments[1]);
  }

  config = utils.merge(defaults, {method: 'get'}, this.defaults, config);
  config.method = config.method.toLowerCase();

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;


/***/ }),

/***/ "./node_modules/axios/lib/core/InterceptorManager.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/core/InterceptorManager.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;


/***/ }),

/***/ "./node_modules/axios/lib/core/createError.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/createError.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__(/*! ./enhanceError */ "./node_modules/axios/lib/core/enhanceError.js");

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};


/***/ }),

/***/ "./node_modules/axios/lib/core/dispatchRequest.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/core/dispatchRequest.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var transformData = __webpack_require__(/*! ./transformData */ "./node_modules/axios/lib/core/transformData.js");
var isCancel = __webpack_require__(/*! ../cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");
var defaults = __webpack_require__(/*! ../defaults */ "./node_modules/axios/lib/defaults.js");
var isAbsoluteURL = __webpack_require__(/*! ./../helpers/isAbsoluteURL */ "./node_modules/axios/lib/helpers/isAbsoluteURL.js");
var combineURLs = __webpack_require__(/*! ./../helpers/combineURLs */ "./node_modules/axios/lib/helpers/combineURLs.js");

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Support baseURL config
  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  }

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers || {}
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/core/enhanceError.js":
/*!*****************************************************!*\
  !*** ./node_modules/axios/lib/core/enhanceError.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }
  error.request = request;
  error.response = response;
  return error;
};


/***/ }),

/***/ "./node_modules/axios/lib/core/settle.js":
/*!***********************************************!*\
  !*** ./node_modules/axios/lib/core/settle.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__(/*! ./createError */ "./node_modules/axios/lib/core/createError.js");

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  // Note: status is not exposed by XDomainRequest
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response.request,
      response
    ));
  }
};


/***/ }),

/***/ "./node_modules/axios/lib/core/transformData.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/core/transformData.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};


/***/ }),

/***/ "./node_modules/axios/lib/defaults.js":
/*!********************************************!*\
  !*** ./node_modules/axios/lib/defaults.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");
var normalizeHeaderName = __webpack_require__(/*! ./helpers/normalizeHeaderName */ "./node_modules/axios/lib/helpers/normalizeHeaderName.js");

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__(/*! ./adapters/xhr */ "./node_modules/axios/lib/adapters/xhr.js");
  } else if (typeof process !== 'undefined') {
    // For node use HTTP adapter
    adapter = __webpack_require__(/*! ./adapters/http */ "./node_modules/axios/lib/adapters/xhr.js");
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/axios/lib/helpers/bind.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/helpers/bind.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/btoa.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/helpers/btoa.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// btoa polyfill for IE<10 courtesy https://github.com/davidchambers/Base64.js

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function E() {
  this.message = 'String contains an invalid character';
}
E.prototype = new Error;
E.prototype.code = 5;
E.prototype.name = 'InvalidCharacterError';

function btoa(input) {
  var str = String(input);
  var output = '';
  for (
    // initialize result and counter
    var block, charCode, idx = 0, map = chars;
    // if the next str index does not exist:
    //   change the mapping table to "="
    //   check if d has no fractional digits
    str.charAt(idx | 0) || (map = '=', idx % 1);
    // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
    output += map.charAt(63 & block >> 8 - idx % 1 * 8)
  ) {
    charCode = str.charCodeAt(idx += 3 / 4);
    if (charCode > 0xFF) {
      throw new E();
    }
    block = block << 8 | charCode;
  }
  return output;
}

module.exports = btoa;


/***/ }),

/***/ "./node_modules/axios/lib/helpers/buildURL.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/helpers/buildURL.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/combineURLs.js":
/*!*******************************************************!*\
  !*** ./node_modules/axios/lib/helpers/combineURLs.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/cookies.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/helpers/cookies.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
  (function standardBrowserEnv() {
    return {
      write: function write(name, value, expires, path, domain, secure) {
        var cookie = [];
        cookie.push(name + '=' + encodeURIComponent(value));

        if (utils.isNumber(expires)) {
          cookie.push('expires=' + new Date(expires).toGMTString());
        }

        if (utils.isString(path)) {
          cookie.push('path=' + path);
        }

        if (utils.isString(domain)) {
          cookie.push('domain=' + domain);
        }

        if (secure === true) {
          cookie.push('secure');
        }

        document.cookie = cookie.join('; ');
      },

      read: function read(name) {
        var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
        return (match ? decodeURIComponent(match[3]) : null);
      },

      remove: function remove(name) {
        this.write(name, '', Date.now() - 86400000);
      }
    };
  })() :

  // Non standard browser env (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return {
      write: function write() {},
      read: function read() { return null; },
      remove: function remove() {}
    };
  })()
);


/***/ }),

/***/ "./node_modules/axios/lib/helpers/isAbsoluteURL.js":
/*!*********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isAbsoluteURL.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/isURLSameOrigin.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isURLSameOrigin.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
  (function standardBrowserEnv() {
    var msie = /(msie|trident)/i.test(navigator.userAgent);
    var urlParsingNode = document.createElement('a');
    var originURL;

    /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
    function resolveURL(url) {
      var href = url;

      if (msie) {
        // IE needs attribute set twice to normalize properties
        urlParsingNode.setAttribute('href', href);
        href = urlParsingNode.href;
      }

      urlParsingNode.setAttribute('href', href);

      // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
      return {
        href: urlParsingNode.href,
        protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
        host: urlParsingNode.host,
        search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
        hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
        hostname: urlParsingNode.hostname,
        port: urlParsingNode.port,
        pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
                  urlParsingNode.pathname :
                  '/' + urlParsingNode.pathname
      };
    }

    originURL = resolveURL(window.location.href);

    /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
    return function isURLSameOrigin(requestURL) {
      var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
      return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
    };
  })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return function isURLSameOrigin() {
      return true;
    };
  })()
);


/***/ }),

/***/ "./node_modules/axios/lib/helpers/normalizeHeaderName.js":
/*!***************************************************************!*\
  !*** ./node_modules/axios/lib/helpers/normalizeHeaderName.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/parseHeaders.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/parseHeaders.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = [
  'age', 'authorization', 'content-length', 'content-type', 'etag',
  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
  'referer', 'retry-after', 'user-agent'
];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/spread.js":
/*!**************************************************!*\
  !*** ./node_modules/axios/lib/helpers/spread.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};


/***/ }),

/***/ "./node_modules/axios/lib/utils.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/utils.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");
var isBuffer = __webpack_require__(/*! is-buffer */ "./node_modules/is-buffer/index.js");

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    return false;
  }
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  extend: extend,
  trim: trim
};


/***/ }),

/***/ "./node_modules/is-buffer/index.js":
/*!*****************************************!*\
  !*** ./node_modules/is-buffer/index.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */

// The _isBuffer check is for Safari 5-7 support, because it's missing
// Object.prototype.constructor. Remove this eventually
module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)
}

function isBuffer (obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

// For Node v0.10 support. Remove this eventually.
function isSlowBuffer (obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))
}


/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYXBwL2J1YmJsZXMuanMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2NsaWVudC5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvc2VuYXJpby5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9hZGFwdGVycy94aHIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9heGlvcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2NhbmNlbC9DYW5jZWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jYW5jZWwvQ2FuY2VsVG9rZW4uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jYW5jZWwvaXNDYW5jZWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jb3JlL0F4aW9zLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9JbnRlcmNlcHRvck1hbmFnZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jb3JlL2NyZWF0ZUVycm9yLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9kaXNwYXRjaFJlcXVlc3QuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jb3JlL2VuaGFuY2VFcnJvci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2NvcmUvc2V0dGxlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS90cmFuc2Zvcm1EYXRhLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvZGVmYXVsdHMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2JpbmQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2J0b2EuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2J1aWxkVVJMLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9jb21iaW5lVVJMcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvY29va2llcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvaXNBYnNvbHV0ZVVSTC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvaXNVUkxTYW1lT3JpZ2luLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9ub3JtYWxpemVIZWFkZXJOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9wYXJzZUhlYWRlcnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL3NwcmVhZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL3V0aWxzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9pcy1idWZmZXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Byb2Nlc3MvYnJvd3Nlci5qcyJdLCJuYW1lcyI6WyJCdWJibGVzIiwiY29udGFpbmVyIiwic2VsZiIsIm9wdGlvbnMiLCJhbmltYXRpb25UaW1lIiwidHlwZVNwZWVkIiwid2lkZXJCeSIsInNpZGVQYWRkaW5nIiwicmVjYWxsSW50ZXJhY3Rpb25zIiwiaW5wdXRDYWxsYmFja0ZuIiwic3RhbmRpbmdBbnN3ZXIiLCJfc2VuYXJpbyIsImxvY2FsU3RvcmFnZUNoZWNrIiwidGVzdCIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJyZW1vdmVJdGVtIiwiZXJyb3IiLCJjb25zb2xlIiwibG9jYWxTdG9yYWdlQXZhaWxhYmxlIiwiaW50ZXJhY3Rpb25zTFMiLCJpbnRlcmFjdGlvbnNIaXN0b3J5IiwiSlNPTiIsInBhcnNlIiwiZ2V0SXRlbSIsImludGVyYWN0aW9uc1NhdmUiLCJzYXkiLCJyZXBseSIsImxlbmd0aCIsInNoaWZ0IiwiaW5jbHVkZXMiLCJwdXNoIiwiaW50ZXJhY3Rpb25zU2F2ZUNvbW1pdCIsInN0cmluZ2lmeSIsImNsYXNzTGlzdCIsImFkZCIsImJ1YmJsZVdyYXAiLCJkb2N1bWVudCIsImNyZWF0ZUVsZW1lbnQiLCJjbGFzc05hbWUiLCJhcHBlbmRDaGlsZCIsInR5cGVJbnB1dCIsImNhbGxiYWNrRm4iLCJpbnB1dFdyYXAiLCJpbnB1dFRleHQiLCJzZXRBdHRyaWJ1dGUiLCJhZGRFdmVudExpc3RlbmVyIiwiZSIsImtleUNvZGUiLCJwcmV2ZW50RGVmYXVsdCIsImJ1YmJsZVF1ZXVlIiwiY2xlYXJUaW1lb3V0IiwibGFzdEJ1YmJsZSIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJjb250YWlucyIsImFkZEJ1YmJsZSIsInZhbHVlIiwiaW5wdXQiLCJzZW5hcmlvIiwic3R5bGUiLCJwYWRkaW5nQm90dG9tIiwiZm9jdXMiLCJidWJibGVUeXBpbmciLCJkb3RzIiwiZG90IiwidGFsayIsImhlcmUiLCJPYmplY3QiLCJhc3NpZ24iLCJpY2VCcmVha2VyIiwidHVybiIsInN0YXJ0IiwicXVlc3Rpb25zSFRNTCIsInVuZGVmaW5lZCIsInJldmVyc2UiLCJpIiwiZWwiLCJjb3VudCIsImFuc3dlciIsInF1ZXN0aW9uIiwib3JkZXJCdWJibGVzIiwic2F5cyIsInJlbW92ZSIsImtleSIsImNvbnRlbnQiLCJmdW5jIiwid2luZG93IiwidGhpbmsiLCJzdG9wIiwicSIsImNhbGxiYWNrIiwic2V0VGltZW91dCIsInBvc2l0aW9uIiwibmV4dENhbGxiYWNrIiwiaW5kZXgiLCJwb3N0ZWQiLCJsaXZlIiwiYnViYmxlIiwiYnViYmxlQ29udGVudCIsImlubmVySFRNTCIsImluc2VydEJlZm9yZSIsImJ1YmJsZUJ1dHRvbnMiLCJ6IiwicGFyZW50Tm9kZSIsIndpZHRoIiwib2Zmc2V0V2lkdGgiLCJyZW1vdmVBdHRyaWJ1dGUiLCJ3YWl0IiwibWluVHlwaW5nV2FpdCIsImJ1YmJsZVdpZHRoQ2FsYyIsImNvbnRhaW5lckhlaWdodCIsIm9mZnNldEhlaWdodCIsInNjcm9sbERpZmZlcmVuY2UiLCJzY3JvbGxIZWlnaHQiLCJzY3JvbGxUb3AiLCJzY3JvbGxIb3AiLCJzY3JvbGxCdWJibGVzIiwicHJlcEhUTUwiLCJyZWxhdGl2ZV9wYXRoIiwiYm9keSIsImV4cG9ydHMiLCJsb2ciLCJCT1RfVVNFUiIsImNoYXRXaW5kb3ciLCJnZXRFbGVtZW50QnlJZCIsIm8iLCJtaXNzIiwicmVwbGllcyIsIm1hdGNoIiwic3RyaXAiLCJ0ZXh0IiwidG9Mb3dlckNhc2UiLCJyZXBsYWNlIiwiZm91bmQiLCJmb3JFYWNoIiwic2VuZE1lc3NhZ2UiLCJsaXZlYWdlbnQiLCJpbml0U2Vzc2lvbiIsImF4aW9zIiwiZ2V0IiwidGhlbiIsInJlc3BvbnNlIiwiZGF0YSIsInN0YXR1c0NvZGUiLCJpZCIsImFmZmluaXR5VG9rZW4iLCJwb3N0Iiwic2Vzc2lvbklkIiwic2Vzc2lvbktleSIsInNlc3Npb25BZmZpbml0eVRva2VuIiwicHVsbE1lc3NhZ2VzIiwiY2F0Y2giLCJtZXNzYWdlIiwicGFyYW1zIiwibWVzc2FnZXMiLCJsaXZlQWdlbnRNZXNzYWdlIiwiaXRlbSIsInR5cGUiLCJuYW1lIiwiYXNrQW55dGhpbmciLCJub3RSZWFsbHkiLCJ5ZXNIZWxwTWUiLCJjaGFsbGVuZ2UiLCJ5ZXNBY2NlcHRDaGFsbGVuZ2VPZmZlciIsInllc1NhdmUiLCJjb25uZWN0VG9MaXZlQWdlbnQiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNuRUE7QUFDQSxTQUFTQSxPQUFULENBQWlCQyxTQUFqQixFQUE0QkMsSUFBNUIsRUFBa0NDLE9BQWxDLEVBQTJDO0FBQ3pDO0FBQ0FBLFlBQVUsT0FBT0EsT0FBUCxLQUFtQixXQUFuQixHQUFpQ0EsT0FBakMsR0FBMkMsRUFBckQ7QUFDQUMsa0JBQWdCRCxRQUFRQyxhQUFSLElBQXlCLEdBQXpDLENBSHlDLENBR0k7QUFDN0NDLGNBQVlGLFFBQVFFLFNBQVIsSUFBcUIsQ0FBakMsQ0FKeUMsQ0FJTjtBQUNuQ0MsWUFBVUgsUUFBUUcsT0FBUixJQUFtQixDQUE3QixDQUx5QyxDQUtWO0FBQy9CQyxnQkFBY0osUUFBUUksV0FBUixJQUF1QixFQUFyQyxDQU55QyxDQU1EO0FBQ3hDQyx1QkFBcUJMLFFBQVFLLGtCQUFSLElBQThCLENBQW5ELENBUHlDLENBT1k7QUFDckRDLG9CQUFrQk4sUUFBUU0sZUFBUixJQUEyQixLQUE3QyxDQVJ5QyxDQVFVOztBQUVuRCxNQUFJQyxpQkFBaUIsT0FBckIsQ0FWeUMsQ0FVWjs7QUFFN0IsTUFBSUMsV0FBVyxFQUFmLENBWnlDLENBWXZCO0FBQ2xCO0FBQ0E7O0FBRUE7QUFDQSxNQUFJQyxvQkFBb0IsWUFBVztBQUNqQyxRQUFJQyxPQUFPLDBCQUFYO0FBQ0EsUUFBSTtBQUNGQyxtQkFBYUMsT0FBYixDQUFxQkYsSUFBckIsRUFBMkJBLElBQTNCO0FBQ0FDLG1CQUFhRSxVQUFiLENBQXdCSCxJQUF4QjtBQUNBLGFBQU8sSUFBUDtBQUNELEtBSkQsQ0FJRSxPQUFPSSxLQUFQLEVBQWM7QUFDZEMsY0FBUUQsS0FBUixDQUNFLCtNQURGO0FBR0EsYUFBTyxLQUFQO0FBQ0Q7QUFDRixHQVpEO0FBYUEsTUFBSUUsd0JBQXdCUCx1QkFBdUJKLHFCQUFxQixDQUF4RTtBQUNBLE1BQUlZLGlCQUFpQiwwQkFBckI7QUFDQSxNQUFJQyxzQkFDREYseUJBQ0NHLEtBQUtDLEtBQUwsQ0FBV1QsYUFBYVUsT0FBYixDQUFxQkosY0FBckIsQ0FBWCxDQURGLElBRUEsRUFIRjs7QUFLQTtBQUNBSyxxQkFBbUIsVUFBU0MsR0FBVCxFQUFjQyxLQUFkLEVBQXFCO0FBQ3RDLFFBQUksQ0FBQ1IscUJBQUwsRUFBNEI7QUFDNUI7QUFDQSxRQUFJRSxvQkFBb0JPLE1BQXBCLEdBQTZCcEIsa0JBQWpDLEVBQ0VhLG9CQUFvQlEsS0FBcEIsR0FKb0MsQ0FJUjs7QUFFOUI7QUFDQTtBQUNFO0FBQ0FILFFBQUlJLFFBQUosQ0FBYSxlQUFiO0FBQ0E7QUFDQUgsY0FBVSxzQkFGVjtBQUdBO0FBQ0FBLGNBQVUsa0JBTlo7QUFRRTtBQUNBOztBQUVGO0FBQ0FOLHdCQUFvQlUsSUFBcEIsQ0FBeUIsRUFBRUwsS0FBS0EsR0FBUCxFQUFZQyxPQUFPQSxLQUFuQixFQUF6QjtBQUNELEdBcEJEOztBQXNCQTtBQUNBSywyQkFBeUIsWUFBVztBQUNsQyxRQUFJLENBQUNiLHFCQUFMLEVBQTRCO0FBQzVCTCxpQkFBYUMsT0FBYixDQUFxQkssY0FBckIsRUFBcUNFLEtBQUtXLFNBQUwsQ0FBZVosbUJBQWYsQ0FBckM7QUFDRCxHQUhEOztBQUtBO0FBQ0FwQixZQUFVaUMsU0FBVixDQUFvQkMsR0FBcEIsQ0FBd0Isa0JBQXhCO0FBQ0EsTUFBSUMsYUFBYUMsU0FBU0MsYUFBVCxDQUF1QixLQUF2QixDQUFqQjtBQUNBRixhQUFXRyxTQUFYLEdBQXVCLGFBQXZCO0FBQ0F0QyxZQUFVdUMsV0FBVixDQUFzQkosVUFBdEI7O0FBRUE7QUFDQSxPQUFLSyxTQUFMLEdBQWlCLFVBQVNDLFVBQVQsRUFBcUI7QUFDcEMsUUFBSUMsWUFBWU4sU0FBU0MsYUFBVCxDQUF1QixLQUF2QixDQUFoQjtBQUNBSyxjQUFVSixTQUFWLEdBQXNCLFlBQXRCO0FBQ0EsUUFBSUssWUFBWVAsU0FBU0MsYUFBVCxDQUF1QixVQUF2QixDQUFoQjtBQUNBTSxjQUFVQyxZQUFWLENBQXVCLGFBQXZCLEVBQXNDLG9CQUF0QztBQUNBRixjQUFVSCxXQUFWLENBQXNCSSxTQUF0QjtBQUNBQSxjQUFVRSxnQkFBVixDQUEyQixVQUEzQixFQUF1QyxVQUFTQyxDQUFULEVBQVk7QUFDakQ7QUFDQSxVQUFJQSxFQUFFQyxPQUFGLElBQWEsRUFBakIsRUFBcUI7QUFDbkJELFVBQUVFLGNBQUY7QUFDQSxlQUFPQyxXQUFQLEtBQXVCLEtBQXZCLEdBQStCQyxhQUFhRCxXQUFiLENBQS9CLEdBQTJELEtBQTNELENBRm1CLENBRThDO0FBQ2pFLFlBQUlFLGFBQWFmLFNBQVNnQixnQkFBVCxDQUEwQixhQUExQixDQUFqQjtBQUNBRCxxQkFBYUEsV0FBV0EsV0FBV3hCLE1BQVgsR0FBb0IsQ0FBL0IsQ0FBYjtBQUNBd0IsbUJBQVdsQixTQUFYLENBQXFCb0IsUUFBckIsQ0FBOEIsT0FBOUIsS0FDQSxDQUFDRixXQUFXbEIsU0FBWCxDQUFxQm9CLFFBQXJCLENBQThCLGdCQUE5QixDQURELEdBRUlGLFdBQVdsQixTQUFYLENBQXFCQyxHQUFyQixDQUF5QixlQUF6QixDQUZKLEdBR0ksS0FISjtBQUlBb0Isa0JBQ0UsNkNBQTZDLEtBQUtDLEtBQWxELEdBQTBELFNBRDVELEVBRUUsWUFBVyxDQUFFLENBRmYsRUFHRSxzQkFIRjtBQUtBO0FBQ0EsZUFBT2QsVUFBUCxLQUFzQixVQUF0QixHQUNJQSxXQUFXO0FBQ1RlLGlCQUFPLEtBQUtELEtBREg7QUFFVEUsbUJBQVMvQyxRQUZBO0FBR1RELDBCQUFnQkE7QUFIUCxTQUFYLENBREosR0FNSSxLQU5KO0FBT0EsYUFBSzhDLEtBQUwsR0FBYSxFQUFiO0FBQ0Q7QUFDRixLQTFCRDtBQTJCQXZELGNBQVV1QyxXQUFWLENBQXNCRyxTQUF0QjtBQUNBUCxlQUFXdUIsS0FBWCxDQUFpQkMsYUFBakIsR0FBaUMsT0FBakM7QUFDQWhCLGNBQVVpQixLQUFWO0FBQ0QsR0FwQ0Q7QUFxQ0FwRCxvQkFBa0IsS0FBS2dDLFNBQUwsQ0FBZWhDLGVBQWYsQ0FBbEIsR0FBb0QsS0FBcEQ7O0FBRUE7QUFDQSxNQUFJcUQsZUFBZXpCLFNBQVNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBbkI7QUFDQXdCLGVBQWF2QixTQUFiLEdBQXlCLHVCQUF6QjtBQUNBLE9BQUt3QixPQUFPLENBQVosRUFBZUEsT0FBTyxDQUF0QixFQUF5QkEsTUFBekIsRUFBaUM7QUFDL0IsUUFBSUMsTUFBTTNCLFNBQVNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBVjtBQUNBMEIsUUFBSXpCLFNBQUosR0FBZ0IsU0FBU3dCLElBQVQsR0FBZ0IsTUFBaEM7QUFDQUQsaUJBQWF0QixXQUFiLENBQXlCd0IsR0FBekI7QUFDRDtBQUNENUIsYUFBV0ksV0FBWCxDQUF1QnNCLFlBQXZCOztBQUVBO0FBQ0EsT0FBS0csSUFBTCxHQUFZLFVBQVNQLE9BQVQsRUFBa0JRLElBQWxCLEVBQXdCO0FBQ2xDO0FBQ0F2RCxlQUFXd0QsT0FBT0MsTUFBUCxDQUFjekQsUUFBZCxFQUF3QitDLE9BQXhCLENBQVgsQ0FGa0MsQ0FFVTs7QUFFNUMsU0FBSy9CLEtBQUwsQ0FBV2hCLFNBQVN1RCxJQUFULENBQVg7QUFDQUEsV0FBUXhELGlCQUFpQndELElBQXpCLEdBQWlDLEtBQWpDO0FBQ0QsR0FORDs7QUFRQSxNQUFJRyxhQUFhLEtBQWpCLENBbkl5QyxDQW1JbEI7QUFDdkIsT0FBSzFDLEtBQUwsR0FBYSxVQUFTMkMsSUFBVCxFQUFlO0FBQzFCRCxpQkFBYSxPQUFPQyxJQUFQLEtBQWdCLFdBQTdCO0FBQ0FBLFdBQU8sQ0FBQ0QsVUFBRCxHQUFjQyxJQUFkLEdBQXFCM0QsU0FBUzRELEtBQXJDO0FBQ0FDLG9CQUFnQixFQUFoQjtBQUNBLFFBQUksQ0FBQ0YsSUFBTCxFQUFXO0FBQ1gsUUFBSUEsS0FBSzNDLEtBQUwsS0FBZThDLFNBQW5CLEVBQThCO0FBQzVCSCxXQUFLM0MsS0FBTCxDQUFXK0MsT0FBWDtBQUNBLFdBQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJTCxLQUFLM0MsS0FBTCxDQUFXQyxNQUEvQixFQUF1QytDLEdBQXZDLEVBQTRDO0FBQzFDLFNBQUMsQ0FBQyxVQUFTQyxFQUFULEVBQWFDLEtBQWIsRUFBb0I7QUFDcEJMLDJCQUNFLHlEQUNBcEUsZ0JBQWdCLENBQWhCLEdBQW9CeUUsS0FEcEIsR0FFQSxlQUZBLEdBR0EzRSxJQUhBLEdBSUEsV0FKQSxHQUtBMEUsR0FBR0UsTUFMSCxHQU1BLE1BTkEsR0FPQUYsR0FBR0csUUFQSCxHQVFBLHlDQVJBLEdBU0FILEdBQUdHLFFBVEgsR0FVQSxTQVhGO0FBWUQsU0FiQSxFQWFFVCxLQUFLM0MsS0FBTCxDQUFXZ0QsQ0FBWCxDQWJGLEVBYWlCQSxDQWJqQjtBQWNGO0FBQ0Y7QUFDREssaUJBQWFWLEtBQUtXLElBQWxCLEVBQXdCLFlBQVc7QUFDakNuQixtQkFBYTVCLFNBQWIsQ0FBdUJnRCxNQUF2QixDQUE4QixTQUE5QjtBQUNBVix3QkFBa0IsRUFBbEIsR0FDSWpCLFVBQVVpQixhQUFWLEVBQXlCLFlBQVcsQ0FBRSxDQUF0QyxFQUF3QyxPQUF4QyxDQURKLEdBRUlWLGFBQWE1QixTQUFiLENBQXVCQyxHQUF2QixDQUEyQixTQUEzQixDQUZKO0FBR0QsS0FMRDtBQU1ELEdBOUJEO0FBK0JBO0FBQ0EsT0FBSzJDLE1BQUwsR0FBYyxVQUFTSyxHQUFULEVBQWNDLE9BQWQsRUFBdUI7QUFDbkMsUUFBSUMsT0FBTyxVQUFTRixHQUFULEVBQWM7QUFDdkIsYUFBT0csT0FBT0gsR0FBUCxDQUFQLEtBQXVCLFVBQXZCLEdBQW9DRyxPQUFPSCxHQUFQLEdBQXBDLEdBQW9ELEtBQXBEO0FBQ0QsS0FGRDtBQUdBeEUsYUFBU3dFLEdBQVQsTUFBa0JWLFNBQWxCLElBQ0ssS0FBSzlDLEtBQUwsQ0FBV2hCLFNBQVN3RSxHQUFULENBQVgsR0FBNEJ6RSxpQkFBaUJ5RSxHQURsRCxJQUVJRSxLQUFLRixHQUFMLENBRko7O0FBSUE7QUFDQSxRQUFJeEUsU0FBU3dFLEdBQVQsTUFBa0JWLFNBQWxCLElBQStCVyxZQUFZWCxTQUEvQyxFQUEwRDtBQUN4RGhELHVCQUNFLDRDQUE0QzJELE9BQTVDLEdBQXNELFNBRHhELEVBRUUsa0JBRkY7QUFJRDtBQUNGLEdBZkQ7O0FBaUJBO0FBQ0EsT0FBS0csS0FBTCxHQUFhLFlBQVc7QUFDdEJ6QixpQkFBYTVCLFNBQWIsQ0FBdUJnRCxNQUF2QixDQUE4QixTQUE5QjtBQUNBLFNBQUtNLElBQUwsR0FBWSxZQUFXO0FBQ3JCMUIsbUJBQWE1QixTQUFiLENBQXVCQyxHQUF2QixDQUEyQixTQUEzQjtBQUNELEtBRkQ7QUFHRCxHQUxEOztBQU9BO0FBQ0EsTUFBSTZDLGVBQWUsVUFBU1MsQ0FBVCxFQUFZQyxRQUFaLEVBQXNCO0FBQ3ZDLFFBQUluQixRQUFRLFlBQVc7QUFDckJvQixpQkFBVyxZQUFXO0FBQ3BCRDtBQUNELE9BRkQsRUFFR3RGLGFBRkg7QUFHRCxLQUpEO0FBS0EsUUFBSXdGLFdBQVcsQ0FBZjtBQUNBLFNBQ0UsSUFBSUMsZUFBZUQsV0FBV0gsRUFBRTdELE1BQWIsR0FBc0IsQ0FEM0MsRUFFRWlFLGdCQUFnQkQsUUFGbEIsRUFHRUMsY0FIRixFQUlFO0FBQ0EsT0FBQyxDQUFDLFVBQVNILFFBQVQsRUFBbUJJLEtBQW5CLEVBQTBCO0FBQzFCdkIsZ0JBQVEsWUFBVztBQUNqQmhCLG9CQUFVa0MsRUFBRUssS0FBRixDQUFWLEVBQW9CSixRQUFwQjtBQUNELFNBRkQ7QUFHRCxPQUpBLEVBSUVuQixLQUpGLEVBSVNzQixZQUpUO0FBS0Y7QUFDRHRCO0FBQ0QsR0FuQkQ7O0FBcUJBO0FBQ0EsTUFBSXJCLGNBQWMsS0FBbEI7QUFDQSxNQUFJSyxZQUFZLFVBQVM3QixHQUFULEVBQWNxRSxNQUFkLEVBQXNCcEUsS0FBdEIsRUFBNkJxRSxJQUE3QixFQUFtQztBQUNqRHJFLFlBQVEsT0FBT0EsS0FBUCxLQUFpQixXQUFqQixHQUErQkEsS0FBL0IsR0FBdUMsRUFBL0M7QUFDQXFFLFdBQU8sT0FBT0EsSUFBUCxLQUFnQixXQUFoQixHQUE4QkEsSUFBOUIsR0FBcUMsSUFBNUMsQ0FGaUQsQ0FFQTtBQUNqRCxRQUFJNUYsZ0JBQWdCNEYsT0FBTyxLQUFLNUYsYUFBWixHQUE0QixDQUFoRDtBQUNBLFFBQUlDLFlBQVkyRixPQUFPLEtBQUszRixTQUFaLEdBQXdCLENBQXhDO0FBQ0E7QUFDQSxRQUFJNEYsU0FBUzVELFNBQVNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBYjtBQUNBLFFBQUk0RCxnQkFBZ0I3RCxTQUFTQyxhQUFULENBQXVCLE1BQXZCLENBQXBCO0FBQ0EyRCxXQUFPMUQsU0FBUCxHQUFtQixxQkFBcUIsQ0FBQ3lELElBQUQsR0FBUSxXQUFSLEdBQXNCLEVBQTNDLElBQWlEckUsS0FBcEU7QUFDQXVFLGtCQUFjM0QsU0FBZCxHQUEwQixnQkFBMUI7QUFDQTJELGtCQUFjQyxTQUFkLEdBQTBCekUsR0FBMUI7QUFDQXVFLFdBQU96RCxXQUFQLENBQW1CMEQsYUFBbkI7QUFDQTlELGVBQVdnRSxZQUFYLENBQXdCSCxNQUF4QixFQUFnQ25DLFlBQWhDO0FBQ0E7QUFDQSxRQUFJbkMsVUFBVSxFQUFkLEVBQWtCO0FBQ2hCLFVBQUkwRSxnQkFBZ0JILGNBQWM3QyxnQkFBZCxDQUErQixnQkFBL0IsQ0FBcEI7QUFDQSxXQUFLLElBQUlpRCxJQUFJLENBQWIsRUFBZ0JBLElBQUlELGNBQWN6RSxNQUFsQyxFQUEwQzBFLEdBQTFDLEVBQStDO0FBQzdDLFNBQUMsQ0FBQyxVQUFTMUIsRUFBVCxFQUFhO0FBQ2IsY0FBSSxDQUFDQSxHQUFHMkIsVUFBSCxDQUFjQSxVQUFkLENBQXlCckUsU0FBekIsQ0FBbUNvQixRQUFuQyxDQUE0QyxnQkFBNUMsQ0FBTCxFQUNFc0IsR0FBR2pCLEtBQUgsQ0FBUzZDLEtBQVQsR0FBaUI1QixHQUFHNkIsV0FBSCxHQUFpQmxHLGNBQWMsQ0FBL0IsR0FBbUNELE9BQW5DLEdBQTZDLElBQTlEO0FBQ0gsU0FIQSxFQUdFK0YsY0FBY0MsQ0FBZCxDQUhGO0FBSUY7QUFDREwsYUFBT25ELGdCQUFQLENBQXdCLE9BQXhCLEVBQWlDLFlBQVc7QUFDMUMsYUFBSyxJQUFJNkIsSUFBSSxDQUFiLEVBQWdCQSxJQUFJMEIsY0FBY3pFLE1BQWxDLEVBQTBDK0MsR0FBMUMsRUFBK0M7QUFDN0MsV0FBQyxDQUFDLFVBQVNDLEVBQVQsRUFBYTtBQUNiQSxlQUFHakIsS0FBSCxDQUFTNkMsS0FBVCxHQUFpQixJQUFJLElBQXJCO0FBQ0E1QixlQUFHMUMsU0FBSCxDQUFhb0IsUUFBYixDQUFzQixhQUF0QixJQUF3Q3NCLEdBQUdqQixLQUFILENBQVM2QyxLQUFULEdBQWlCLEVBQXpELEdBQStELEtBQS9EO0FBQ0E1QixlQUFHOEIsZUFBSCxDQUFtQixTQUFuQjtBQUNELFdBSkEsRUFJRUwsY0FBYzFCLENBQWQsQ0FKRjtBQUtGO0FBQ0QsYUFBS3pDLFNBQUwsQ0FBZUMsR0FBZixDQUFtQixlQUFuQjtBQUNELE9BVEQ7QUFVRDtBQUNEO0FBQ0F3RSxXQUFPWCxPQUFPNUYsZ0JBQWdCLENBQXZCLEdBQTJCLENBQWxDO0FBQ0F3RyxvQkFBZ0JaLE9BQU81RixnQkFBZ0IsQ0FBdkIsR0FBMkIsQ0FBM0M7QUFDQSxRQUFJc0IsSUFBSUUsTUFBSixHQUFhdkIsU0FBYixHQUF5QkQsYUFBekIsSUFBMEN1QixTQUFTLEVBQXZELEVBQTJEO0FBQ3pEZ0YsY0FBUXRHLFlBQVlxQixJQUFJRSxNQUF4QjtBQUNBK0UsYUFBT0MsYUFBUCxHQUF3QkQsT0FBT0MsYUFBL0IsR0FBZ0QsS0FBaEQ7QUFDQWpCLGlCQUFXLFlBQVc7QUFDcEI3QixxQkFBYTVCLFNBQWIsQ0FBdUJnRCxNQUF2QixDQUE4QixTQUE5QjtBQUNELE9BRkQsRUFFRzlFLGFBRkg7QUFHRDtBQUNENEYsWUFBUUwsV0FBVyxZQUFXO0FBQzVCN0IsbUJBQWE1QixTQUFiLENBQXVCQyxHQUF2QixDQUEyQixTQUEzQjtBQUNELEtBRk8sRUFFTHdFLE9BQU92RyxnQkFBZ0IsQ0FGbEIsQ0FBUjtBQUdBOEMsa0JBQWN5QyxXQUFXLFlBQVc7QUFDbENNLGFBQU8vRCxTQUFQLENBQWlCZ0QsTUFBakIsQ0FBd0IsU0FBeEI7QUFDQSxVQUFJMkIsa0JBQWtCWCxjQUFjTyxXQUFkLEdBQTRCbkcsT0FBNUIsR0FBc0MsSUFBNUQ7QUFDQTJGLGFBQU90QyxLQUFQLENBQWE2QyxLQUFiLEdBQXFCN0UsU0FBUyxFQUFULEdBQWNrRixlQUFkLEdBQWdDLEVBQXJEO0FBQ0FaLGFBQU90QyxLQUFQLENBQWE2QyxLQUFiLEdBQXFCOUUsSUFBSUksUUFBSixDQUFhLFdBQWIsSUFDakIsS0FEaUIsR0FFakJtRSxPQUFPdEMsS0FBUCxDQUFhNkMsS0FGakI7QUFHQVAsYUFBTy9ELFNBQVAsQ0FBaUJDLEdBQWpCLENBQXFCLEtBQXJCO0FBQ0E0RDs7QUFFQTtBQUNBdEUsdUJBQWlCQyxHQUFqQixFQUFzQkMsS0FBdEI7QUFDQSxPQUFDMEMsVUFBRCxJQUFlckMsd0JBQWYsQ0Faa0MsQ0FZTTs7QUFFeEM7QUFDQThFLHdCQUFrQjdHLFVBQVU4RyxZQUE1QjtBQUNBQyx5QkFBbUI1RSxXQUFXNkUsWUFBWCxHQUEwQjdFLFdBQVc4RSxTQUF4RDtBQUNBQyxrQkFBWUgsbUJBQW1CLEdBQS9CO0FBQ0EsVUFBSUksZ0JBQWdCLFlBQVc7QUFDN0IsYUFBSyxJQUFJekMsSUFBSSxDQUFiLEVBQWdCQSxLQUFLcUMsbUJBQW1CRyxTQUF4QyxFQUFtRHhDLEdBQW5ELEVBQXdEO0FBQ3RELFdBQUMsQ0FBQyxZQUFXO0FBQ1hnQix1QkFBVyxZQUFXO0FBQ3BCdkQseUJBQVc2RSxZQUFYLEdBQTBCN0UsV0FBVzhFLFNBQXJDLEdBQWlESixlQUFqRCxHQUNLMUUsV0FBVzhFLFNBQVgsR0FBdUI5RSxXQUFXOEUsU0FBWCxHQUF1QkMsU0FEbkQsR0FFSSxLQUZKO0FBR0QsYUFKRCxFQUlHeEMsSUFBSSxDQUpQO0FBS0QsV0FOQTtBQU9GO0FBQ0YsT0FWRDtBQVdBZ0IsaUJBQVd5QixhQUFYLEVBQTBCaEgsZ0JBQWdCLENBQTFDO0FBQ0QsS0E5QmEsRUE4Qlh1RyxPQUFPdkcsZ0JBQWdCLENBOUJaLENBQWQ7QUErQkQsR0E3RUQ7O0FBK0VBO0FBQ0EsT0FBSyxJQUFJdUUsSUFBSSxDQUFiLEVBQWdCQSxJQUFJdEQsb0JBQW9CTyxNQUF4QyxFQUFnRCtDLEdBQWhELEVBQXFEO0FBQ25EcEIsY0FDRWxDLG9CQUFvQnNELENBQXBCLEVBQXVCakQsR0FEekIsRUFFRSxZQUFXLENBQUUsQ0FGZixFQUdFTCxvQkFBb0JzRCxDQUFwQixFQUF1QmhELEtBSHpCLEVBSUUsS0FKRjtBQU1EO0FBQ0Y7O0FBRUQ7O0FBRUE7QUFDQSxTQUFTMEYsUUFBVCxDQUFrQmxILE9BQWxCLEVBQTJCO0FBQ3pCO0FBQ0EsTUFBSUEsVUFBVSxPQUFPQSxPQUFQLEtBQW1CLFdBQW5CLEdBQWlDQSxPQUFqQyxHQUEyQyxFQUF6RDtBQUNBLE1BQUlGLFlBQVlFLFFBQVFGLFNBQVIsSUFBcUIsTUFBckMsQ0FIeUIsQ0FHbUI7QUFDNUMsTUFBSXFILGdCQUFnQm5ILFFBQVFtSCxhQUFSLElBQXlCLElBQTdDOztBQUVBO0FBQ0FoQyxTQUFPckYsU0FBUCxJQUFvQm9DLFNBQVNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBcEI7QUFDQWdELFNBQU9yRixTQUFQLEVBQWtCNEMsWUFBbEIsQ0FBK0IsSUFBL0IsRUFBcUM1QyxTQUFyQztBQUNBb0MsV0FBU2tGLElBQVQsQ0FBYy9FLFdBQWQsQ0FBMEI4QyxPQUFPckYsU0FBUCxDQUExQjtBQUNEOztBQUVEO0FBQ0EsSUFBSSxJQUFKLEVBQW9DO0FBQ2xDdUgsVUFBUXhILE9BQVIsR0FBa0JBLE9BQWxCO0FBQ0F3SCxVQUFRSCxRQUFSLEdBQW1CQSxRQUFuQjtBQUNELEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuVUQ7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDREQUFBQSxDQUFTLEVBQUVDLGVBQWUsSUFBakIsRUFBVDs7QUFFQXhHLGFBQWFDLE9BQWIsQ0FBcUIsYUFBckIsRUFBbUMsS0FBbkM7QUFDQUcsUUFBUXVHLEdBQVIsQ0FBWTNHLGFBQWFVLE9BQWIsQ0FBcUIsYUFBckIsQ0FBWjtBQUNBLElBQUlrRyxXQUFXLDBCQUFmOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxJQUFJQyxhQUFhLElBQUksbURBQUosQ0FBWXRGLFNBQVN1RixjQUFULENBQXdCLE1BQXhCLENBQVosRUFBNkMsWUFBN0MsRUFBMkQ7QUFDeEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FuSCxxQkFBaUIsVUFBVW9ILENBQVYsRUFBYTtBQUMxQixZQUFHL0csYUFBYVUsT0FBYixDQUFxQixhQUFyQixLQUF1QyxPQUExQyxFQUFrRDtBQUFFO0FBQ2hEO0FBQ0EsZ0JBQUlzRyxPQUFPLFlBQVk7QUFDbkIsb0JBQUlDLFVBQVVGLEVBQUVuRSxPQUFGLENBQVVtRSxFQUFFbkgsY0FBWixFQUE0QmlCLEtBQTFDO0FBQ0FULHdCQUFRdUcsR0FBUixDQUFZTSxPQUFaO0FBQ0Esb0JBQUdBLFdBQVcsS0FBZCxFQUNJQSxVQUFVLENBQ047QUFDSWhELDhCQUFVLFlBRGQ7QUFFSUQsNEJBQVE7QUFGWixpQkFETSxDQUFWO0FBTUo2QywyQkFBVzFELElBQVgsQ0FDSTtBQUNJLHFDQUFpQjtBQUNiZ0IsOEJBQU0sQ0FDRnlDLFdBQVcsb0dBRFQsQ0FETztBQUliL0YsK0JBQU9vRztBQUpNO0FBRHJCLGlCQURKLEVBU0ksZUFUSjtBQVdILGFBckJEO0FBc0JBO0FBQ0EsZ0JBQUlDLFFBQVEsVUFBVTdDLEdBQVYsRUFBZTtBQUN2QlEsMkJBQVcsWUFBWTtBQUNuQmdDLCtCQUFXMUQsSUFBWCxDQUFnQixtREFBaEIsRUFBeUJrQixHQUF6QixFQURtQixDQUNXO0FBQ2pDLGlCQUZELEVBRUcsR0FGSDtBQUdILGFBSkQ7QUFLQTtBQUNBLGdCQUFJOEMsUUFBUSxVQUFVQyxJQUFWLEVBQWdCO0FBQ3hCLHVCQUFPQSxLQUFLQyxXQUFMLEdBQW1CQyxPQUFuQixDQUEyQixrQ0FBM0IsRUFBK0QsRUFBL0QsQ0FBUDtBQUNILGFBRkQ7QUFHQTtBQUNBLGdCQUFJQyxRQUFRLEtBQVo7QUFDQVIsY0FBRW5FLE9BQUYsQ0FBVW1FLEVBQUVuSCxjQUFaLEVBQTRCaUIsS0FBNUIsQ0FBa0MyRyxPQUFsQyxDQUEwQyxVQUFVdkYsQ0FBVixFQUFhNEIsQ0FBYixFQUFnQjtBQUN0RHNELHNCQUFNbEYsRUFBRWdDLFFBQVIsRUFBa0JqRCxRQUFsQixDQUEyQm1HLE1BQU1KLEVBQUVwRSxLQUFSLENBQTNCLEtBQThDb0UsRUFBRXBFLEtBQUYsQ0FBUTdCLE1BQVIsR0FBaUIsQ0FBL0QsR0FDT3lHLFFBQVF0RixFQUFFK0IsTUFEakIsR0FFTXVELFFBQVEsSUFBUixHQUFnQkEsUUFBUSxLQUY5QjtBQUdILGFBSkQ7QUFLQUEsb0JBQVFMLE1BQU1LLEtBQU4sQ0FBUixHQUF1QlAsTUFBdkI7QUFDSCxTQTFDRCxNQTBDTTtBQUNGUyx3QkFBWVYsRUFBRXBFLEtBQWQ7QUFDSDtBQUNKO0FBckR1RSxDQUEzRCxDQUFqQixDLENBc0RHOztBQUVINkIsT0FBT3FDLFVBQVAsR0FBb0JBLFVBQXBCOztBQUlBckMsT0FBT2tELFNBQVAsR0FBbUIsWUFBWTtBQUMzQkM7QUFDQWQsZUFBVzFELElBQVgsQ0FBZ0IsbURBQWhCLEVBQXlCLG9CQUF6QixFQUYyQixDQUVvQjtBQUMvQy9DLFlBQVF1RyxHQUFSLENBQVksZUFBWjtBQUNILENBSkQ7O0FBTUEsU0FBU2dCLFdBQVQsR0FBc0I7QUFDbEJDLElBQUEsNENBQUFBLENBQU1DLEdBQU4sQ0FBVSxjQUFWLEVBQ0NDLElBREQsQ0FDT0MsUUFBRCxJQUFjO0FBQ2hCLFlBQUlDLE9BQU9ELFNBQVNDLElBQXBCO0FBQ0EsWUFBR0EsS0FBS0QsUUFBTCxDQUFjRSxVQUFkLElBQTRCLEdBQS9CLEVBQW1DO0FBQy9CN0gsb0JBQVF1RyxHQUFSLENBQVksTUFBWixFQUFtQnFCLEtBQUtELFFBQUwsQ0FBY3RCLElBQWpDO0FBQ0F6Ryx5QkFBYUMsT0FBYixDQUFxQixXQUFyQixFQUFpQytILEtBQUtELFFBQUwsQ0FBY3RCLElBQWQsQ0FBbUJ5QixFQUFwRDtBQUNBbEkseUJBQWFDLE9BQWIsQ0FBcUIsWUFBckIsRUFBa0MrSCxLQUFLRCxRQUFMLENBQWN0QixJQUFkLENBQW1CcEMsR0FBckQ7QUFDQXJFLHlCQUFhQyxPQUFiLENBQXFCLHNCQUFyQixFQUE0QytILEtBQUtELFFBQUwsQ0FBY3RCLElBQWQsQ0FBbUIwQixhQUEvRDtBQUNILFNBTEQsTUFLSztBQUNEL0gsb0JBQVF1RyxHQUFSLENBQVksT0FBWixFQUFvQnFCLEtBQUtELFFBQUwsQ0FBYzVILEtBQWxDO0FBQ0g7QUFDSixLQVhELEVBV0cySCxJQVhILENBV1EsTUFBSTtBQUNSMUgsZ0JBQVF1RyxHQUFSLENBQVkzRyxhQUFhVSxPQUFiLENBQXFCLFdBQXJCLENBQVo7QUFDQWtILFFBQUEsNENBQUFBLENBQU1RLElBQU4sQ0FBVyxrQkFBWCxFQUE4QjtBQUMxQkMsdUJBQVdySSxhQUFhVSxPQUFiLENBQXFCLFdBQXJCLENBRGU7QUFFMUI0SCx3QkFBWXRJLGFBQWFVLE9BQWIsQ0FBcUIsWUFBckIsQ0FGYztBQUcxQjZILGtDQUFzQnZJLGFBQWFVLE9BQWIsQ0FBcUIsc0JBQXJCO0FBSEksU0FBOUIsRUFLQ29ILElBTEQsQ0FLT0MsUUFBRCxJQUFZO0FBQ2QsZ0JBQUdBLFNBQVNDLElBQVQsQ0FBY0QsUUFBZCxDQUF1QnRCLElBQXZCLElBQStCLElBQWxDLEVBQXVDO0FBQ25Dckcsd0JBQVF1RyxHQUFSLENBQVksaUNBQVo7QUFDQTNHLDZCQUFhQyxPQUFiLENBQXFCLGFBQXJCLEVBQW1DLElBQW5DO0FBQ0g7QUFDSixTQVZEO0FBV0gsS0F4QkQsRUF3Qkc2SCxJQXhCSCxDQXdCUSxNQUFJO0FBQ1JVO0FBQ0gsS0ExQkQsRUEyQkNDLEtBM0JELENBMkJRdEksS0FBRCxJQUFXO0FBQ2RDLGdCQUFRdUcsR0FBUixDQUFZeEcsS0FBWjtBQUNILEtBN0JEO0FBOEJIOztBQUVEOzs7QUFHQSxTQUFTc0gsV0FBVCxDQUFxQmlCLE9BQXJCLEVBQTZCO0FBQ3hCZCxJQUFBLDRDQUFBQSxDQUFNUSxJQUFOLENBQVcsa0JBQVgsRUFBOEI7QUFDM0JNLGlCQUFTQSxPQURrQjtBQUUzQkosb0JBQVl0SSxhQUFhVSxPQUFiLENBQXFCLFlBQXJCLENBRmU7QUFHM0I2SCw4QkFBc0J2SSxhQUFhVSxPQUFiLENBQXFCLHNCQUFyQjtBQUhLLEtBQTlCLEVBS0FvSCxJQUxBLENBS01DLFFBQUQsSUFBWTtBQUNkM0gsZ0JBQVF1RyxHQUFSLENBQVlvQixTQUFTQyxJQUFULENBQWNELFFBQTFCO0FBQ0EsWUFBR0EsU0FBU0MsSUFBVCxDQUFjRCxRQUFkLENBQXVCdEIsSUFBdkIsSUFBK0IsSUFBbEMsRUFBdUM7QUFDbkNyRyxvQkFBUXVHLEdBQVIsQ0FBWSxtQkFBWjtBQUNIO0FBQ0osS0FWQTtBQVdKOztBQUVEOzs7QUFHQSxTQUFTNkIsWUFBVCxHQUF1QjtBQUNuQlosSUFBQSw0Q0FBQUEsQ0FBTUMsR0FBTixDQUFVLGtCQUFWLEVBQTZCO0FBQ3pCYyxnQkFBUTtBQUNKTCx3QkFBWXRJLGFBQWFVLE9BQWIsQ0FBcUIsWUFBckIsQ0FEUjtBQUVKNkgsa0NBQXNCdkksYUFBYVUsT0FBYixDQUFxQixzQkFBckI7QUFGbEI7QUFEaUIsS0FBN0IsRUFNQ29ILElBTkQsQ0FNT0MsUUFBRCxJQUFZO0FBQ2QsWUFBR0EsU0FBUzVILEtBQVosRUFBa0I7QUFDZHdIO0FBQ0E7QUFDSDtBQUNEdkgsZ0JBQVF1RyxHQUFSLENBQVksaUJBQVosRUFBOEJvQixTQUFTQyxJQUF2QztBQUNBLFlBQUlZLFdBQVcsRUFBZjtBQUNBLFlBQUc7QUFDQ0EsdUJBQVdiLFNBQVNDLElBQVQsQ0FBY0QsUUFBZCxDQUF1QnRCLElBQXZCLENBQTRCbUMsUUFBdkM7QUFDSCxTQUZELENBRUMsT0FBTTNHLENBQU4sRUFBUTtBQUNMN0Isb0JBQVF1RyxHQUFSLENBQVkxRSxFQUFFeUcsT0FBZDtBQUNIO0FBQ0R0SSxnQkFBUXVHLEdBQVIsQ0FBWWlDLFFBQVo7QUFDQSxZQUFHQSxZQUFZQSxTQUFTOUgsTUFBVCxHQUFrQixDQUFqQyxFQUFtQztBQUMvQjhCLFlBQUEsbURBQUFBLENBQVFpRyxnQkFBUixHQUEyQixFQUFDMUUsTUFBTSxFQUFQLEVBQVd0RCxPQUFPLEVBQWxCLEVBQTNCO0FBQ0ErSCxxQkFBU3BCLE9BQVQsQ0FBa0JzQixJQUFELElBQVE7QUFDckIsb0JBQUdBLEtBQUtDLElBQUwsSUFBYSxpQkFBaEIsRUFBa0M7QUFDOUJuRyxvQkFBQSxtREFBQUEsQ0FBUWlHLGdCQUFSLENBQXlCMUUsSUFBekIsQ0FBOEJsRCxJQUE5QixDQUFtQzJGLFdBQVcsd0NBQVgsR0FBcURrQyxLQUFLSixPQUFMLENBQWFNLElBQWxFLEdBQXVFLFdBQTFHO0FBQ0gsaUJBRkQsTUFFTSxJQUFHRixLQUFLQyxJQUFMLElBQWEsYUFBaEIsRUFBOEI7QUFDaENuRyxvQkFBQSxtREFBQUEsQ0FBUWlHLGdCQUFSLENBQXlCMUUsSUFBekIsQ0FBOEJsRCxJQUE5QixDQUFtQyxhQUFXNkgsS0FBS0osT0FBTCxDQUFhTSxJQUF4QixHQUE2QixjQUE3QixHQUE0Q0YsS0FBS0osT0FBTCxDQUFhdEIsSUFBNUY7QUFDSCxpQkFGSyxNQUVBLElBQUcwQixLQUFLQyxJQUFMLElBQWEsV0FBaEIsRUFBNEI7QUFDOUJuRyxvQkFBQSxtREFBQUEsQ0FBUWlHLGdCQUFSLENBQXlCMUUsSUFBekIsQ0FBOEJsRCxJQUE5QixDQUFtQzJGLFdBQVcsc0JBQTlDO0FBQ0E1RyxpQ0FBYUMsT0FBYixDQUFxQixhQUFyQixFQUFtQyxLQUFuQztBQUNILGlCQUhLLE1BR0EsSUFBRzZJLEtBQUtDLElBQUwsSUFBYSxpQkFBaEIsRUFBa0M7QUFDcENwQjtBQUNBO0FBQ0g7QUFDSixhQVpEO0FBYUFkLHVCQUFXMUQsSUFBWCxDQUFnQixtREFBaEIsRUFBeUIsa0JBQXpCO0FBQ0g7O0FBRURxRjtBQUNILEtBdENEO0FBdUNIOztBQUdEO0FBQ0EzQixXQUFXMUQsSUFBWCxDQUFnQixtREFBaEIsRTs7Ozs7Ozs7Ozs7O0FDNUxBO0FBQUEsSUFBSXlELFdBQVcsMEJBQWY7O0FBRUEsSUFBSWhFLFVBQVU7QUFDVmEsV0FBTztBQUNIVSxjQUFNLENBQ0V5QyxXQUFXLElBRGIsRUFFRUEsV0FBVyx5REFGYixDQURIO0FBS0gvRixlQUFPLENBQ0g7QUFDSW9ELHNCQUFVLGdCQURkO0FBRUlELG9CQUFRO0FBRlosU0FERyxFQUtIO0FBQ0lDLHNCQUFVLFlBRGQ7QUFFSUQsb0JBQVE7QUFGWixTQUxHO0FBTEosS0FERztBQWlCVmlGLGlCQUFhO0FBQ1Q5RSxjQUFNLENBQUN5QyxXQUFXLDhDQUFaLENBREc7QUFFVC9GLGVBQU87QUFGRSxLQWpCSDtBQXFCVnFJLGVBQVc7QUFDUC9FLGNBQU0sQ0FBQ3lDLFdBQVcsNkJBQVosQ0FEQztBQUVQL0YsZUFBTyxDQUNIO0FBQ0lvRCxzQkFBVSxLQURkO0FBRUlELG9CQUFRO0FBRlosU0FERyxFQUtIO0FBQ0lDLHNCQUFVLElBRGQ7QUFFSUQsb0JBQVE7QUFGWixTQUxHO0FBRkEsS0FyQkQ7QUFrQ1ZtRixlQUFXO0FBQ1BoRixjQUFNLENBQ0Z5QyxXQUFXLHlFQURULEVBRUYsaUVBRkUsRUFHRiwyQkFIRSxFQUlGLHFCQUpFLEVBS0YscUZBTEUsRUFNRix5REFORSxDQURDO0FBU1AvRixlQUFPLENBQ0g7QUFDSW9ELHNCQUFVLGNBRGQ7QUFFSUQsb0JBQVE7QUFGWixTQURHLEVBS0g7QUFDSUMsc0JBQVUsV0FEZDtBQUVJRCxvQkFBUTtBQUZaLFNBTEc7QUFUQSxLQWxDRDtBQXNEVm9GLGVBQVc7QUFDUGpGLGNBQU0sQ0FDRnlDLFdBQVcsVUFEVCxFQUVGLDBGQUZFLENBREM7QUFLUC9GLGVBQU8sQ0FDSDtBQUNJb0Qsc0JBQVUsS0FEZDtBQUVJRCxvQkFBUTtBQUZaLFNBREcsRUFLSDtBQUNJQyxzQkFBVSxJQURkO0FBRUlELG9CQUFRO0FBRlosU0FMRztBQUxBLEtBdEREO0FBc0VUcUYsNkJBQXlCO0FBQ3RCbEYsY0FBTSxDQUNGeUMsV0FBVSwrRkFEUixDQURnQjtBQUl0Qi9GLGVBQU8sQ0FDSDtBQUNJb0Qsc0JBQVUsS0FEZDtBQUVJRCxvQkFBUTtBQUZaLFNBREcsRUFLSDtBQUNJQyxzQkFBVSxJQURkO0FBRUlELG9CQUFRO0FBRlosU0FMRztBQUplLEtBdEVoQjtBQXFGVnNGLGFBQVM7QUFDTG5GLGNBQU0sQ0FDRnlDLFdBQVcsOEVBRFQsRUFFRiwwQ0FGRSxDQUREO0FBS0wvRixlQUFPO0FBTEYsS0FyRkM7QUE0RlYwSSx3QkFBbUI7QUFDZnBGLGNBQU0sQ0FDRnlDLFdBQVcsa0RBRFQsQ0FEUztBQUlmL0YsZUFBTztBQUpRO0FBNUZULENBQWQ7O0FBcUdBLCtEQUFlK0IsT0FBZixFOzs7Ozs7Ozs7OztBQ3ZHQSw2Rjs7Ozs7Ozs7Ozs7O0FDQUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw0Q0FBNEM7QUFDNUM7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDs7Ozs7Ozs7Ozs7OztBQ25MQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWSxNQUFNO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNuREE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOzs7Ozs7Ozs7Ozs7O0FDbEJBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxTQUFTO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7OztBQ3hEQTs7QUFFQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNKQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUEsa0NBQWtDLGNBQWM7QUFDaEQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0RBQWdEO0FBQ2hEO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLGdEQUFnRDtBQUNoRDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxDQUFDOztBQUVEOzs7Ozs7Ozs7Ozs7O0FDOUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFNBQVM7QUFDcEIsV0FBVyxTQUFTO0FBQ3BCO0FBQ0EsWUFBWSxPQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsU0FBUztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7Ozs7Ozs7Ozs7Ozs7QUNuREE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2pCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CLHVDQUF1QztBQUN2QztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7QUFDSDs7Ozs7Ozs7Ozs7OztBQ3JGQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE1BQU07QUFDakIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsTUFBTTtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ3BCQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFNBQVM7QUFDcEIsV0FBVyxTQUFTO0FBQ3BCLFdBQVcsT0FBTztBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDekJBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsY0FBYztBQUN6QixXQUFXLE1BQU07QUFDakIsV0FBVyxlQUFlO0FBQzFCLGFBQWEsRUFBRTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7Ozs7Ozs7Ozs7Ozs7K0NDbkJBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0VBQXdFO0FBQ3hFO0FBQ0E7QUFDQTtBQUNBLHVEQUF1RDtBQUN2RDtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPLFlBQVk7QUFDbkI7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7O0FDL0ZBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixpQkFBaUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7QUNuQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNqRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDYkE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLHdDQUF3QztBQUN4QyxPQUFPOztBQUVQO0FBQ0EsMERBQTBELHdCQUF3QjtBQUNsRjtBQUNBLE9BQU87O0FBRVA7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQztBQUNoQyw2QkFBNkIsYUFBYSxFQUFFO0FBQzVDO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7Ozs7Ozs7Ozs7Ozs7QUNwREE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2JBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsT0FBTztBQUNyQixnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxjQUFjLE9BQU87QUFDckIsZ0JBQWdCLFFBQVE7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7Ozs7Ozs7Ozs7O0FDbkVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7Ozs7Ozs7Ozs7OztBQ1hBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxpQkFBaUIsZUFBZTs7QUFFaEM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTs7Ozs7Ozs7Ozs7OztBQ3BEQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQSxXQUFXLFNBQVM7QUFDcEIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQzFCQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxhQUFhO0FBQ3hCLFdBQVcsU0FBUztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsbUNBQW1DLE9BQU87QUFDMUM7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLFNBQVMsR0FBRyxTQUFTO0FBQzVDLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBLHVDQUF1QyxPQUFPO0FBQzlDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFlBQVksT0FBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDOVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDcEJBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsc0JBQXNCO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxxQ0FBcUM7O0FBRXJDO0FBQ0E7QUFDQTs7QUFFQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCLFVBQVUiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vYXBwL2NsaWVudC5qc1wiKTtcbiIsIi8vIGNvcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIEJ1YmJsZXMoY29udGFpbmVyLCBzZWxmLCBvcHRpb25zKSB7XG4gIC8vIG9wdGlvbnNcbiAgb3B0aW9ucyA9IHR5cGVvZiBvcHRpb25zICE9PSBcInVuZGVmaW5lZFwiID8gb3B0aW9ucyA6IHt9XG4gIGFuaW1hdGlvblRpbWUgPSBvcHRpb25zLmFuaW1hdGlvblRpbWUgfHwgMjAwIC8vIGhvdyBsb25nIGl0IHRha2VzIHRvIGFuaW1hdGUgY2hhdCBidWJibGUsIGFsc28gc2V0IGluIENTU1xuICB0eXBlU3BlZWQgPSBvcHRpb25zLnR5cGVTcGVlZCB8fCA1IC8vIGRlbGF5IHBlciBjaGFyYWN0ZXIsIHRvIHNpbXVsYXRlIHRoZSBtYWNoaW5lIFwidHlwaW5nXCJcbiAgd2lkZXJCeSA9IG9wdGlvbnMud2lkZXJCeSB8fCAyIC8vIGFkZCBhIGxpdHRsZSBleHRyYSB3aWR0aCB0byBidWJibGVzIHRvIG1ha2Ugc3VyZSB0aGV5IGRvbid0IGJyZWFrXG4gIHNpZGVQYWRkaW5nID0gb3B0aW9ucy5zaWRlUGFkZGluZyB8fCAxMCAvLyBwYWRkaW5nIG9uIGJvdGggc2lkZXMgb2YgY2hhdCBidWJibGVzXG4gIHJlY2FsbEludGVyYWN0aW9ucyA9IG9wdGlvbnMucmVjYWxsSW50ZXJhY3Rpb25zIHx8IDAgLy8gbnVtYmVyIG9mIGludGVyYWN0aW9ucyB0byBiZSByZW1lbWJlcmVkIGFuZCBicm91Z2h0IGJhY2sgdXBvbiByZXN0YXJ0XG4gIGlucHV0Q2FsbGJhY2tGbiA9IG9wdGlvbnMuaW5wdXRDYWxsYmFja0ZuIHx8IGZhbHNlIC8vIHNob3VsZCB3ZSBkaXNwbGF5IGFuIGlucHV0IGZpZWxkP1xuXG4gIHZhciBzdGFuZGluZ0Fuc3dlciA9IFwic3RhcnRcIiAvLyByZW1lbWJlciB3aGVyZSB0byByZXN0YXJ0IHNlbmFyaW8gaWYgaW50ZXJydXB0ZWRcblxuICB2YXIgX3NlbmFyaW8gPSB7fSAvLyBsb2NhbCBtZW1vcnkgZm9yIGNvbnZlcnNhdGlvbiBKU09OIG9iamVjdFxuICAvLy0tPiBOT1RFIHRoYXQgdGhpcyBvYmplY3QgaXMgb25seSBhc3NpZ25lZCBvbmNlLCBwZXIgc2Vzc2lvbiBhbmQgZG9lcyBub3QgY2hhbmdlIGZvciB0aGlzXG4gIC8vIFx0XHRjb25zdHJ1Y3RvciBuYW1lIGR1cmluZyBvcGVuIHNlc3Npb24uXG5cbiAgLy8gbG9jYWwgc3RvcmFnZSBmb3IgcmVjYWxsaW5nIGNvbnZlcnNhdGlvbnMgdXBvbiByZXN0YXJ0XG4gIHZhciBsb2NhbFN0b3JhZ2VDaGVjayA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciB0ZXN0ID0gXCJjaGF0LWJ1YmJsZS1zdG9yYWdlLXRlc3RcIlxuICAgIHRyeSB7XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0ZXN0LCB0ZXN0KVxuICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGVzdClcbiAgICAgIHJldHVybiB0cnVlXG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoXG4gICAgICAgIFwiWW91ciBzZXJ2ZXIgZG9lcyBub3QgYWxsb3cgc3RvcmluZyBkYXRhIGxvY2FsbHkuIE1vc3QgbGlrZWx5IGl0J3MgYmVjYXVzZSB5b3UndmUgb3BlbmVkIHRoaXMgcGFnZSBmcm9tIHlvdXIgaGFyZC1kcml2ZS4gRm9yIHRlc3RpbmcgeW91IGNhbiBkaXNhYmxlIHlvdXIgYnJvd3NlcidzIHNlY3VyaXR5IG9yIHN0YXJ0IGEgbG9jYWxob3N0IGVudmlyb25tZW50LlwiXG4gICAgICApXG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG4gIH1cbiAgdmFyIGxvY2FsU3RvcmFnZUF2YWlsYWJsZSA9IGxvY2FsU3RvcmFnZUNoZWNrKCkgJiYgcmVjYWxsSW50ZXJhY3Rpb25zID4gMFxuICB2YXIgaW50ZXJhY3Rpb25zTFMgPSBcImNoYXQtYnViYmxlLWludGVyYWN0aW9uc1wiXG4gIHZhciBpbnRlcmFjdGlvbnNIaXN0b3J5ID1cbiAgICAobG9jYWxTdG9yYWdlQXZhaWxhYmxlICYmXG4gICAgICBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKGludGVyYWN0aW9uc0xTKSkpIHx8XG4gICAgW11cblxuICAvLyBwcmVwYXJlIG5leHQgc2F2ZSBwb2ludFxuICBpbnRlcmFjdGlvbnNTYXZlID0gZnVuY3Rpb24oc2F5LCByZXBseSkge1xuICAgIGlmICghbG9jYWxTdG9yYWdlQXZhaWxhYmxlKSByZXR1cm5cbiAgICAvLyBsaW1pdCBudW1iZXIgb2Ygc2F2ZXNcbiAgICBpZiAoaW50ZXJhY3Rpb25zSGlzdG9yeS5sZW5ndGggPiByZWNhbGxJbnRlcmFjdGlvbnMpXG4gICAgICBpbnRlcmFjdGlvbnNIaXN0b3J5LnNoaWZ0KCkgLy8gcmVtb3ZlcyB0aGUgb2xkZXN0IChmaXJzdCkgc2F2ZSB0byBtYWtlIHNwYWNlXG5cbiAgICAvLyBkbyBub3QgbWVtb3JpemUgYnV0dG9uczsgb25seSB1c2VyIGlucHV0IGdldHMgbWVtb3JpemVkOlxuICAgIGlmIChcbiAgICAgIC8vIGBidWJibGUtYnV0dG9uYCBjbGFzcyBuYW1lIHNpZ25hbHMgdGhhdCBpdCdzIGEgYnV0dG9uXG4gICAgICBzYXkuaW5jbHVkZXMoXCJidWJibGUtYnV0dG9uXCIpICYmXG4gICAgICAvLyBpZiBpdCBpcyBub3Qgb2YgYSB0eXBlIG9mIHRleHR1YWwgcmVwbHlcbiAgICAgIHJlcGx5ICE9PSBcInJlcGx5IHJlcGx5LWZyZWVmb3JtXCIgJiZcbiAgICAgIC8vIGlmIGl0IGlzIG5vdCBvZiBhIHR5cGUgb2YgdGV4dHVhbCByZXBseSBvciBtZW1vcml6ZWQgdXNlciBjaG9pY2VcbiAgICAgIHJlcGx5ICE9PSBcInJlcGx5IHJlcGx5LXBpY2tcIlxuICAgIClcbiAgICAgIC8vIC4uLml0IHNoYW4ndCBiZSBtZW1vcml6ZWRcbiAgICAgIHJldHVyblxuXG4gICAgLy8gc2F2ZSB0byBtZW1vcnlcbiAgICBpbnRlcmFjdGlvbnNIaXN0b3J5LnB1c2goeyBzYXk6IHNheSwgcmVwbHk6IHJlcGx5IH0pXG4gIH1cblxuICAvLyBjb21taXQgc2F2ZSB0byBsb2NhbFN0b3JhZ2VcbiAgaW50ZXJhY3Rpb25zU2F2ZUNvbW1pdCA9IGZ1bmN0aW9uKCkge1xuICAgIGlmICghbG9jYWxTdG9yYWdlQXZhaWxhYmxlKSByZXR1cm5cbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShpbnRlcmFjdGlvbnNMUywgSlNPTi5zdHJpbmdpZnkoaW50ZXJhY3Rpb25zSGlzdG9yeSkpXG4gIH1cblxuICAvLyBzZXQgdXAgdGhlIHN0YWdlXG4gIGNvbnRhaW5lci5jbGFzc0xpc3QuYWRkKFwiYnViYmxlLWNvbnRhaW5lclwiKVxuICB2YXIgYnViYmxlV3JhcCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIilcbiAgYnViYmxlV3JhcC5jbGFzc05hbWUgPSBcImJ1YmJsZS13cmFwXCJcbiAgY29udGFpbmVyLmFwcGVuZENoaWxkKGJ1YmJsZVdyYXApXG5cbiAgLy8gaW5zdGFsbCB1c2VyIGlucHV0IHRleHRmaWVsZFxuICB0aGlzLnR5cGVJbnB1dCA9IGZ1bmN0aW9uKGNhbGxiYWNrRm4pIHtcbiAgICB2YXIgaW5wdXRXcmFwID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKVxuICAgIGlucHV0V3JhcC5jbGFzc05hbWUgPSBcImlucHV0LXdyYXBcIlxuICAgIHZhciBpbnB1dFRleHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwidGV4dGFyZWFcIilcbiAgICBpbnB1dFRleHQuc2V0QXR0cmlidXRlKFwicGxhY2Vob2xkZXJcIiwgXCJBc2sgbWUgYW55dGhpbmcuLi5cIilcbiAgICBpbnB1dFdyYXAuYXBwZW5kQ2hpbGQoaW5wdXRUZXh0KVxuICAgIGlucHV0VGV4dC5hZGRFdmVudExpc3RlbmVyKFwia2V5cHJlc3NcIiwgZnVuY3Rpb24oZSkge1xuICAgICAgLy8gcmVnaXN0ZXIgdXNlciBpbnB1dFxuICAgICAgaWYgKGUua2V5Q29kZSA9PSAxMykge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgdHlwZW9mIGJ1YmJsZVF1ZXVlICE9PSBmYWxzZSA/IGNsZWFyVGltZW91dChidWJibGVRdWV1ZSkgOiBmYWxzZSAvLyBhbGxvdyB1c2VyIHRvIGludGVycnVwdCB0aGUgYm90XG4gICAgICAgIHZhciBsYXN0QnViYmxlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5idWJibGUuc2F5XCIpXG4gICAgICAgIGxhc3RCdWJibGUgPSBsYXN0QnViYmxlW2xhc3RCdWJibGUubGVuZ3RoIC0gMV1cbiAgICAgICAgbGFzdEJ1YmJsZS5jbGFzc0xpc3QuY29udGFpbnMoXCJyZXBseVwiKSAmJlxuICAgICAgICAhbGFzdEJ1YmJsZS5jbGFzc0xpc3QuY29udGFpbnMoXCJyZXBseS1mcmVlZm9ybVwiKVxuICAgICAgICAgID8gbGFzdEJ1YmJsZS5jbGFzc0xpc3QuYWRkKFwiYnViYmxlLWhpZGRlblwiKVxuICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgYWRkQnViYmxlKFxuICAgICAgICAgICc8c3BhbiBjbGFzcz1cImJ1YmJsZS1idXR0b24gYnViYmxlLXBpY2tcIj4nICsgdGhpcy52YWx1ZSArIFwiPC9zcGFuPlwiLFxuICAgICAgICAgIGZ1bmN0aW9uKCkge30sXG4gICAgICAgICAgXCJyZXBseSByZXBseS1mcmVlZm9ybVwiXG4gICAgICAgIClcbiAgICAgICAgLy8gY2FsbGJhY2tcbiAgICAgICAgdHlwZW9mIGNhbGxiYWNrRm4gPT09IFwiZnVuY3Rpb25cIlxuICAgICAgICAgID8gY2FsbGJhY2tGbih7XG4gICAgICAgICAgICAgIGlucHV0OiB0aGlzLnZhbHVlLFxuICAgICAgICAgICAgICBzZW5hcmlvOiBfc2VuYXJpbyxcbiAgICAgICAgICAgICAgc3RhbmRpbmdBbnN3ZXI6IHN0YW5kaW5nQW5zd2VyXG4gICAgICAgICAgICB9KVxuICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgdGhpcy52YWx1ZSA9IFwiXCJcbiAgICAgIH1cbiAgICB9KVxuICAgIGNvbnRhaW5lci5hcHBlbmRDaGlsZChpbnB1dFdyYXApXG4gICAgYnViYmxlV3JhcC5zdHlsZS5wYWRkaW5nQm90dG9tID0gXCIxMDBweFwiXG4gICAgaW5wdXRUZXh0LmZvY3VzKClcbiAgfVxuICBpbnB1dENhbGxiYWNrRm4gPyB0aGlzLnR5cGVJbnB1dChpbnB1dENhbGxiYWNrRm4pIDogZmFsc2VcblxuICAvLyBpbml0IHR5cGluZyBidWJibGVcbiAgdmFyIGJ1YmJsZVR5cGluZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIilcbiAgYnViYmxlVHlwaW5nLmNsYXNzTmFtZSA9IFwiYnViYmxlLXR5cGluZyBpbWFnaW5lXCJcbiAgZm9yIChkb3RzID0gMDsgZG90cyA8IDM7IGRvdHMrKykge1xuICAgIHZhciBkb3QgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpXG4gICAgZG90LmNsYXNzTmFtZSA9IFwiZG90X1wiICsgZG90cyArIFwiIGRvdFwiXG4gICAgYnViYmxlVHlwaW5nLmFwcGVuZENoaWxkKGRvdClcbiAgfVxuICBidWJibGVXcmFwLmFwcGVuZENoaWxkKGJ1YmJsZVR5cGluZylcblxuICAvLyBhY2NlcHQgSlNPTiAmIGNyZWF0ZSBidWJibGVzXG4gIHRoaXMudGFsayA9IGZ1bmN0aW9uKHNlbmFyaW8sIGhlcmUpIHtcbiAgICAvLyBhbGwgZnVydGhlciAudGFsaygpIGNhbGxzIHdpbGwgYXBwZW5kIHRoZSBjb252ZXJzYXRpb24gd2l0aCBhZGRpdGlvbmFsIGJsb2NrcyBkZWZpbmVkIGluIHNlbmFyaW8gcGFyYW1ldGVyXG4gICAgX3NlbmFyaW8gPSBPYmplY3QuYXNzaWduKF9zZW5hcmlvLCBzZW5hcmlvKSAvLyBQT0xZRklMTCBSRVFVSVJFRCBGT1IgT0xERVIgQlJPV1NFUlNcblxuICAgIHRoaXMucmVwbHkoX3NlbmFyaW9baGVyZV0pXG4gICAgaGVyZSA/IChzdGFuZGluZ0Fuc3dlciA9IGhlcmUpIDogZmFsc2VcbiAgfVxuXG4gIHZhciBpY2VCcmVha2VyID0gZmFsc2UgLy8gdGhpcyB2YXJpYWJsZSBob2xkcyBhbnN3ZXIgdG8gd2hldGhlciB0aGlzIGlzIHRoZSBpbml0YXRpdmUgYm90IGludGVyYWN0aW9uIG9yIG5vdFxuICB0aGlzLnJlcGx5ID0gZnVuY3Rpb24odHVybikge1xuICAgIGljZUJyZWFrZXIgPSB0eXBlb2YgdHVybiA9PT0gXCJ1bmRlZmluZWRcIlxuICAgIHR1cm4gPSAhaWNlQnJlYWtlciA/IHR1cm4gOiBfc2VuYXJpby5zdGFydFxuICAgIHF1ZXN0aW9uc0hUTUwgPSBcIlwiXG4gICAgaWYgKCF0dXJuKSByZXR1cm5cbiAgICBpZiAodHVybi5yZXBseSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICB0dXJuLnJlcGx5LnJldmVyc2UoKVxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0dXJuLnJlcGx5Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIDsoZnVuY3Rpb24oZWwsIGNvdW50KSB7XG4gICAgICAgICAgcXVlc3Rpb25zSFRNTCArPVxuICAgICAgICAgICAgJzxzcGFuIGNsYXNzPVwiYnViYmxlLWJ1dHRvblwiIHN0eWxlPVwiYW5pbWF0aW9uLWRlbGF5OiAnICtcbiAgICAgICAgICAgIGFuaW1hdGlvblRpbWUgLyAyICogY291bnQgK1xuICAgICAgICAgICAgJ21zXCIgb25DbGljaz1cIicgK1xuICAgICAgICAgICAgc2VsZiArXG4gICAgICAgICAgICBcIi5hbnN3ZXIoJ1wiICtcbiAgICAgICAgICAgIGVsLmFuc3dlciArXG4gICAgICAgICAgICBcIicsICdcIiArXG4gICAgICAgICAgICBlbC5xdWVzdGlvbiArXG4gICAgICAgICAgICBcIicpO3RoaXMuY2xhc3NMaXN0LmFkZCgnYnViYmxlLXBpY2snKVxcXCI+XCIgK1xuICAgICAgICAgICAgZWwucXVlc3Rpb24gK1xuICAgICAgICAgICAgXCI8L3NwYW4+XCJcbiAgICAgICAgfSkodHVybi5yZXBseVtpXSwgaSlcbiAgICAgIH1cbiAgICB9XG4gICAgb3JkZXJCdWJibGVzKHR1cm4uc2F5cywgZnVuY3Rpb24oKSB7XG4gICAgICBidWJibGVUeXBpbmcuY2xhc3NMaXN0LnJlbW92ZShcImltYWdpbmVcIilcbiAgICAgIHF1ZXN0aW9uc0hUTUwgIT09IFwiXCJcbiAgICAgICAgPyBhZGRCdWJibGUocXVlc3Rpb25zSFRNTCwgZnVuY3Rpb24oKSB7fSwgXCJyZXBseVwiKVxuICAgICAgICA6IGJ1YmJsZVR5cGluZy5jbGFzc0xpc3QuYWRkKFwiaW1hZ2luZVwiKVxuICAgIH0pXG4gIH1cbiAgLy8gbmF2aWdhdGUgXCJhbnN3ZXJzXCJcbiAgdGhpcy5hbnN3ZXIgPSBmdW5jdGlvbihrZXksIGNvbnRlbnQpIHtcbiAgICB2YXIgZnVuYyA9IGZ1bmN0aW9uKGtleSkge1xuICAgICAgdHlwZW9mIHdpbmRvd1trZXldID09PSBcImZ1bmN0aW9uXCIgPyB3aW5kb3dba2V5XSgpIDogZmFsc2VcbiAgICB9XG4gICAgX3NlbmFyaW9ba2V5XSAhPT0gdW5kZWZpbmVkXG4gICAgICA/ICh0aGlzLnJlcGx5KF9zZW5hcmlvW2tleV0pLCAoc3RhbmRpbmdBbnN3ZXIgPSBrZXkpKVxuICAgICAgOiBmdW5jKGtleSlcblxuICAgIC8vIGFkZCByZS1nZW5lcmF0ZWQgdXNlciBwaWNrcyB0byB0aGUgaGlzdG9yeSBzdGFja1xuICAgIGlmIChfc2VuYXJpb1trZXldICE9PSB1bmRlZmluZWQgJiYgY29udGVudCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBpbnRlcmFjdGlvbnNTYXZlKFxuICAgICAgICAnPHNwYW4gY2xhc3M9XCJidWJibGUtYnV0dG9uIHJlcGx5LXBpY2tcIj4nICsgY29udGVudCArIFwiPC9zcGFuPlwiLFxuICAgICAgICBcInJlcGx5IHJlcGx5LXBpY2tcIlxuICAgICAgKVxuICAgIH1cbiAgfVxuXG4gIC8vIGFwaSBmb3IgdHlwaW5nIGJ1YmJsZVxuICB0aGlzLnRoaW5rID0gZnVuY3Rpb24oKSB7XG4gICAgYnViYmxlVHlwaW5nLmNsYXNzTGlzdC5yZW1vdmUoXCJpbWFnaW5lXCIpXG4gICAgdGhpcy5zdG9wID0gZnVuY3Rpb24oKSB7XG4gICAgICBidWJibGVUeXBpbmcuY2xhc3NMaXN0LmFkZChcImltYWdpbmVcIilcbiAgICB9XG4gIH1cblxuICAvLyBcInR5cGVcIiBlYWNoIG1lc3NhZ2Ugd2l0aGluIHRoZSBncm91cFxuICB2YXIgb3JkZXJCdWJibGVzID0gZnVuY3Rpb24ocSwgY2FsbGJhY2spIHtcbiAgICB2YXIgc3RhcnQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgIGNhbGxiYWNrKClcbiAgICAgIH0sIGFuaW1hdGlvblRpbWUpXG4gICAgfVxuICAgIHZhciBwb3NpdGlvbiA9IDBcbiAgICBmb3IgKFxuICAgICAgdmFyIG5leHRDYWxsYmFjayA9IHBvc2l0aW9uICsgcS5sZW5ndGggLSAxO1xuICAgICAgbmV4dENhbGxiYWNrID49IHBvc2l0aW9uO1xuICAgICAgbmV4dENhbGxiYWNrLS1cbiAgICApIHtcbiAgICAgIDsoZnVuY3Rpb24oY2FsbGJhY2ssIGluZGV4KSB7XG4gICAgICAgIHN0YXJ0ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgYWRkQnViYmxlKHFbaW5kZXhdLCBjYWxsYmFjaylcbiAgICAgICAgfVxuICAgICAgfSkoc3RhcnQsIG5leHRDYWxsYmFjaylcbiAgICB9XG4gICAgc3RhcnQoKVxuICB9XG5cbiAgLy8gY3JlYXRlIGEgYnViYmxlXG4gIHZhciBidWJibGVRdWV1ZSA9IGZhbHNlXG4gIHZhciBhZGRCdWJibGUgPSBmdW5jdGlvbihzYXksIHBvc3RlZCwgcmVwbHksIGxpdmUpIHtcbiAgICByZXBseSA9IHR5cGVvZiByZXBseSAhPT0gXCJ1bmRlZmluZWRcIiA/IHJlcGx5IDogXCJcIlxuICAgIGxpdmUgPSB0eXBlb2YgbGl2ZSAhPT0gXCJ1bmRlZmluZWRcIiA/IGxpdmUgOiB0cnVlIC8vIGJ1YmJsZXMgdGhhdCBhcmUgbm90IFwibGl2ZVwiIGFyZSBub3QgYW5pbWF0ZWQgYW5kIGRpc3BsYXllZCBkaWZmZXJlbnRseVxuICAgIHZhciBhbmltYXRpb25UaW1lID0gbGl2ZSA/IHRoaXMuYW5pbWF0aW9uVGltZSA6IDBcbiAgICB2YXIgdHlwZVNwZWVkID0gbGl2ZSA/IHRoaXMudHlwZVNwZWVkIDogMFxuICAgIC8vIGNyZWF0ZSBidWJibGUgZWxlbWVudFxuICAgIHZhciBidWJibGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpXG4gICAgdmFyIGJ1YmJsZUNvbnRlbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic3BhblwiKVxuICAgIGJ1YmJsZS5jbGFzc05hbWUgPSBcImJ1YmJsZSBpbWFnaW5lIFwiICsgKCFsaXZlID8gXCIgaGlzdG9yeSBcIiA6IFwiXCIpICsgcmVwbHlcbiAgICBidWJibGVDb250ZW50LmNsYXNzTmFtZSA9IFwiYnViYmxlLWNvbnRlbnRcIlxuICAgIGJ1YmJsZUNvbnRlbnQuaW5uZXJIVE1MID0gc2F5XG4gICAgYnViYmxlLmFwcGVuZENoaWxkKGJ1YmJsZUNvbnRlbnQpXG4gICAgYnViYmxlV3JhcC5pbnNlcnRCZWZvcmUoYnViYmxlLCBidWJibGVUeXBpbmcpXG4gICAgLy8gYW5zd2VyIHBpY2tlciBzdHlsZXNcbiAgICBpZiAocmVwbHkgIT09IFwiXCIpIHtcbiAgICAgIHZhciBidWJibGVCdXR0b25zID0gYnViYmxlQ29udGVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmJ1YmJsZS1idXR0b25cIilcbiAgICAgIGZvciAodmFyIHogPSAwOyB6IDwgYnViYmxlQnV0dG9ucy5sZW5ndGg7IHorKykge1xuICAgICAgICA7KGZ1bmN0aW9uKGVsKSB7XG4gICAgICAgICAgaWYgKCFlbC5wYXJlbnROb2RlLnBhcmVudE5vZGUuY2xhc3NMaXN0LmNvbnRhaW5zKFwicmVwbHktZnJlZWZvcm1cIikpXG4gICAgICAgICAgICBlbC5zdHlsZS53aWR0aCA9IGVsLm9mZnNldFdpZHRoIC0gc2lkZVBhZGRpbmcgKiAyICsgd2lkZXJCeSArIFwicHhcIlxuICAgICAgICB9KShidWJibGVCdXR0b25zW3pdKVxuICAgICAgfVxuICAgICAgYnViYmxlLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBidWJibGVCdXR0b25zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgOyhmdW5jdGlvbihlbCkge1xuICAgICAgICAgICAgZWwuc3R5bGUud2lkdGggPSAwICsgXCJweFwiXG4gICAgICAgICAgICBlbC5jbGFzc0xpc3QuY29udGFpbnMoXCJidWJibGUtcGlja1wiKSA/IChlbC5zdHlsZS53aWR0aCA9IFwiXCIpIDogZmFsc2VcbiAgICAgICAgICAgIGVsLnJlbW92ZUF0dHJpYnV0ZShcIm9uY2xpY2tcIilcbiAgICAgICAgICB9KShidWJibGVCdXR0b25zW2ldKVxuICAgICAgICB9XG4gICAgICAgIHRoaXMuY2xhc3NMaXN0LmFkZChcImJ1YmJsZS1waWNrZWRcIilcbiAgICAgIH0pXG4gICAgfVxuICAgIC8vIHRpbWUsIHNpemUgJiBhbmltYXRlXG4gICAgd2FpdCA9IGxpdmUgPyBhbmltYXRpb25UaW1lICogMiA6IDBcbiAgICBtaW5UeXBpbmdXYWl0ID0gbGl2ZSA/IGFuaW1hdGlvblRpbWUgKiA2IDogMFxuICAgIGlmIChzYXkubGVuZ3RoICogdHlwZVNwZWVkID4gYW5pbWF0aW9uVGltZSAmJiByZXBseSA9PSBcIlwiKSB7XG4gICAgICB3YWl0ICs9IHR5cGVTcGVlZCAqIHNheS5sZW5ndGhcbiAgICAgIHdhaXQgPCBtaW5UeXBpbmdXYWl0ID8gKHdhaXQgPSBtaW5UeXBpbmdXYWl0KSA6IGZhbHNlXG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICBidWJibGVUeXBpbmcuY2xhc3NMaXN0LnJlbW92ZShcImltYWdpbmVcIilcbiAgICAgIH0sIGFuaW1hdGlvblRpbWUpXG4gICAgfVxuICAgIGxpdmUgJiYgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgIGJ1YmJsZVR5cGluZy5jbGFzc0xpc3QuYWRkKFwiaW1hZ2luZVwiKVxuICAgIH0sIHdhaXQgLSBhbmltYXRpb25UaW1lICogMilcbiAgICBidWJibGVRdWV1ZSA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICBidWJibGUuY2xhc3NMaXN0LnJlbW92ZShcImltYWdpbmVcIilcbiAgICAgIHZhciBidWJibGVXaWR0aENhbGMgPSBidWJibGVDb250ZW50Lm9mZnNldFdpZHRoICsgd2lkZXJCeSArIFwicHhcIlxuICAgICAgYnViYmxlLnN0eWxlLndpZHRoID0gcmVwbHkgPT0gXCJcIiA/IGJ1YmJsZVdpZHRoQ2FsYyA6IFwiXCJcbiAgICAgIGJ1YmJsZS5zdHlsZS53aWR0aCA9IHNheS5pbmNsdWRlcyhcIjxpbWcgc3JjPVwiKVxuICAgICAgICA/IFwiNTAlXCJcbiAgICAgICAgOiBidWJibGUuc3R5bGUud2lkdGhcbiAgICAgIGJ1YmJsZS5jbGFzc0xpc3QuYWRkKFwic2F5XCIpXG4gICAgICBwb3N0ZWQoKVxuXG4gICAgICAvLyBzYXZlIHRoZSBpbnRlcmFjdGlvblxuICAgICAgaW50ZXJhY3Rpb25zU2F2ZShzYXksIHJlcGx5KVxuICAgICAgIWljZUJyZWFrZXIgJiYgaW50ZXJhY3Rpb25zU2F2ZUNvbW1pdCgpIC8vIHNhdmUgcG9pbnRcblxuICAgICAgLy8gYW5pbWF0ZSBzY3JvbGxpbmdcbiAgICAgIGNvbnRhaW5lckhlaWdodCA9IGNvbnRhaW5lci5vZmZzZXRIZWlnaHRcbiAgICAgIHNjcm9sbERpZmZlcmVuY2UgPSBidWJibGVXcmFwLnNjcm9sbEhlaWdodCAtIGJ1YmJsZVdyYXAuc2Nyb2xsVG9wXG4gICAgICBzY3JvbGxIb3AgPSBzY3JvbGxEaWZmZXJlbmNlIC8gMjAwXG4gICAgICB2YXIgc2Nyb2xsQnViYmxlcyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8PSBzY3JvbGxEaWZmZXJlbmNlIC8gc2Nyb2xsSG9wOyBpKyspIHtcbiAgICAgICAgICA7KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgYnViYmxlV3JhcC5zY3JvbGxIZWlnaHQgLSBidWJibGVXcmFwLnNjcm9sbFRvcCA+IGNvbnRhaW5lckhlaWdodFxuICAgICAgICAgICAgICAgID8gKGJ1YmJsZVdyYXAuc2Nyb2xsVG9wID0gYnViYmxlV3JhcC5zY3JvbGxUb3AgKyBzY3JvbGxIb3ApXG4gICAgICAgICAgICAgICAgOiBmYWxzZVxuICAgICAgICAgICAgfSwgaSAqIDUpXG4gICAgICAgICAgfSkoKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBzZXRUaW1lb3V0KHNjcm9sbEJ1YmJsZXMsIGFuaW1hdGlvblRpbWUgLyAyKVxuICAgIH0sIHdhaXQgKyBhbmltYXRpb25UaW1lICogMilcbiAgfVxuXG4gIC8vIHJlY2FsbCBwcmV2aW91cyBpbnRlcmFjdGlvbnNcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBpbnRlcmFjdGlvbnNIaXN0b3J5Lmxlbmd0aDsgaSsrKSB7XG4gICAgYWRkQnViYmxlKFxuICAgICAgaW50ZXJhY3Rpb25zSGlzdG9yeVtpXS5zYXksXG4gICAgICBmdW5jdGlvbigpIHt9LFxuICAgICAgaW50ZXJhY3Rpb25zSGlzdG9yeVtpXS5yZXBseSxcbiAgICAgIGZhbHNlXG4gICAgKVxuICB9XG59XG5cbi8vIGJlbG93IGZ1bmN0aW9ucyBhcmUgc3BlY2lmaWNhbGx5IGZvciBXZWJQYWNrLXR5cGUgcHJvamVjdCB0aGF0IHdvcmsgd2l0aCBpbXBvcnQoKVxuXG4vLyB0aGlzIGZ1bmN0aW9uIGF1dG9tYXRpY2FsbHkgYWRkcyBhbGwgSFRNTCBhbmQgQ1NTIG5lY2Vzc2FyeSBmb3IgY2hhdC1idWJibGUgdG8gZnVuY3Rpb25cbmZ1bmN0aW9uIHByZXBIVE1MKG9wdGlvbnMpIHtcbiAgLy8gb3B0aW9uc1xuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBvcHRpb25zICE9PSBcInVuZGVmaW5lZFwiID8gb3B0aW9ucyA6IHt9XG4gIHZhciBjb250YWluZXIgPSBvcHRpb25zLmNvbnRhaW5lciB8fCBcImNoYXRcIiAvLyBpZCBvZiB0aGUgY29udGFpbmVyIEhUTUwgZWxlbWVudFxuICB2YXIgcmVsYXRpdmVfcGF0aCA9IG9wdGlvbnMucmVsYXRpdmVfcGF0aCB8fCBcIi4vXCJcblxuICAvLyBtYWtlIEhUTUwgY29udGFpbmVyIGVsZW1lbnRcbiAgd2luZG93W2NvbnRhaW5lcl0gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpXG4gIHdpbmRvd1tjb250YWluZXJdLnNldEF0dHJpYnV0ZShcImlkXCIsIGNvbnRhaW5lcilcbiAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh3aW5kb3dbY29udGFpbmVyXSlcbn1cblxuLy8gZXhwb3J0cyBmb3IgZXM2XG5pZiAodHlwZW9mIGV4cG9ydHMgIT09IFwidW5kZWZpbmVkXCIpIHtcbiAgZXhwb3J0cy5CdWJibGVzID0gQnViYmxlc1xuICBleHBvcnRzLnByZXBIVE1MID0gcHJlcEhUTUxcbn1cbiIsIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi8qKioqKioqIENPTlZFTklFTkNFIE1FVEhPRFMgQVZBSUxBQkxFIEZPUiBFUzYgQlVJTEQgRU5WSVJPTk1FTlRTICoqKioqKiovXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG4vLyB0aGUgVVJMIG9mIHdoZXJlIHlvdSd2ZSBpbnN0YWxsZWQgdGhlIGNvbXBvbmVudDsgeW91IG1heSBuZWVkIHRvIGNoYW5nZSB0aGlzOlxyXG5pbXBvcnQgeyBCdWJibGVzLCBwcmVwSFRNTCB9IGZyb20gXCIuL2J1YmJsZXMuanNcIlxyXG5pbXBvcnQgc2VuYXJpbyBmcm9tIFwiLi9zZW5hcmlvLmpzXCJcclxuXHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcydcclxuXHJcbi8vIHRoaXMgaXMgYSBjb252ZW5pZW5jZSBzY3JpcHQgdGhhdCBidWlsZHMgYWxsIG5lY2Vzc2FyeSBIVE1MLFxyXG4vLyBpbXBvcnRzIGFsbCBzY3JpcHRzIGFuZCBzdHlsZXNoZWV0czsgeW91ciBjb250YWluZXIgRElWIHdpbGxcclxuLy8gaGF2ZSBhIGRlZmF1bHQgYGlkPVwiY2hhdFwiYDtcclxuLy8geW91IGNhbiBzcGVjaWZ5IGEgZGlmZmVyZW50IElEIHdpdGg6XHJcbi8vIGBjb250YWluZXI6IFwibXlfY2hhdGJveF9pZFwiYCBvcHRpb25cclxucHJlcEhUTUwoeyByZWxhdGl2ZV9wYXRoOiBcIi4vXCIgfSlcclxuXHJcbmxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY2hhdFNlc3Npb25cIixmYWxzZSk7XHJcbmNvbnNvbGUubG9nKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdjaGF0U2Vzc2lvbicpKVxyXG52YXIgQk9UX1VTRVIgPSBcIjxzdHJvbmcgPkJvdDwvc3Ryb25nID46IFwiO1xyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLyoqKioqKioqKioqKioqKioqKioqKiogTkdQIGNoYXRib3QgSW1wbGVtZW50YXRpb24gKioqKioqKioqKioqKioqKioqKioqKi9cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbnZhciBjaGF0V2luZG93ID0gbmV3IEJ1YmJsZXMoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjaGF0XCIpLCBcImNoYXRXaW5kb3dcIiwge1xyXG4gICAgLy8gdGhlIG9uZSB0aGF0IHdlIGNhcmUgYWJvdXQgaXMgaW5wdXRDYWxsYmFja0ZuKClcclxuICAgIC8vIHRoaXMgZnVuY3Rpb24gcmV0dXJucyBhbiBvYmplY3Qgd2l0aCBzb21lIGRhdGEgdGhhdCB3ZSBjYW4gcHJvY2VzcyBmcm9tIHVzZXIgaW5wdXRcclxuICAgIC8vIGFuZCB1bmRlcnN0YW5kIHRoZSBjb250ZXh0IG9mIGl0XHJcbiAgICAvLyB0aGlzIGlzIGFuIGV4YW1wbGUgZnVuY3Rpb24gdGhhdCBtYXRjaGVzIHRoZSB0ZXh0IHVzZXIgdHlwZWQgdG8gb25lIG9mIHRoZSBhbnN3ZXIgYnViYmxlc1xyXG4gICAgLy8gdGhpcyBmdW5jdGlvbiBkb2VzIG5vIG5hdHVyYWwgbGFuZ3VhZ2UgcHJvY2Vzc2luZ1xyXG4gICAgLy8gdGhpcyBpcyB3aGVyZSB5b3UgbWF5IHdhbnQgdG8gY29ubmVjdCB0aGlzIHNjcmlwdCB0byBOTEMgYmFja2VuZC5cclxuICAgIGlucHV0Q2FsbGJhY2tGbjogZnVuY3Rpb24gKG8pIHtcclxuICAgICAgICBpZihsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImNoYXRTZXNzaW9uXCIpID09IFwiZmFsc2VcIil7IC8vIGlmIG5vIGxpdmUgc2Vzc2lvbiBpcyBvblxyXG4gICAgICAgICAgICAvLyBhZGQgZXJyb3IgY29udmVyc2F0aW9uIGJsb2NrICYgcmVjYWxsIGl0IGlmIG5vIGFuc3dlciBtYXRjaGVkXHJcbiAgICAgICAgICAgIHZhciBtaXNzID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHJlcGxpZXMgPSBvLnNlbmFyaW9bby5zdGFuZGluZ0Fuc3dlcl0ucmVwbHkgXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXBsaWVzKTtcclxuICAgICAgICAgICAgICAgIGlmKHJlcGxpZXMgPT0gZmFsc2UpXHJcbiAgICAgICAgICAgICAgICAgICAgcmVwbGllcyA9IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb246IFwiTGl2ZSBBZ2VudFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5zd2VyOiBcImxpdmVhZ2VudFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBdO1xyXG4gICAgICAgICAgICAgICAgY2hhdFdpbmRvdy50YWxrKFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJpLWRvbnQtZ2V0LWl0XCI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNheXM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBCT1RfVVNFUiArIFwiU29ycnksIEkgZG9uJ3QgZ2V0IGl0IPCfmJUuIHR5cGUgXFxcIjxzdHJvbmc+TGl2ZSBBZ2VudDwvc3Ryb25nPlxcXCIgaWYgeW91IHdhbnQgdG8gY2hhdCB3aXRoIGEgaHVtYW4g8J+RqFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVwbHk6IHJlcGxpZXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgXCJpLWRvbnQtZ2V0LWl0XCJcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBkbyB0aGlzIGlmIGFuc3dlciBmb3VuZFxyXG4gICAgICAgICAgICB2YXIgbWF0Y2ggPSBmdW5jdGlvbiAoa2V5KSB7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBjaGF0V2luZG93LnRhbGsoc2VuYXJpbywga2V5KSAvLyByZXN0YXJ0IGN1cnJlbnQgc2VuYXJpbyBmcm9tIHBvaW50IGZvdW5kIGluIHRoZSBhbnN3ZXJcclxuICAgICAgICAgICAgICAgIH0sIDYwMClcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBzYW5pdGl6ZSB0ZXh0IGZvciBzZWFyY2ggZnVuY3Rpb25cclxuICAgICAgICAgICAgdmFyIHN0cmlwID0gZnVuY3Rpb24gKHRleHQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0ZXh0LnRvTG93ZXJDYXNlKCkucmVwbGFjZSgvW1xccy4sXFwvIyEkJVxcXiZcXCo7Ont9PVxcLV8nXCJgfigpXS9nLCBcIlwiKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIHNlYXJjaCBmdW5jdGlvblxyXG4gICAgICAgICAgICB2YXIgZm91bmQgPSBmYWxzZVxyXG4gICAgICAgICAgICBvLnNlbmFyaW9bby5zdGFuZGluZ0Fuc3dlcl0ucmVwbHkuZm9yRWFjaChmdW5jdGlvbiAoZSwgaSkge1xyXG4gICAgICAgICAgICAgICAgc3RyaXAoZS5xdWVzdGlvbikuaW5jbHVkZXMoc3RyaXAoby5pbnB1dCkpICYmIG8uaW5wdXQubGVuZ3RoID4gMFxyXG4gICAgICAgICAgICAgICAgICAgID8gKGZvdW5kID0gZS5hbnN3ZXIpXHJcbiAgICAgICAgICAgICAgICAgICAgOiBmb3VuZCA/IG51bGwgOiAoZm91bmQgPSBmYWxzZSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgZm91bmQgPyBtYXRjaChmb3VuZCkgOiBtaXNzKClcclxuICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICAgIHNlbmRNZXNzYWdlKG8uaW5wdXQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSkgLy8gZG9uZSBzZXR0aW5nIHVwIGNoYXQtYnViYmxlXHJcblxyXG53aW5kb3cuY2hhdFdpbmRvdyA9IGNoYXRXaW5kb3dcclxuXHJcblxyXG5cclxud2luZG93LmxpdmVhZ2VudCA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGluaXRTZXNzaW9uKCk7XHJcbiAgICBjaGF0V2luZG93LnRhbGsoc2VuYXJpbywgXCJjb25uZWN0VG9MaXZlQWdlbnRcIikgLy8gdGhlIGNvbnZlcnNhdGlvbiBjYW4gYmUgZWFzaWx5IHJlc3RhcnRlZCBmcm9tIGhlcmUuXHJcbiAgICBjb25zb2xlLmxvZyhcIkNvbm5lY3RpbmcuLi5cIilcclxufVxyXG5cclxuZnVuY3Rpb24gaW5pdFNlc3Npb24oKXtcclxuICAgIGF4aW9zLmdldChcIi9pbml0U2Vzc2lvblwiKVxyXG4gICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgbGV0IGRhdGEgPSByZXNwb25zZS5kYXRhO1xyXG4gICAgICAgIGlmKGRhdGEucmVzcG9uc2Uuc3RhdHVzQ29kZSA9PSAyMDApe1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnYm9keScsZGF0YS5yZXNwb25zZS5ib2R5KTtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Nlc3Npb25JZCcsZGF0YS5yZXNwb25zZS5ib2R5LmlkKTtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Nlc3Npb25LZXknLGRhdGEucmVzcG9uc2UuYm9keS5rZXkpO1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnc2Vzc2lvbkFmZmluaXR5VG9rZW4nLGRhdGEucmVzcG9uc2UuYm9keS5hZmZpbml0eVRva2VuKTtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2Vycm9yJyxkYXRhLnJlc3BvbnNlLmVycm9yKTtcclxuICAgICAgICB9XHJcbiAgICB9KS50aGVuKCgpPT57XHJcbiAgICAgICAgY29uc29sZS5sb2cobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Nlc3Npb25JZCcpKTtcclxuICAgICAgICBheGlvcy5wb3N0KFwiL2luaXRDaGF0U2Vzc2lvblwiLHtcclxuICAgICAgICAgICAgc2Vzc2lvbklkOiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnc2Vzc2lvbklkJyksXHJcbiAgICAgICAgICAgIHNlc3Npb25LZXk6IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdzZXNzaW9uS2V5JyksXHJcbiAgICAgICAgICAgIHNlc3Npb25BZmZpbml0eVRva2VuOiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnc2Vzc2lvbkFmZmluaXR5VG9rZW4nKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKT0+e1xyXG4gICAgICAgICAgICBpZihyZXNwb25zZS5kYXRhLnJlc3BvbnNlLmJvZHkgPT0gXCJPS1wiKXtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTGl2ZSBBZ2VudCBjaGF0IHNlc3Npb24gY3JlYXRlZFwiKTtcclxuICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY2hhdFNlc3Npb25cIix0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9KS50aGVuKCgpPT57XHJcbiAgICAgICAgcHVsbE1lc3NhZ2VzKCk7XHJcbiAgICB9KVxyXG4gICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG4vKipcclxuICogU2VuZCBDaGF0IE1lc3NhZ2VcclxuICovXHJcbmZ1bmN0aW9uIHNlbmRNZXNzYWdlKG1lc3NhZ2Upe1xyXG4gICAgIGF4aW9zLnBvc3QoXCIvc2VuZENoYXRNZXNzYWdlXCIse1xyXG4gICAgICAgIG1lc3NhZ2U6IG1lc3NhZ2UsXHJcbiAgICAgICAgc2Vzc2lvbktleTogbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Nlc3Npb25LZXknKSxcclxuICAgICAgICBzZXNzaW9uQWZmaW5pdHlUb2tlbjogbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Nlc3Npb25BZmZpbml0eVRva2VuJylcclxuICAgIH0pXHJcbiAgICAudGhlbigocmVzcG9uc2UpPT57XHJcbiAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UuZGF0YS5yZXNwb25zZSk7XHJcbiAgICAgICAgaWYocmVzcG9uc2UuZGF0YS5yZXNwb25zZS5ib2R5ID09IFwiT0tcIil7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ2hhdCBtZXNzYWdlIHNlbnRcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfSlcclxufVxyXG5cclxuLyoqXHJcbiAqIFB1bGxpbmcgbWVzc2FnZXNcclxuICovXHJcbmZ1bmN0aW9uIHB1bGxNZXNzYWdlcygpe1xyXG4gICAgYXhpb3MuZ2V0KFwiL2dldENoYXRNZXNzYWdlc1wiLHtcclxuICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgc2Vzc2lvbktleTogbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Nlc3Npb25LZXknKSxcclxuICAgICAgICAgICAgc2Vzc2lvbkFmZmluaXR5VG9rZW46IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdzZXNzaW9uQWZmaW5pdHlUb2tlbicpXHJcbiAgICAgICAgfVxyXG4gICAgfSlcclxuICAgIC50aGVuKChyZXNwb25zZSk9PntcclxuICAgICAgICBpZihyZXNwb25zZS5lcnJvcil7XHJcbiAgICAgICAgICAgIGluaXRTZXNzaW9uKCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJnZXRDaGF0TWVzc2FnZXNcIixyZXNwb25zZS5kYXRhKVxyXG4gICAgICAgIGxldCBtZXNzYWdlcyA9IFtdO1xyXG4gICAgICAgIHRyeXtcclxuICAgICAgICAgICAgbWVzc2FnZXMgPSByZXNwb25zZS5kYXRhLnJlc3BvbnNlLmJvZHkubWVzc2FnZXM7XHJcbiAgICAgICAgfWNhdGNoKGUpe1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlLm1lc3NhZ2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zb2xlLmxvZyhtZXNzYWdlcylcclxuICAgICAgICBpZihtZXNzYWdlcyAmJiBtZXNzYWdlcy5sZW5ndGggPiAwKXtcclxuICAgICAgICAgICAgc2VuYXJpby5saXZlQWdlbnRNZXNzYWdlID0ge3NheXM6IFtdLCByZXBseTogW119O1xyXG4gICAgICAgICAgICBtZXNzYWdlcy5mb3JFYWNoKChpdGVtKT0+e1xyXG4gICAgICAgICAgICAgICAgaWYoaXRlbS50eXBlID09IFwiQ2hhdEVzdGFibGlzaGVkXCIpe1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbmFyaW8ubGl2ZUFnZW50TWVzc2FnZS5zYXlzLnB1c2goQk9UX1VTRVIgKyBcIkxpdmUgQ2hhdCDwn5KsIGVzdGFibGlzaGVkIHdpdGggPHN0cm9uZz5cIisgaXRlbS5tZXNzYWdlLm5hbWUrXCI8L3N0cm9uZz5cIilcclxuICAgICAgICAgICAgICAgIH1lbHNlIGlmKGl0ZW0udHlwZSA9PSBcIkNoYXRNZXNzYWdlXCIpe1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbmFyaW8ubGl2ZUFnZW50TWVzc2FnZS5zYXlzLnB1c2goXCI8c3Ryb25nPlwiK2l0ZW0ubWVzc2FnZS5uYW1lK1wiPC9zdHJvbmc+IDogXCIraXRlbS5tZXNzYWdlLnRleHQpXHJcbiAgICAgICAgICAgICAgICB9ZWxzZSBpZihpdGVtLnR5cGUgPT0gXCJDaGF0RW5kZWRcIil7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VuYXJpby5saXZlQWdlbnRNZXNzYWdlLnNheXMucHVzaChCT1RfVVNFUiArIFwiTGl2ZSBDaGF0IPCfkqwgZW5kZWQg4p2MXCIpXHJcbiAgICAgICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjaGF0U2Vzc2lvblwiLGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIH1lbHNlIGlmKGl0ZW0udHlwZSA9PSBcIkNoYXRSZXF1ZXN0RmFpbFwiKXtcclxuICAgICAgICAgICAgICAgICAgICBpbml0U2Vzc2lvbigpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgY2hhdFdpbmRvdy50YWxrKHNlbmFyaW8sIFwibGl2ZUFnZW50TWVzc2FnZVwiKVxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICBwdWxsTWVzc2FnZXMoKTtcclxuICAgIH0pXHJcbn1cclxuXHJcblxyXG4vLyBwYXNzIEpTT04gdG8geW91ciBmdW5jdGlvbiBhbmQgeW91J3JlIGRvbmUhXHJcbmNoYXRXaW5kb3cudGFsayhzZW5hcmlvKSIsInZhciBCT1RfVVNFUiA9IFwiPHN0cm9uZyA+Qm90PC9zdHJvbmcgPjogXCI7XHJcblxyXG5sZXQgc2VuYXJpbyA9IHtcclxuICAgIHN0YXJ0OiB7XHJcbiAgICAgICAgc2F5czogWyBcclxuICAgICAgICAgICAgICAgIEJPVF9VU0VSICsgXCJIaVwiLCBcclxuICAgICAgICAgICAgICAgIEJPVF9VU0VSICsgXCJOaWNlIHByb2plY3Qg8J+RjS4gRG8geW91IGFscmVhZHkga25vdyBob3cgdG8gZmluYW5jZSBpdD9cIlxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgIHJlcGx5OiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHF1ZXN0aW9uOiBcIlllcywgVGhhbmsgeW91XCIsXHJcbiAgICAgICAgICAgICAgICBhbnN3ZXI6IFwiYXNrQW55dGhpbmdcIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBxdWVzdGlvbjogXCJOb3QgUmVhbGx5XCIsXHJcbiAgICAgICAgICAgICAgICBhbnN3ZXI6IFwibm90UmVhbGx5XCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICBhc2tBbnl0aGluZzoge1xyXG4gICAgICAgIHNheXM6IFtCT1RfVVNFUiArIFwiT2ssIElzIHRoZXJlIGFueXRoaW5nIGVsc2UgSSBjYW4gZG8gZm9yIHlvdT9cIl0sXHJcbiAgICAgICAgcmVwbHk6IFtdXHJcbiAgICB9LFxyXG4gICAgbm90UmVhbGx5OiB7XHJcbiAgICAgICAgc2F5czogW0JPVF9VU0VSICsgXCJEbyB5b3Ugd2FudCBtZSB0byBoZWxwIHlvdT9cIl0sXHJcbiAgICAgICAgcmVwbHk6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcXVlc3Rpb246IFwiWWVzXCIsXHJcbiAgICAgICAgICAgICAgICBhbnN3ZXI6IFwieWVzSGVscE1lXCJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcXVlc3Rpb246IFwiTm9cIixcclxuICAgICAgICAgICAgICAgIGFuc3dlcjogXCJhc2tBbnl0aGluZ1wiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAgeWVzSGVscE1lOiB7XHJcbiAgICAgICAgc2F5czogW1xyXG4gICAgICAgICAgICBCT1RfVVNFUiArIFwiVGhhbmsgeW91ISBXZSBjYW4gcHJvcG9zZSB5b3UgYSBsb2FuIGZvciB0aGUgdG90YWwgdmFsdWUgb2YgeW91IHRyYXZlbC5cIixcclxuICAgICAgICAgICAgXCJGb3IgdGhlIDMgMDAw4oKsIHlvdSBuZWVkLCB3ZSBjYW4gcHJvcG9zZSB5b3UgYSBnbG9iYWwgcmF0ZSBvZiA0JVwiLFxyXG4gICAgICAgICAgICBcIjI1NeKCrCBvZiBtb250aGx5IHJlcGF5bWVudFwiLFxyXG4gICAgICAgICAgICBcImEgdG90YWwgY29zdCBvZiA2NOKCrFwiLFxyXG4gICAgICAgICAgICBcIm9yIGlmIHlvdSBhcmUgcmVhZHkgdG8gcmFpc2UgdGhlIGNoYWxsZW5nZSwgd2UgY2FuIG9mZmVyIHlvdSBhIHRyZW1lbmRvdXMgZGlzY291bnQuXCIsXHJcbiAgICAgICAgICAgIFwiV2hhdCBhcmUgeW91IHVwIHRvIDogXFxcIkRpcmVjdCBvZmZlclxcXCIgb3IgXFxcIkNoYWxsZW5nZVxcXCI/XCJcclxuICAgICAgICBdLFxyXG4gICAgICAgIHJlcGx5OiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHF1ZXN0aW9uOiBcIkRpcmVjdCBvZmZlclwiLFxyXG4gICAgICAgICAgICAgICAgYW5zd2VyOiBcImRpcmVjdE9mZmVyXCJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcXVlc3Rpb246IFwiQ2hhbGxlbmdlXCIsXHJcbiAgICAgICAgICAgICAgICBhbnN3ZXI6IFwiY2hhbGxlbmdlXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICBjaGFsbGVuZ2U6IHtcclxuICAgICAgICBzYXlzOiBbXHJcbiAgICAgICAgICAgIEJPVF9VU0VSICsgXCJHcmVhdCDwn5GMXCIsXHJcbiAgICAgICAgICAgIFwiWW91ciBtaXNzaW9uIHNoYWxsIHlvdSBhY2NlcHQgaXQsIGlzIHRvIHNhdmUgMSAwMDDigqwgaW4gdGhlIG5leHQgNiBtb250aHMuIEFyZSB5b3UgcmVhZHk/XCJcclxuICAgICAgICBdLFxyXG4gICAgICAgIHJlcGx5OiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHF1ZXN0aW9uOiBcIlllc1wiLFxyXG4gICAgICAgICAgICAgICAgYW5zd2VyOiBcInllc0FjY2VwdENoYWxsZW5nZU9mZmVyXCJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcXVlc3Rpb246IFwiTm9cIixcclxuICAgICAgICAgICAgICAgIGFuc3dlcjogXCJhc2tBbnl0aGluZ1wiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAgIHllc0FjY2VwdENoYWxsZW5nZU9mZmVyOiB7XHJcbiAgICAgICAgc2F5czogW1xyXG4gICAgICAgICAgICBCT1RfVVNFUiArXCJDb29sISBJIHNlZSB0aGF0IHlvdSBoYXZlIGFscmVhZHkgMTAw4oKsIGF2YWlsYWJsZSByaWdodCBub3cuIERvIHlvdSB3YW50IG1lIHRvIHNwYXJlIHRoZW0gbm93P1wiXHJcbiAgICAgICAgXSxcclxuICAgICAgICByZXBseTogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBxdWVzdGlvbjogXCJZZXNcIixcclxuICAgICAgICAgICAgICAgIGFuc3dlcjogXCJ5ZXNTYXZlXCJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcXVlc3Rpb246IFwiTm9cIixcclxuICAgICAgICAgICAgICAgIGFuc3dlcjogXCJhc2tBbnl0aGluZ1wiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAgeWVzU2F2ZToge1xyXG4gICAgICAgIHNheXM6IFtcclxuICAgICAgICAgICAgQk9UX1VTRVIgKyBcIk9rIGRvbmUuIEFscmVhZHkgMTAlIG9mIHlvdXIgY2hhbGxlbmdlIGNvbXBsZXRlZC4uLiBDb250aW51ZSB0aGUgZ29vZCBqb2Ig8J+RjVwiLFxyXG4gICAgICAgICAgICBcIklzIHRoZXJlIGFueXRoaW5nIGVsc2UgSSBjYW4gZG8gZm9yIHlvdT9cIlxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgcmVwbHk6IFtdXHJcbiAgICB9LFxyXG4gICAgY29ubmVjdFRvTGl2ZUFnZW50OntcclxuICAgICAgICBzYXlzOiBbXHJcbiAgICAgICAgICAgIEJPVF9VU0VSICsgXCJQbGVhc2Ugd2FpdCDijJsgd2hpbGUgY29ubmVjdGluZyB5b3UgdG8gYSBodW1hbiDwn5OhXCJcclxuICAgICAgICBdLFxyXG4gICAgICAgIHJlcGx5OiBbXVxyXG4gICAgfVxyXG59XHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgc2VuYXJpbzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vbGliL2F4aW9zJyk7IiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG52YXIgc2V0dGxlID0gcmVxdWlyZSgnLi8uLi9jb3JlL3NldHRsZScpO1xudmFyIGJ1aWxkVVJMID0gcmVxdWlyZSgnLi8uLi9oZWxwZXJzL2J1aWxkVVJMJyk7XG52YXIgcGFyc2VIZWFkZXJzID0gcmVxdWlyZSgnLi8uLi9oZWxwZXJzL3BhcnNlSGVhZGVycycpO1xudmFyIGlzVVJMU2FtZU9yaWdpbiA9IHJlcXVpcmUoJy4vLi4vaGVscGVycy9pc1VSTFNhbWVPcmlnaW4nKTtcbnZhciBjcmVhdGVFcnJvciA9IHJlcXVpcmUoJy4uL2NvcmUvY3JlYXRlRXJyb3InKTtcbnZhciBidG9hID0gKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5idG9hICYmIHdpbmRvdy5idG9hLmJpbmQod2luZG93KSkgfHwgcmVxdWlyZSgnLi8uLi9oZWxwZXJzL2J0b2EnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiB4aHJBZGFwdGVyKGNvbmZpZykge1xuICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gZGlzcGF0Y2hYaHJSZXF1ZXN0KHJlc29sdmUsIHJlamVjdCkge1xuICAgIHZhciByZXF1ZXN0RGF0YSA9IGNvbmZpZy5kYXRhO1xuICAgIHZhciByZXF1ZXN0SGVhZGVycyA9IGNvbmZpZy5oZWFkZXJzO1xuXG4gICAgaWYgKHV0aWxzLmlzRm9ybURhdGEocmVxdWVzdERhdGEpKSB7XG4gICAgICBkZWxldGUgcmVxdWVzdEhlYWRlcnNbJ0NvbnRlbnQtVHlwZSddOyAvLyBMZXQgdGhlIGJyb3dzZXIgc2V0IGl0XG4gICAgfVxuXG4gICAgdmFyIHJlcXVlc3QgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcbiAgICB2YXIgbG9hZEV2ZW50ID0gJ29ucmVhZHlzdGF0ZWNoYW5nZSc7XG4gICAgdmFyIHhEb21haW4gPSBmYWxzZTtcblxuICAgIC8vIEZvciBJRSA4LzkgQ09SUyBzdXBwb3J0XG4gICAgLy8gT25seSBzdXBwb3J0cyBQT1NUIGFuZCBHRVQgY2FsbHMgYW5kIGRvZXNuJ3QgcmV0dXJucyB0aGUgcmVzcG9uc2UgaGVhZGVycy5cbiAgICAvLyBET04nVCBkbyB0aGlzIGZvciB0ZXN0aW5nIGIvYyBYTUxIdHRwUmVxdWVzdCBpcyBtb2NrZWQsIG5vdCBYRG9tYWluUmVxdWVzdC5cbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICd0ZXN0JyAmJlxuICAgICAgICB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJlxuICAgICAgICB3aW5kb3cuWERvbWFpblJlcXVlc3QgJiYgISgnd2l0aENyZWRlbnRpYWxzJyBpbiByZXF1ZXN0KSAmJlxuICAgICAgICAhaXNVUkxTYW1lT3JpZ2luKGNvbmZpZy51cmwpKSB7XG4gICAgICByZXF1ZXN0ID0gbmV3IHdpbmRvdy5YRG9tYWluUmVxdWVzdCgpO1xuICAgICAgbG9hZEV2ZW50ID0gJ29ubG9hZCc7XG4gICAgICB4RG9tYWluID0gdHJ1ZTtcbiAgICAgIHJlcXVlc3Qub25wcm9ncmVzcyA9IGZ1bmN0aW9uIGhhbmRsZVByb2dyZXNzKCkge307XG4gICAgICByZXF1ZXN0Lm9udGltZW91dCA9IGZ1bmN0aW9uIGhhbmRsZVRpbWVvdXQoKSB7fTtcbiAgICB9XG5cbiAgICAvLyBIVFRQIGJhc2ljIGF1dGhlbnRpY2F0aW9uXG4gICAgaWYgKGNvbmZpZy5hdXRoKSB7XG4gICAgICB2YXIgdXNlcm5hbWUgPSBjb25maWcuYXV0aC51c2VybmFtZSB8fCAnJztcbiAgICAgIHZhciBwYXNzd29yZCA9IGNvbmZpZy5hdXRoLnBhc3N3b3JkIHx8ICcnO1xuICAgICAgcmVxdWVzdEhlYWRlcnMuQXV0aG9yaXphdGlvbiA9ICdCYXNpYyAnICsgYnRvYSh1c2VybmFtZSArICc6JyArIHBhc3N3b3JkKTtcbiAgICB9XG5cbiAgICByZXF1ZXN0Lm9wZW4oY29uZmlnLm1ldGhvZC50b1VwcGVyQ2FzZSgpLCBidWlsZFVSTChjb25maWcudXJsLCBjb25maWcucGFyYW1zLCBjb25maWcucGFyYW1zU2VyaWFsaXplciksIHRydWUpO1xuXG4gICAgLy8gU2V0IHRoZSByZXF1ZXN0IHRpbWVvdXQgaW4gTVNcbiAgICByZXF1ZXN0LnRpbWVvdXQgPSBjb25maWcudGltZW91dDtcblxuICAgIC8vIExpc3RlbiBmb3IgcmVhZHkgc3RhdGVcbiAgICByZXF1ZXN0W2xvYWRFdmVudF0gPSBmdW5jdGlvbiBoYW5kbGVMb2FkKCkge1xuICAgICAgaWYgKCFyZXF1ZXN0IHx8IChyZXF1ZXN0LnJlYWR5U3RhdGUgIT09IDQgJiYgIXhEb21haW4pKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgLy8gVGhlIHJlcXVlc3QgZXJyb3JlZCBvdXQgYW5kIHdlIGRpZG4ndCBnZXQgYSByZXNwb25zZSwgdGhpcyB3aWxsIGJlXG4gICAgICAvLyBoYW5kbGVkIGJ5IG9uZXJyb3IgaW5zdGVhZFxuICAgICAgLy8gV2l0aCBvbmUgZXhjZXB0aW9uOiByZXF1ZXN0IHRoYXQgdXNpbmcgZmlsZTogcHJvdG9jb2wsIG1vc3QgYnJvd3NlcnNcbiAgICAgIC8vIHdpbGwgcmV0dXJuIHN0YXR1cyBhcyAwIGV2ZW4gdGhvdWdoIGl0J3MgYSBzdWNjZXNzZnVsIHJlcXVlc3RcbiAgICAgIGlmIChyZXF1ZXN0LnN0YXR1cyA9PT0gMCAmJiAhKHJlcXVlc3QucmVzcG9uc2VVUkwgJiYgcmVxdWVzdC5yZXNwb25zZVVSTC5pbmRleE9mKCdmaWxlOicpID09PSAwKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIC8vIFByZXBhcmUgdGhlIHJlc3BvbnNlXG4gICAgICB2YXIgcmVzcG9uc2VIZWFkZXJzID0gJ2dldEFsbFJlc3BvbnNlSGVhZGVycycgaW4gcmVxdWVzdCA/IHBhcnNlSGVhZGVycyhyZXF1ZXN0LmdldEFsbFJlc3BvbnNlSGVhZGVycygpKSA6IG51bGw7XG4gICAgICB2YXIgcmVzcG9uc2VEYXRhID0gIWNvbmZpZy5yZXNwb25zZVR5cGUgfHwgY29uZmlnLnJlc3BvbnNlVHlwZSA9PT0gJ3RleHQnID8gcmVxdWVzdC5yZXNwb25zZVRleHQgOiByZXF1ZXN0LnJlc3BvbnNlO1xuICAgICAgdmFyIHJlc3BvbnNlID0ge1xuICAgICAgICBkYXRhOiByZXNwb25zZURhdGEsXG4gICAgICAgIC8vIElFIHNlbmRzIDEyMjMgaW5zdGVhZCBvZiAyMDQgKGh0dHBzOi8vZ2l0aHViLmNvbS9heGlvcy9heGlvcy9pc3N1ZXMvMjAxKVxuICAgICAgICBzdGF0dXM6IHJlcXVlc3Quc3RhdHVzID09PSAxMjIzID8gMjA0IDogcmVxdWVzdC5zdGF0dXMsXG4gICAgICAgIHN0YXR1c1RleHQ6IHJlcXVlc3Quc3RhdHVzID09PSAxMjIzID8gJ05vIENvbnRlbnQnIDogcmVxdWVzdC5zdGF0dXNUZXh0LFxuICAgICAgICBoZWFkZXJzOiByZXNwb25zZUhlYWRlcnMsXG4gICAgICAgIGNvbmZpZzogY29uZmlnLFxuICAgICAgICByZXF1ZXN0OiByZXF1ZXN0XG4gICAgICB9O1xuXG4gICAgICBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCByZXNwb25zZSk7XG5cbiAgICAgIC8vIENsZWFuIHVwIHJlcXVlc3RcbiAgICAgIHJlcXVlc3QgPSBudWxsO1xuICAgIH07XG5cbiAgICAvLyBIYW5kbGUgbG93IGxldmVsIG5ldHdvcmsgZXJyb3JzXG4gICAgcmVxdWVzdC5vbmVycm9yID0gZnVuY3Rpb24gaGFuZGxlRXJyb3IoKSB7XG4gICAgICAvLyBSZWFsIGVycm9ycyBhcmUgaGlkZGVuIGZyb20gdXMgYnkgdGhlIGJyb3dzZXJcbiAgICAgIC8vIG9uZXJyb3Igc2hvdWxkIG9ubHkgZmlyZSBpZiBpdCdzIGEgbmV0d29yayBlcnJvclxuICAgICAgcmVqZWN0KGNyZWF0ZUVycm9yKCdOZXR3b3JrIEVycm9yJywgY29uZmlnLCBudWxsLCByZXF1ZXN0KSk7XG5cbiAgICAgIC8vIENsZWFuIHVwIHJlcXVlc3RcbiAgICAgIHJlcXVlc3QgPSBudWxsO1xuICAgIH07XG5cbiAgICAvLyBIYW5kbGUgdGltZW91dFxuICAgIHJlcXVlc3Qub250aW1lb3V0ID0gZnVuY3Rpb24gaGFuZGxlVGltZW91dCgpIHtcbiAgICAgIHJlamVjdChjcmVhdGVFcnJvcigndGltZW91dCBvZiAnICsgY29uZmlnLnRpbWVvdXQgKyAnbXMgZXhjZWVkZWQnLCBjb25maWcsICdFQ09OTkFCT1JURUQnLFxuICAgICAgICByZXF1ZXN0KSk7XG5cbiAgICAgIC8vIENsZWFuIHVwIHJlcXVlc3RcbiAgICAgIHJlcXVlc3QgPSBudWxsO1xuICAgIH07XG5cbiAgICAvLyBBZGQgeHNyZiBoZWFkZXJcbiAgICAvLyBUaGlzIGlzIG9ubHkgZG9uZSBpZiBydW5uaW5nIGluIGEgc3RhbmRhcmQgYnJvd3NlciBlbnZpcm9ubWVudC5cbiAgICAvLyBTcGVjaWZpY2FsbHkgbm90IGlmIHdlJ3JlIGluIGEgd2ViIHdvcmtlciwgb3IgcmVhY3QtbmF0aXZlLlxuICAgIGlmICh1dGlscy5pc1N0YW5kYXJkQnJvd3NlckVudigpKSB7XG4gICAgICB2YXIgY29va2llcyA9IHJlcXVpcmUoJy4vLi4vaGVscGVycy9jb29raWVzJyk7XG5cbiAgICAgIC8vIEFkZCB4c3JmIGhlYWRlclxuICAgICAgdmFyIHhzcmZWYWx1ZSA9IChjb25maWcud2l0aENyZWRlbnRpYWxzIHx8IGlzVVJMU2FtZU9yaWdpbihjb25maWcudXJsKSkgJiYgY29uZmlnLnhzcmZDb29raWVOYW1lID9cbiAgICAgICAgICBjb29raWVzLnJlYWQoY29uZmlnLnhzcmZDb29raWVOYW1lKSA6XG4gICAgICAgICAgdW5kZWZpbmVkO1xuXG4gICAgICBpZiAoeHNyZlZhbHVlKSB7XG4gICAgICAgIHJlcXVlc3RIZWFkZXJzW2NvbmZpZy54c3JmSGVhZGVyTmFtZV0gPSB4c3JmVmFsdWU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gQWRkIGhlYWRlcnMgdG8gdGhlIHJlcXVlc3RcbiAgICBpZiAoJ3NldFJlcXVlc3RIZWFkZXInIGluIHJlcXVlc3QpIHtcbiAgICAgIHV0aWxzLmZvckVhY2gocmVxdWVzdEhlYWRlcnMsIGZ1bmN0aW9uIHNldFJlcXVlc3RIZWFkZXIodmFsLCBrZXkpIHtcbiAgICAgICAgaWYgKHR5cGVvZiByZXF1ZXN0RGF0YSA9PT0gJ3VuZGVmaW5lZCcgJiYga2V5LnRvTG93ZXJDYXNlKCkgPT09ICdjb250ZW50LXR5cGUnKSB7XG4gICAgICAgICAgLy8gUmVtb3ZlIENvbnRlbnQtVHlwZSBpZiBkYXRhIGlzIHVuZGVmaW5lZFxuICAgICAgICAgIGRlbGV0ZSByZXF1ZXN0SGVhZGVyc1trZXldO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIE90aGVyd2lzZSBhZGQgaGVhZGVyIHRvIHRoZSByZXF1ZXN0XG4gICAgICAgICAgcmVxdWVzdC5zZXRSZXF1ZXN0SGVhZGVyKGtleSwgdmFsKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gQWRkIHdpdGhDcmVkZW50aWFscyB0byByZXF1ZXN0IGlmIG5lZWRlZFxuICAgIGlmIChjb25maWcud2l0aENyZWRlbnRpYWxzKSB7XG4gICAgICByZXF1ZXN0LndpdGhDcmVkZW50aWFscyA9IHRydWU7XG4gICAgfVxuXG4gICAgLy8gQWRkIHJlc3BvbnNlVHlwZSB0byByZXF1ZXN0IGlmIG5lZWRlZFxuICAgIGlmIChjb25maWcucmVzcG9uc2VUeXBlKSB7XG4gICAgICB0cnkge1xuICAgICAgICByZXF1ZXN0LnJlc3BvbnNlVHlwZSA9IGNvbmZpZy5yZXNwb25zZVR5cGU7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIC8vIEV4cGVjdGVkIERPTUV4Y2VwdGlvbiB0aHJvd24gYnkgYnJvd3NlcnMgbm90IGNvbXBhdGlibGUgWE1MSHR0cFJlcXVlc3QgTGV2ZWwgMi5cbiAgICAgICAgLy8gQnV0LCB0aGlzIGNhbiBiZSBzdXBwcmVzc2VkIGZvciAnanNvbicgdHlwZSBhcyBpdCBjYW4gYmUgcGFyc2VkIGJ5IGRlZmF1bHQgJ3RyYW5zZm9ybVJlc3BvbnNlJyBmdW5jdGlvbi5cbiAgICAgICAgaWYgKGNvbmZpZy5yZXNwb25zZVR5cGUgIT09ICdqc29uJykge1xuICAgICAgICAgIHRocm93IGU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBIYW5kbGUgcHJvZ3Jlc3MgaWYgbmVlZGVkXG4gICAgaWYgKHR5cGVvZiBjb25maWcub25Eb3dubG9hZFByb2dyZXNzID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICByZXF1ZXN0LmFkZEV2ZW50TGlzdGVuZXIoJ3Byb2dyZXNzJywgY29uZmlnLm9uRG93bmxvYWRQcm9ncmVzcyk7XG4gICAgfVxuXG4gICAgLy8gTm90IGFsbCBicm93c2VycyBzdXBwb3J0IHVwbG9hZCBldmVudHNcbiAgICBpZiAodHlwZW9mIGNvbmZpZy5vblVwbG9hZFByb2dyZXNzID09PSAnZnVuY3Rpb24nICYmIHJlcXVlc3QudXBsb2FkKSB7XG4gICAgICByZXF1ZXN0LnVwbG9hZC5hZGRFdmVudExpc3RlbmVyKCdwcm9ncmVzcycsIGNvbmZpZy5vblVwbG9hZFByb2dyZXNzKTtcbiAgICB9XG5cbiAgICBpZiAoY29uZmlnLmNhbmNlbFRva2VuKSB7XG4gICAgICAvLyBIYW5kbGUgY2FuY2VsbGF0aW9uXG4gICAgICBjb25maWcuY2FuY2VsVG9rZW4ucHJvbWlzZS50aGVuKGZ1bmN0aW9uIG9uQ2FuY2VsZWQoY2FuY2VsKSB7XG4gICAgICAgIGlmICghcmVxdWVzdCkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlcXVlc3QuYWJvcnQoKTtcbiAgICAgICAgcmVqZWN0KGNhbmNlbCk7XG4gICAgICAgIC8vIENsZWFuIHVwIHJlcXVlc3RcbiAgICAgICAgcmVxdWVzdCA9IG51bGw7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZiAocmVxdWVzdERhdGEgPT09IHVuZGVmaW5lZCkge1xuICAgICAgcmVxdWVzdERhdGEgPSBudWxsO1xuICAgIH1cblxuICAgIC8vIFNlbmQgdGhlIHJlcXVlc3RcbiAgICByZXF1ZXN0LnNlbmQocmVxdWVzdERhdGEpO1xuICB9KTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMnKTtcbnZhciBiaW5kID0gcmVxdWlyZSgnLi9oZWxwZXJzL2JpbmQnKTtcbnZhciBBeGlvcyA9IHJlcXVpcmUoJy4vY29yZS9BeGlvcycpO1xudmFyIGRlZmF1bHRzID0gcmVxdWlyZSgnLi9kZWZhdWx0cycpO1xuXG4vKipcbiAqIENyZWF0ZSBhbiBpbnN0YW5jZSBvZiBBeGlvc1xuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBkZWZhdWx0Q29uZmlnIFRoZSBkZWZhdWx0IGNvbmZpZyBmb3IgdGhlIGluc3RhbmNlXG4gKiBAcmV0dXJuIHtBeGlvc30gQSBuZXcgaW5zdGFuY2Ugb2YgQXhpb3NcbiAqL1xuZnVuY3Rpb24gY3JlYXRlSW5zdGFuY2UoZGVmYXVsdENvbmZpZykge1xuICB2YXIgY29udGV4dCA9IG5ldyBBeGlvcyhkZWZhdWx0Q29uZmlnKTtcbiAgdmFyIGluc3RhbmNlID0gYmluZChBeGlvcy5wcm90b3R5cGUucmVxdWVzdCwgY29udGV4dCk7XG5cbiAgLy8gQ29weSBheGlvcy5wcm90b3R5cGUgdG8gaW5zdGFuY2VcbiAgdXRpbHMuZXh0ZW5kKGluc3RhbmNlLCBBeGlvcy5wcm90b3R5cGUsIGNvbnRleHQpO1xuXG4gIC8vIENvcHkgY29udGV4dCB0byBpbnN0YW5jZVxuICB1dGlscy5leHRlbmQoaW5zdGFuY2UsIGNvbnRleHQpO1xuXG4gIHJldHVybiBpbnN0YW5jZTtcbn1cblxuLy8gQ3JlYXRlIHRoZSBkZWZhdWx0IGluc3RhbmNlIHRvIGJlIGV4cG9ydGVkXG52YXIgYXhpb3MgPSBjcmVhdGVJbnN0YW5jZShkZWZhdWx0cyk7XG5cbi8vIEV4cG9zZSBBeGlvcyBjbGFzcyB0byBhbGxvdyBjbGFzcyBpbmhlcml0YW5jZVxuYXhpb3MuQXhpb3MgPSBBeGlvcztcblxuLy8gRmFjdG9yeSBmb3IgY3JlYXRpbmcgbmV3IGluc3RhbmNlc1xuYXhpb3MuY3JlYXRlID0gZnVuY3Rpb24gY3JlYXRlKGluc3RhbmNlQ29uZmlnKSB7XG4gIHJldHVybiBjcmVhdGVJbnN0YW5jZSh1dGlscy5tZXJnZShkZWZhdWx0cywgaW5zdGFuY2VDb25maWcpKTtcbn07XG5cbi8vIEV4cG9zZSBDYW5jZWwgJiBDYW5jZWxUb2tlblxuYXhpb3MuQ2FuY2VsID0gcmVxdWlyZSgnLi9jYW5jZWwvQ2FuY2VsJyk7XG5heGlvcy5DYW5jZWxUb2tlbiA9IHJlcXVpcmUoJy4vY2FuY2VsL0NhbmNlbFRva2VuJyk7XG5heGlvcy5pc0NhbmNlbCA9IHJlcXVpcmUoJy4vY2FuY2VsL2lzQ2FuY2VsJyk7XG5cbi8vIEV4cG9zZSBhbGwvc3ByZWFkXG5heGlvcy5hbGwgPSBmdW5jdGlvbiBhbGwocHJvbWlzZXMpIHtcbiAgcmV0dXJuIFByb21pc2UuYWxsKHByb21pc2VzKTtcbn07XG5heGlvcy5zcHJlYWQgPSByZXF1aXJlKCcuL2hlbHBlcnMvc3ByZWFkJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gYXhpb3M7XG5cbi8vIEFsbG93IHVzZSBvZiBkZWZhdWx0IGltcG9ydCBzeW50YXggaW4gVHlwZVNjcmlwdFxubW9kdWxlLmV4cG9ydHMuZGVmYXVsdCA9IGF4aW9zO1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIEEgYENhbmNlbGAgaXMgYW4gb2JqZWN0IHRoYXQgaXMgdGhyb3duIHdoZW4gYW4gb3BlcmF0aW9uIGlzIGNhbmNlbGVkLlxuICpcbiAqIEBjbGFzc1xuICogQHBhcmFtIHtzdHJpbmc9fSBtZXNzYWdlIFRoZSBtZXNzYWdlLlxuICovXG5mdW5jdGlvbiBDYW5jZWwobWVzc2FnZSkge1xuICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlO1xufVxuXG5DYW5jZWwucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gIHJldHVybiAnQ2FuY2VsJyArICh0aGlzLm1lc3NhZ2UgPyAnOiAnICsgdGhpcy5tZXNzYWdlIDogJycpO1xufTtcblxuQ2FuY2VsLnByb3RvdHlwZS5fX0NBTkNFTF9fID0gdHJ1ZTtcblxubW9kdWxlLmV4cG9ydHMgPSBDYW5jZWw7XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciBDYW5jZWwgPSByZXF1aXJlKCcuL0NhbmNlbCcpO1xuXG4vKipcbiAqIEEgYENhbmNlbFRva2VuYCBpcyBhbiBvYmplY3QgdGhhdCBjYW4gYmUgdXNlZCB0byByZXF1ZXN0IGNhbmNlbGxhdGlvbiBvZiBhbiBvcGVyYXRpb24uXG4gKlxuICogQGNsYXNzXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBleGVjdXRvciBUaGUgZXhlY3V0b3IgZnVuY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIENhbmNlbFRva2VuKGV4ZWN1dG9yKSB7XG4gIGlmICh0eXBlb2YgZXhlY3V0b3IgIT09ICdmdW5jdGlvbicpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdleGVjdXRvciBtdXN0IGJlIGEgZnVuY3Rpb24uJyk7XG4gIH1cblxuICB2YXIgcmVzb2x2ZVByb21pc2U7XG4gIHRoaXMucHJvbWlzZSA9IG5ldyBQcm9taXNlKGZ1bmN0aW9uIHByb21pc2VFeGVjdXRvcihyZXNvbHZlKSB7XG4gICAgcmVzb2x2ZVByb21pc2UgPSByZXNvbHZlO1xuICB9KTtcblxuICB2YXIgdG9rZW4gPSB0aGlzO1xuICBleGVjdXRvcihmdW5jdGlvbiBjYW5jZWwobWVzc2FnZSkge1xuICAgIGlmICh0b2tlbi5yZWFzb24pIHtcbiAgICAgIC8vIENhbmNlbGxhdGlvbiBoYXMgYWxyZWFkeSBiZWVuIHJlcXVlc3RlZFxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRva2VuLnJlYXNvbiA9IG5ldyBDYW5jZWwobWVzc2FnZSk7XG4gICAgcmVzb2x2ZVByb21pc2UodG9rZW4ucmVhc29uKTtcbiAgfSk7XG59XG5cbi8qKlxuICogVGhyb3dzIGEgYENhbmNlbGAgaWYgY2FuY2VsbGF0aW9uIGhhcyBiZWVuIHJlcXVlc3RlZC5cbiAqL1xuQ2FuY2VsVG9rZW4ucHJvdG90eXBlLnRocm93SWZSZXF1ZXN0ZWQgPSBmdW5jdGlvbiB0aHJvd0lmUmVxdWVzdGVkKCkge1xuICBpZiAodGhpcy5yZWFzb24pIHtcbiAgICB0aHJvdyB0aGlzLnJlYXNvbjtcbiAgfVxufTtcblxuLyoqXG4gKiBSZXR1cm5zIGFuIG9iamVjdCB0aGF0IGNvbnRhaW5zIGEgbmV3IGBDYW5jZWxUb2tlbmAgYW5kIGEgZnVuY3Rpb24gdGhhdCwgd2hlbiBjYWxsZWQsXG4gKiBjYW5jZWxzIHRoZSBgQ2FuY2VsVG9rZW5gLlxuICovXG5DYW5jZWxUb2tlbi5zb3VyY2UgPSBmdW5jdGlvbiBzb3VyY2UoKSB7XG4gIHZhciBjYW5jZWw7XG4gIHZhciB0b2tlbiA9IG5ldyBDYW5jZWxUb2tlbihmdW5jdGlvbiBleGVjdXRvcihjKSB7XG4gICAgY2FuY2VsID0gYztcbiAgfSk7XG4gIHJldHVybiB7XG4gICAgdG9rZW46IHRva2VuLFxuICAgIGNhbmNlbDogY2FuY2VsXG4gIH07XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IENhbmNlbFRva2VuO1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGlzQ2FuY2VsKHZhbHVlKSB7XG4gIHJldHVybiAhISh2YWx1ZSAmJiB2YWx1ZS5fX0NBTkNFTF9fKTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciBkZWZhdWx0cyA9IHJlcXVpcmUoJy4vLi4vZGVmYXVsdHMnKTtcbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcbnZhciBJbnRlcmNlcHRvck1hbmFnZXIgPSByZXF1aXJlKCcuL0ludGVyY2VwdG9yTWFuYWdlcicpO1xudmFyIGRpc3BhdGNoUmVxdWVzdCA9IHJlcXVpcmUoJy4vZGlzcGF0Y2hSZXF1ZXN0Jyk7XG5cbi8qKlxuICogQ3JlYXRlIGEgbmV3IGluc3RhbmNlIG9mIEF4aW9zXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IGluc3RhbmNlQ29uZmlnIFRoZSBkZWZhdWx0IGNvbmZpZyBmb3IgdGhlIGluc3RhbmNlXG4gKi9cbmZ1bmN0aW9uIEF4aW9zKGluc3RhbmNlQ29uZmlnKSB7XG4gIHRoaXMuZGVmYXVsdHMgPSBpbnN0YW5jZUNvbmZpZztcbiAgdGhpcy5pbnRlcmNlcHRvcnMgPSB7XG4gICAgcmVxdWVzdDogbmV3IEludGVyY2VwdG9yTWFuYWdlcigpLFxuICAgIHJlc3BvbnNlOiBuZXcgSW50ZXJjZXB0b3JNYW5hZ2VyKClcbiAgfTtcbn1cblxuLyoqXG4gKiBEaXNwYXRjaCBhIHJlcXVlc3RcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gY29uZmlnIFRoZSBjb25maWcgc3BlY2lmaWMgZm9yIHRoaXMgcmVxdWVzdCAobWVyZ2VkIHdpdGggdGhpcy5kZWZhdWx0cylcbiAqL1xuQXhpb3MucHJvdG90eXBlLnJlcXVlc3QgPSBmdW5jdGlvbiByZXF1ZXN0KGNvbmZpZykge1xuICAvKmVzbGludCBuby1wYXJhbS1yZWFzc2lnbjowKi9cbiAgLy8gQWxsb3cgZm9yIGF4aW9zKCdleGFtcGxlL3VybCdbLCBjb25maWddKSBhIGxhIGZldGNoIEFQSVxuICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcbiAgICBjb25maWcgPSB1dGlscy5tZXJnZSh7XG4gICAgICB1cmw6IGFyZ3VtZW50c1swXVxuICAgIH0sIGFyZ3VtZW50c1sxXSk7XG4gIH1cblxuICBjb25maWcgPSB1dGlscy5tZXJnZShkZWZhdWx0cywge21ldGhvZDogJ2dldCd9LCB0aGlzLmRlZmF1bHRzLCBjb25maWcpO1xuICBjb25maWcubWV0aG9kID0gY29uZmlnLm1ldGhvZC50b0xvd2VyQ2FzZSgpO1xuXG4gIC8vIEhvb2sgdXAgaW50ZXJjZXB0b3JzIG1pZGRsZXdhcmVcbiAgdmFyIGNoYWluID0gW2Rpc3BhdGNoUmVxdWVzdCwgdW5kZWZpbmVkXTtcbiAgdmFyIHByb21pc2UgPSBQcm9taXNlLnJlc29sdmUoY29uZmlnKTtcblxuICB0aGlzLmludGVyY2VwdG9ycy5yZXF1ZXN0LmZvckVhY2goZnVuY3Rpb24gdW5zaGlmdFJlcXVlc3RJbnRlcmNlcHRvcnMoaW50ZXJjZXB0b3IpIHtcbiAgICBjaGFpbi51bnNoaWZ0KGludGVyY2VwdG9yLmZ1bGZpbGxlZCwgaW50ZXJjZXB0b3IucmVqZWN0ZWQpO1xuICB9KTtcblxuICB0aGlzLmludGVyY2VwdG9ycy5yZXNwb25zZS5mb3JFYWNoKGZ1bmN0aW9uIHB1c2hSZXNwb25zZUludGVyY2VwdG9ycyhpbnRlcmNlcHRvcikge1xuICAgIGNoYWluLnB1c2goaW50ZXJjZXB0b3IuZnVsZmlsbGVkLCBpbnRlcmNlcHRvci5yZWplY3RlZCk7XG4gIH0pO1xuXG4gIHdoaWxlIChjaGFpbi5sZW5ndGgpIHtcbiAgICBwcm9taXNlID0gcHJvbWlzZS50aGVuKGNoYWluLnNoaWZ0KCksIGNoYWluLnNoaWZ0KCkpO1xuICB9XG5cbiAgcmV0dXJuIHByb21pc2U7XG59O1xuXG4vLyBQcm92aWRlIGFsaWFzZXMgZm9yIHN1cHBvcnRlZCByZXF1ZXN0IG1ldGhvZHNcbnV0aWxzLmZvckVhY2goWydkZWxldGUnLCAnZ2V0JywgJ2hlYWQnLCAnb3B0aW9ucyddLCBmdW5jdGlvbiBmb3JFYWNoTWV0aG9kTm9EYXRhKG1ldGhvZCkge1xuICAvKmVzbGludCBmdW5jLW5hbWVzOjAqL1xuICBBeGlvcy5wcm90b3R5cGVbbWV0aG9kXSA9IGZ1bmN0aW9uKHVybCwgY29uZmlnKSB7XG4gICAgcmV0dXJuIHRoaXMucmVxdWVzdCh1dGlscy5tZXJnZShjb25maWcgfHwge30sIHtcbiAgICAgIG1ldGhvZDogbWV0aG9kLFxuICAgICAgdXJsOiB1cmxcbiAgICB9KSk7XG4gIH07XG59KTtcblxudXRpbHMuZm9yRWFjaChbJ3Bvc3QnLCAncHV0JywgJ3BhdGNoJ10sIGZ1bmN0aW9uIGZvckVhY2hNZXRob2RXaXRoRGF0YShtZXRob2QpIHtcbiAgLyplc2xpbnQgZnVuYy1uYW1lczowKi9cbiAgQXhpb3MucHJvdG90eXBlW21ldGhvZF0gPSBmdW5jdGlvbih1cmwsIGRhdGEsIGNvbmZpZykge1xuICAgIHJldHVybiB0aGlzLnJlcXVlc3QodXRpbHMubWVyZ2UoY29uZmlnIHx8IHt9LCB7XG4gICAgICBtZXRob2Q6IG1ldGhvZCxcbiAgICAgIHVybDogdXJsLFxuICAgICAgZGF0YTogZGF0YVxuICAgIH0pKTtcbiAgfTtcbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IEF4aW9zO1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG5cbmZ1bmN0aW9uIEludGVyY2VwdG9yTWFuYWdlcigpIHtcbiAgdGhpcy5oYW5kbGVycyA9IFtdO1xufVxuXG4vKipcbiAqIEFkZCBhIG5ldyBpbnRlcmNlcHRvciB0byB0aGUgc3RhY2tcbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdWxmaWxsZWQgVGhlIGZ1bmN0aW9uIHRvIGhhbmRsZSBgdGhlbmAgZm9yIGEgYFByb21pc2VgXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSByZWplY3RlZCBUaGUgZnVuY3Rpb24gdG8gaGFuZGxlIGByZWplY3RgIGZvciBhIGBQcm9taXNlYFxuICpcbiAqIEByZXR1cm4ge051bWJlcn0gQW4gSUQgdXNlZCB0byByZW1vdmUgaW50ZXJjZXB0b3IgbGF0ZXJcbiAqL1xuSW50ZXJjZXB0b3JNYW5hZ2VyLnByb3RvdHlwZS51c2UgPSBmdW5jdGlvbiB1c2UoZnVsZmlsbGVkLCByZWplY3RlZCkge1xuICB0aGlzLmhhbmRsZXJzLnB1c2goe1xuICAgIGZ1bGZpbGxlZDogZnVsZmlsbGVkLFxuICAgIHJlamVjdGVkOiByZWplY3RlZFxuICB9KTtcbiAgcmV0dXJuIHRoaXMuaGFuZGxlcnMubGVuZ3RoIC0gMTtcbn07XG5cbi8qKlxuICogUmVtb3ZlIGFuIGludGVyY2VwdG9yIGZyb20gdGhlIHN0YWNrXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IGlkIFRoZSBJRCB0aGF0IHdhcyByZXR1cm5lZCBieSBgdXNlYFxuICovXG5JbnRlcmNlcHRvck1hbmFnZXIucHJvdG90eXBlLmVqZWN0ID0gZnVuY3Rpb24gZWplY3QoaWQpIHtcbiAgaWYgKHRoaXMuaGFuZGxlcnNbaWRdKSB7XG4gICAgdGhpcy5oYW5kbGVyc1tpZF0gPSBudWxsO1xuICB9XG59O1xuXG4vKipcbiAqIEl0ZXJhdGUgb3ZlciBhbGwgdGhlIHJlZ2lzdGVyZWQgaW50ZXJjZXB0b3JzXG4gKlxuICogVGhpcyBtZXRob2QgaXMgcGFydGljdWxhcmx5IHVzZWZ1bCBmb3Igc2tpcHBpbmcgb3ZlciBhbnlcbiAqIGludGVyY2VwdG9ycyB0aGF0IG1heSBoYXZlIGJlY29tZSBgbnVsbGAgY2FsbGluZyBgZWplY3RgLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZuIFRoZSBmdW5jdGlvbiB0byBjYWxsIGZvciBlYWNoIGludGVyY2VwdG9yXG4gKi9cbkludGVyY2VwdG9yTWFuYWdlci5wcm90b3R5cGUuZm9yRWFjaCA9IGZ1bmN0aW9uIGZvckVhY2goZm4pIHtcbiAgdXRpbHMuZm9yRWFjaCh0aGlzLmhhbmRsZXJzLCBmdW5jdGlvbiBmb3JFYWNoSGFuZGxlcihoKSB7XG4gICAgaWYgKGggIT09IG51bGwpIHtcbiAgICAgIGZuKGgpO1xuICAgIH1cbiAgfSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEludGVyY2VwdG9yTWFuYWdlcjtcbiIsIid1c2Ugc3RyaWN0JztcblxudmFyIGVuaGFuY2VFcnJvciA9IHJlcXVpcmUoJy4vZW5oYW5jZUVycm9yJyk7XG5cbi8qKlxuICogQ3JlYXRlIGFuIEVycm9yIHdpdGggdGhlIHNwZWNpZmllZCBtZXNzYWdlLCBjb25maWcsIGVycm9yIGNvZGUsIHJlcXVlc3QgYW5kIHJlc3BvbnNlLlxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBtZXNzYWdlIFRoZSBlcnJvciBtZXNzYWdlLlxuICogQHBhcmFtIHtPYmplY3R9IGNvbmZpZyBUaGUgY29uZmlnLlxuICogQHBhcmFtIHtzdHJpbmd9IFtjb2RlXSBUaGUgZXJyb3IgY29kZSAoZm9yIGV4YW1wbGUsICdFQ09OTkFCT1JURUQnKS5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbcmVxdWVzdF0gVGhlIHJlcXVlc3QuXG4gKiBAcGFyYW0ge09iamVjdH0gW3Jlc3BvbnNlXSBUaGUgcmVzcG9uc2UuXG4gKiBAcmV0dXJucyB7RXJyb3J9IFRoZSBjcmVhdGVkIGVycm9yLlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGNyZWF0ZUVycm9yKG1lc3NhZ2UsIGNvbmZpZywgY29kZSwgcmVxdWVzdCwgcmVzcG9uc2UpIHtcbiAgdmFyIGVycm9yID0gbmV3IEVycm9yKG1lc3NhZ2UpO1xuICByZXR1cm4gZW5oYW5jZUVycm9yKGVycm9yLCBjb25maWcsIGNvZGUsIHJlcXVlc3QsIHJlc3BvbnNlKTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcbnZhciB0cmFuc2Zvcm1EYXRhID0gcmVxdWlyZSgnLi90cmFuc2Zvcm1EYXRhJyk7XG52YXIgaXNDYW5jZWwgPSByZXF1aXJlKCcuLi9jYW5jZWwvaXNDYW5jZWwnKTtcbnZhciBkZWZhdWx0cyA9IHJlcXVpcmUoJy4uL2RlZmF1bHRzJyk7XG52YXIgaXNBYnNvbHV0ZVVSTCA9IHJlcXVpcmUoJy4vLi4vaGVscGVycy9pc0Fic29sdXRlVVJMJyk7XG52YXIgY29tYmluZVVSTHMgPSByZXF1aXJlKCcuLy4uL2hlbHBlcnMvY29tYmluZVVSTHMnKTtcblxuLyoqXG4gKiBUaHJvd3MgYSBgQ2FuY2VsYCBpZiBjYW5jZWxsYXRpb24gaGFzIGJlZW4gcmVxdWVzdGVkLlxuICovXG5mdW5jdGlvbiB0aHJvd0lmQ2FuY2VsbGF0aW9uUmVxdWVzdGVkKGNvbmZpZykge1xuICBpZiAoY29uZmlnLmNhbmNlbFRva2VuKSB7XG4gICAgY29uZmlnLmNhbmNlbFRva2VuLnRocm93SWZSZXF1ZXN0ZWQoKTtcbiAgfVxufVxuXG4vKipcbiAqIERpc3BhdGNoIGEgcmVxdWVzdCB0byB0aGUgc2VydmVyIHVzaW5nIHRoZSBjb25maWd1cmVkIGFkYXB0ZXIuXG4gKlxuICogQHBhcmFtIHtvYmplY3R9IGNvbmZpZyBUaGUgY29uZmlnIHRoYXQgaXMgdG8gYmUgdXNlZCBmb3IgdGhlIHJlcXVlc3RcbiAqIEByZXR1cm5zIHtQcm9taXNlfSBUaGUgUHJvbWlzZSB0byBiZSBmdWxmaWxsZWRcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBkaXNwYXRjaFJlcXVlc3QoY29uZmlnKSB7XG4gIHRocm93SWZDYW5jZWxsYXRpb25SZXF1ZXN0ZWQoY29uZmlnKTtcblxuICAvLyBTdXBwb3J0IGJhc2VVUkwgY29uZmlnXG4gIGlmIChjb25maWcuYmFzZVVSTCAmJiAhaXNBYnNvbHV0ZVVSTChjb25maWcudXJsKSkge1xuICAgIGNvbmZpZy51cmwgPSBjb21iaW5lVVJMcyhjb25maWcuYmFzZVVSTCwgY29uZmlnLnVybCk7XG4gIH1cblxuICAvLyBFbnN1cmUgaGVhZGVycyBleGlzdFxuICBjb25maWcuaGVhZGVycyA9IGNvbmZpZy5oZWFkZXJzIHx8IHt9O1xuXG4gIC8vIFRyYW5zZm9ybSByZXF1ZXN0IGRhdGFcbiAgY29uZmlnLmRhdGEgPSB0cmFuc2Zvcm1EYXRhKFxuICAgIGNvbmZpZy5kYXRhLFxuICAgIGNvbmZpZy5oZWFkZXJzLFxuICAgIGNvbmZpZy50cmFuc2Zvcm1SZXF1ZXN0XG4gICk7XG5cbiAgLy8gRmxhdHRlbiBoZWFkZXJzXG4gIGNvbmZpZy5oZWFkZXJzID0gdXRpbHMubWVyZ2UoXG4gICAgY29uZmlnLmhlYWRlcnMuY29tbW9uIHx8IHt9LFxuICAgIGNvbmZpZy5oZWFkZXJzW2NvbmZpZy5tZXRob2RdIHx8IHt9LFxuICAgIGNvbmZpZy5oZWFkZXJzIHx8IHt9XG4gICk7XG5cbiAgdXRpbHMuZm9yRWFjaChcbiAgICBbJ2RlbGV0ZScsICdnZXQnLCAnaGVhZCcsICdwb3N0JywgJ3B1dCcsICdwYXRjaCcsICdjb21tb24nXSxcbiAgICBmdW5jdGlvbiBjbGVhbkhlYWRlckNvbmZpZyhtZXRob2QpIHtcbiAgICAgIGRlbGV0ZSBjb25maWcuaGVhZGVyc1ttZXRob2RdO1xuICAgIH1cbiAgKTtcblxuICB2YXIgYWRhcHRlciA9IGNvbmZpZy5hZGFwdGVyIHx8IGRlZmF1bHRzLmFkYXB0ZXI7XG5cbiAgcmV0dXJuIGFkYXB0ZXIoY29uZmlnKS50aGVuKGZ1bmN0aW9uIG9uQWRhcHRlclJlc29sdXRpb24ocmVzcG9uc2UpIHtcbiAgICB0aHJvd0lmQ2FuY2VsbGF0aW9uUmVxdWVzdGVkKGNvbmZpZyk7XG5cbiAgICAvLyBUcmFuc2Zvcm0gcmVzcG9uc2UgZGF0YVxuICAgIHJlc3BvbnNlLmRhdGEgPSB0cmFuc2Zvcm1EYXRhKFxuICAgICAgcmVzcG9uc2UuZGF0YSxcbiAgICAgIHJlc3BvbnNlLmhlYWRlcnMsXG4gICAgICBjb25maWcudHJhbnNmb3JtUmVzcG9uc2VcbiAgICApO1xuXG4gICAgcmV0dXJuIHJlc3BvbnNlO1xuICB9LCBmdW5jdGlvbiBvbkFkYXB0ZXJSZWplY3Rpb24ocmVhc29uKSB7XG4gICAgaWYgKCFpc0NhbmNlbChyZWFzb24pKSB7XG4gICAgICB0aHJvd0lmQ2FuY2VsbGF0aW9uUmVxdWVzdGVkKGNvbmZpZyk7XG5cbiAgICAgIC8vIFRyYW5zZm9ybSByZXNwb25zZSBkYXRhXG4gICAgICBpZiAocmVhc29uICYmIHJlYXNvbi5yZXNwb25zZSkge1xuICAgICAgICByZWFzb24ucmVzcG9uc2UuZGF0YSA9IHRyYW5zZm9ybURhdGEoXG4gICAgICAgICAgcmVhc29uLnJlc3BvbnNlLmRhdGEsXG4gICAgICAgICAgcmVhc29uLnJlc3BvbnNlLmhlYWRlcnMsXG4gICAgICAgICAgY29uZmlnLnRyYW5zZm9ybVJlc3BvbnNlXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIFByb21pc2UucmVqZWN0KHJlYXNvbik7XG4gIH0pO1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBVcGRhdGUgYW4gRXJyb3Igd2l0aCB0aGUgc3BlY2lmaWVkIGNvbmZpZywgZXJyb3IgY29kZSwgYW5kIHJlc3BvbnNlLlxuICpcbiAqIEBwYXJhbSB7RXJyb3J9IGVycm9yIFRoZSBlcnJvciB0byB1cGRhdGUuXG4gKiBAcGFyYW0ge09iamVjdH0gY29uZmlnIFRoZSBjb25maWcuXG4gKiBAcGFyYW0ge3N0cmluZ30gW2NvZGVdIFRoZSBlcnJvciBjb2RlIChmb3IgZXhhbXBsZSwgJ0VDT05OQUJPUlRFRCcpLlxuICogQHBhcmFtIHtPYmplY3R9IFtyZXF1ZXN0XSBUaGUgcmVxdWVzdC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbcmVzcG9uc2VdIFRoZSByZXNwb25zZS5cbiAqIEByZXR1cm5zIHtFcnJvcn0gVGhlIGVycm9yLlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGVuaGFuY2VFcnJvcihlcnJvciwgY29uZmlnLCBjb2RlLCByZXF1ZXN0LCByZXNwb25zZSkge1xuICBlcnJvci5jb25maWcgPSBjb25maWc7XG4gIGlmIChjb2RlKSB7XG4gICAgZXJyb3IuY29kZSA9IGNvZGU7XG4gIH1cbiAgZXJyb3IucmVxdWVzdCA9IHJlcXVlc3Q7XG4gIGVycm9yLnJlc3BvbnNlID0gcmVzcG9uc2U7XG4gIHJldHVybiBlcnJvcjtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciBjcmVhdGVFcnJvciA9IHJlcXVpcmUoJy4vY3JlYXRlRXJyb3InKTtcblxuLyoqXG4gKiBSZXNvbHZlIG9yIHJlamVjdCBhIFByb21pc2UgYmFzZWQgb24gcmVzcG9uc2Ugc3RhdHVzLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IHJlc29sdmUgQSBmdW5jdGlvbiB0aGF0IHJlc29sdmVzIHRoZSBwcm9taXNlLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gcmVqZWN0IEEgZnVuY3Rpb24gdGhhdCByZWplY3RzIHRoZSBwcm9taXNlLlxuICogQHBhcmFtIHtvYmplY3R9IHJlc3BvbnNlIFRoZSByZXNwb25zZS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCByZXNwb25zZSkge1xuICB2YXIgdmFsaWRhdGVTdGF0dXMgPSByZXNwb25zZS5jb25maWcudmFsaWRhdGVTdGF0dXM7XG4gIC8vIE5vdGU6IHN0YXR1cyBpcyBub3QgZXhwb3NlZCBieSBYRG9tYWluUmVxdWVzdFxuICBpZiAoIXJlc3BvbnNlLnN0YXR1cyB8fCAhdmFsaWRhdGVTdGF0dXMgfHwgdmFsaWRhdGVTdGF0dXMocmVzcG9uc2Uuc3RhdHVzKSkge1xuICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICB9IGVsc2Uge1xuICAgIHJlamVjdChjcmVhdGVFcnJvcihcbiAgICAgICdSZXF1ZXN0IGZhaWxlZCB3aXRoIHN0YXR1cyBjb2RlICcgKyByZXNwb25zZS5zdGF0dXMsXG4gICAgICByZXNwb25zZS5jb25maWcsXG4gICAgICBudWxsLFxuICAgICAgcmVzcG9uc2UucmVxdWVzdCxcbiAgICAgIHJlc3BvbnNlXG4gICAgKSk7XG4gIH1cbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxuLyoqXG4gKiBUcmFuc2Zvcm0gdGhlIGRhdGEgZm9yIGEgcmVxdWVzdCBvciBhIHJlc3BvbnNlXG4gKlxuICogQHBhcmFtIHtPYmplY3R8U3RyaW5nfSBkYXRhIFRoZSBkYXRhIHRvIGJlIHRyYW5zZm9ybWVkXG4gKiBAcGFyYW0ge0FycmF5fSBoZWFkZXJzIFRoZSBoZWFkZXJzIGZvciB0aGUgcmVxdWVzdCBvciByZXNwb25zZVxuICogQHBhcmFtIHtBcnJheXxGdW5jdGlvbn0gZm5zIEEgc2luZ2xlIGZ1bmN0aW9uIG9yIEFycmF5IG9mIGZ1bmN0aW9uc1xuICogQHJldHVybnMgeyp9IFRoZSByZXN1bHRpbmcgdHJhbnNmb3JtZWQgZGF0YVxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHRyYW5zZm9ybURhdGEoZGF0YSwgaGVhZGVycywgZm5zKSB7XG4gIC8qZXNsaW50IG5vLXBhcmFtLXJlYXNzaWduOjAqL1xuICB1dGlscy5mb3JFYWNoKGZucywgZnVuY3Rpb24gdHJhbnNmb3JtKGZuKSB7XG4gICAgZGF0YSA9IGZuKGRhdGEsIGhlYWRlcnMpO1xuICB9KTtcblxuICByZXR1cm4gZGF0YTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMnKTtcbnZhciBub3JtYWxpemVIZWFkZXJOYW1lID0gcmVxdWlyZSgnLi9oZWxwZXJzL25vcm1hbGl6ZUhlYWRlck5hbWUnKTtcblxudmFyIERFRkFVTFRfQ09OVEVOVF9UWVBFID0ge1xuICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCdcbn07XG5cbmZ1bmN0aW9uIHNldENvbnRlbnRUeXBlSWZVbnNldChoZWFkZXJzLCB2YWx1ZSkge1xuICBpZiAoIXV0aWxzLmlzVW5kZWZpbmVkKGhlYWRlcnMpICYmIHV0aWxzLmlzVW5kZWZpbmVkKGhlYWRlcnNbJ0NvbnRlbnQtVHlwZSddKSkge1xuICAgIGhlYWRlcnNbJ0NvbnRlbnQtVHlwZSddID0gdmFsdWU7XG4gIH1cbn1cblxuZnVuY3Rpb24gZ2V0RGVmYXVsdEFkYXB0ZXIoKSB7XG4gIHZhciBhZGFwdGVyO1xuICBpZiAodHlwZW9mIFhNTEh0dHBSZXF1ZXN0ICE9PSAndW5kZWZpbmVkJykge1xuICAgIC8vIEZvciBicm93c2VycyB1c2UgWEhSIGFkYXB0ZXJcbiAgICBhZGFwdGVyID0gcmVxdWlyZSgnLi9hZGFwdGVycy94aHInKTtcbiAgfSBlbHNlIGlmICh0eXBlb2YgcHJvY2VzcyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAvLyBGb3Igbm9kZSB1c2UgSFRUUCBhZGFwdGVyXG4gICAgYWRhcHRlciA9IHJlcXVpcmUoJy4vYWRhcHRlcnMvaHR0cCcpO1xuICB9XG4gIHJldHVybiBhZGFwdGVyO1xufVxuXG52YXIgZGVmYXVsdHMgPSB7XG4gIGFkYXB0ZXI6IGdldERlZmF1bHRBZGFwdGVyKCksXG5cbiAgdHJhbnNmb3JtUmVxdWVzdDogW2Z1bmN0aW9uIHRyYW5zZm9ybVJlcXVlc3QoZGF0YSwgaGVhZGVycykge1xuICAgIG5vcm1hbGl6ZUhlYWRlck5hbWUoaGVhZGVycywgJ0NvbnRlbnQtVHlwZScpO1xuICAgIGlmICh1dGlscy5pc0Zvcm1EYXRhKGRhdGEpIHx8XG4gICAgICB1dGlscy5pc0FycmF5QnVmZmVyKGRhdGEpIHx8XG4gICAgICB1dGlscy5pc0J1ZmZlcihkYXRhKSB8fFxuICAgICAgdXRpbHMuaXNTdHJlYW0oZGF0YSkgfHxcbiAgICAgIHV0aWxzLmlzRmlsZShkYXRhKSB8fFxuICAgICAgdXRpbHMuaXNCbG9iKGRhdGEpXG4gICAgKSB7XG4gICAgICByZXR1cm4gZGF0YTtcbiAgICB9XG4gICAgaWYgKHV0aWxzLmlzQXJyYXlCdWZmZXJWaWV3KGRhdGEpKSB7XG4gICAgICByZXR1cm4gZGF0YS5idWZmZXI7XG4gICAgfVxuICAgIGlmICh1dGlscy5pc1VSTFNlYXJjaFBhcmFtcyhkYXRhKSkge1xuICAgICAgc2V0Q29udGVudFR5cGVJZlVuc2V0KGhlYWRlcnMsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQ7Y2hhcnNldD11dGYtOCcpO1xuICAgICAgcmV0dXJuIGRhdGEudG9TdHJpbmcoKTtcbiAgICB9XG4gICAgaWYgKHV0aWxzLmlzT2JqZWN0KGRhdGEpKSB7XG4gICAgICBzZXRDb250ZW50VHlwZUlmVW5zZXQoaGVhZGVycywgJ2FwcGxpY2F0aW9uL2pzb247Y2hhcnNldD11dGYtOCcpO1xuICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KGRhdGEpO1xuICAgIH1cbiAgICByZXR1cm4gZGF0YTtcbiAgfV0sXG5cbiAgdHJhbnNmb3JtUmVzcG9uc2U6IFtmdW5jdGlvbiB0cmFuc2Zvcm1SZXNwb25zZShkYXRhKSB7XG4gICAgLyplc2xpbnQgbm8tcGFyYW0tcmVhc3NpZ246MCovXG4gICAgaWYgKHR5cGVvZiBkYXRhID09PSAnc3RyaW5nJykge1xuICAgICAgdHJ5IHtcbiAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UoZGF0YSk7XG4gICAgICB9IGNhdGNoIChlKSB7IC8qIElnbm9yZSAqLyB9XG4gICAgfVxuICAgIHJldHVybiBkYXRhO1xuICB9XSxcblxuICAvKipcbiAgICogQSB0aW1lb3V0IGluIG1pbGxpc2Vjb25kcyB0byBhYm9ydCBhIHJlcXVlc3QuIElmIHNldCB0byAwIChkZWZhdWx0KSBhXG4gICAqIHRpbWVvdXQgaXMgbm90IGNyZWF0ZWQuXG4gICAqL1xuICB0aW1lb3V0OiAwLFxuXG4gIHhzcmZDb29raWVOYW1lOiAnWFNSRi1UT0tFTicsXG4gIHhzcmZIZWFkZXJOYW1lOiAnWC1YU1JGLVRPS0VOJyxcblxuICBtYXhDb250ZW50TGVuZ3RoOiAtMSxcblxuICB2YWxpZGF0ZVN0YXR1czogZnVuY3Rpb24gdmFsaWRhdGVTdGF0dXMoc3RhdHVzKSB7XG4gICAgcmV0dXJuIHN0YXR1cyA+PSAyMDAgJiYgc3RhdHVzIDwgMzAwO1xuICB9XG59O1xuXG5kZWZhdWx0cy5oZWFkZXJzID0ge1xuICBjb21tb246IHtcbiAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24sIHRleHQvcGxhaW4sICovKidcbiAgfVxufTtcblxudXRpbHMuZm9yRWFjaChbJ2RlbGV0ZScsICdnZXQnLCAnaGVhZCddLCBmdW5jdGlvbiBmb3JFYWNoTWV0aG9kTm9EYXRhKG1ldGhvZCkge1xuICBkZWZhdWx0cy5oZWFkZXJzW21ldGhvZF0gPSB7fTtcbn0pO1xuXG51dGlscy5mb3JFYWNoKFsncG9zdCcsICdwdXQnLCAncGF0Y2gnXSwgZnVuY3Rpb24gZm9yRWFjaE1ldGhvZFdpdGhEYXRhKG1ldGhvZCkge1xuICBkZWZhdWx0cy5oZWFkZXJzW21ldGhvZF0gPSB1dGlscy5tZXJnZShERUZBVUxUX0NPTlRFTlRfVFlQRSk7XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBkZWZhdWx0cztcbiIsIid1c2Ugc3RyaWN0JztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBiaW5kKGZuLCB0aGlzQXJnKSB7XG4gIHJldHVybiBmdW5jdGlvbiB3cmFwKCkge1xuICAgIHZhciBhcmdzID0gbmV3IEFycmF5KGFyZ3VtZW50cy5sZW5ndGgpO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYXJncy5sZW5ndGg7IGkrKykge1xuICAgICAgYXJnc1tpXSA9IGFyZ3VtZW50c1tpXTtcbiAgICB9XG4gICAgcmV0dXJuIGZuLmFwcGx5KHRoaXNBcmcsIGFyZ3MpO1xuICB9O1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuLy8gYnRvYSBwb2x5ZmlsbCBmb3IgSUU8MTAgY291cnRlc3kgaHR0cHM6Ly9naXRodWIuY29tL2RhdmlkY2hhbWJlcnMvQmFzZTY0LmpzXG5cbnZhciBjaGFycyA9ICdBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWmFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6MDEyMzQ1Njc4OSsvPSc7XG5cbmZ1bmN0aW9uIEUoKSB7XG4gIHRoaXMubWVzc2FnZSA9ICdTdHJpbmcgY29udGFpbnMgYW4gaW52YWxpZCBjaGFyYWN0ZXInO1xufVxuRS5wcm90b3R5cGUgPSBuZXcgRXJyb3I7XG5FLnByb3RvdHlwZS5jb2RlID0gNTtcbkUucHJvdG90eXBlLm5hbWUgPSAnSW52YWxpZENoYXJhY3RlckVycm9yJztcblxuZnVuY3Rpb24gYnRvYShpbnB1dCkge1xuICB2YXIgc3RyID0gU3RyaW5nKGlucHV0KTtcbiAgdmFyIG91dHB1dCA9ICcnO1xuICBmb3IgKFxuICAgIC8vIGluaXRpYWxpemUgcmVzdWx0IGFuZCBjb3VudGVyXG4gICAgdmFyIGJsb2NrLCBjaGFyQ29kZSwgaWR4ID0gMCwgbWFwID0gY2hhcnM7XG4gICAgLy8gaWYgdGhlIG5leHQgc3RyIGluZGV4IGRvZXMgbm90IGV4aXN0OlxuICAgIC8vICAgY2hhbmdlIHRoZSBtYXBwaW5nIHRhYmxlIHRvIFwiPVwiXG4gICAgLy8gICBjaGVjayBpZiBkIGhhcyBubyBmcmFjdGlvbmFsIGRpZ2l0c1xuICAgIHN0ci5jaGFyQXQoaWR4IHwgMCkgfHwgKG1hcCA9ICc9JywgaWR4ICUgMSk7XG4gICAgLy8gXCI4IC0gaWR4ICUgMSAqIDhcIiBnZW5lcmF0ZXMgdGhlIHNlcXVlbmNlIDIsIDQsIDYsIDhcbiAgICBvdXRwdXQgKz0gbWFwLmNoYXJBdCg2MyAmIGJsb2NrID4+IDggLSBpZHggJSAxICogOClcbiAgKSB7XG4gICAgY2hhckNvZGUgPSBzdHIuY2hhckNvZGVBdChpZHggKz0gMyAvIDQpO1xuICAgIGlmIChjaGFyQ29kZSA+IDB4RkYpIHtcbiAgICAgIHRocm93IG5ldyBFKCk7XG4gICAgfVxuICAgIGJsb2NrID0gYmxvY2sgPDwgOCB8IGNoYXJDb2RlO1xuICB9XG4gIHJldHVybiBvdXRwdXQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYnRvYTtcbiIsIid1c2Ugc3RyaWN0JztcblxudmFyIHV0aWxzID0gcmVxdWlyZSgnLi8uLi91dGlscycpO1xuXG5mdW5jdGlvbiBlbmNvZGUodmFsKSB7XG4gIHJldHVybiBlbmNvZGVVUklDb21wb25lbnQodmFsKS5cbiAgICByZXBsYWNlKC8lNDAvZ2ksICdAJykuXG4gICAgcmVwbGFjZSgvJTNBL2dpLCAnOicpLlxuICAgIHJlcGxhY2UoLyUyNC9nLCAnJCcpLlxuICAgIHJlcGxhY2UoLyUyQy9naSwgJywnKS5cbiAgICByZXBsYWNlKC8lMjAvZywgJysnKS5cbiAgICByZXBsYWNlKC8lNUIvZ2ksICdbJykuXG4gICAgcmVwbGFjZSgvJTVEL2dpLCAnXScpO1xufVxuXG4vKipcbiAqIEJ1aWxkIGEgVVJMIGJ5IGFwcGVuZGluZyBwYXJhbXMgdG8gdGhlIGVuZFxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSB1cmwgVGhlIGJhc2Ugb2YgdGhlIHVybCAoZS5nLiwgaHR0cDovL3d3dy5nb29nbGUuY29tKVxuICogQHBhcmFtIHtvYmplY3R9IFtwYXJhbXNdIFRoZSBwYXJhbXMgdG8gYmUgYXBwZW5kZWRcbiAqIEByZXR1cm5zIHtzdHJpbmd9IFRoZSBmb3JtYXR0ZWQgdXJsXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gYnVpbGRVUkwodXJsLCBwYXJhbXMsIHBhcmFtc1NlcmlhbGl6ZXIpIHtcbiAgLyplc2xpbnQgbm8tcGFyYW0tcmVhc3NpZ246MCovXG4gIGlmICghcGFyYW1zKSB7XG4gICAgcmV0dXJuIHVybDtcbiAgfVxuXG4gIHZhciBzZXJpYWxpemVkUGFyYW1zO1xuICBpZiAocGFyYW1zU2VyaWFsaXplcikge1xuICAgIHNlcmlhbGl6ZWRQYXJhbXMgPSBwYXJhbXNTZXJpYWxpemVyKHBhcmFtcyk7XG4gIH0gZWxzZSBpZiAodXRpbHMuaXNVUkxTZWFyY2hQYXJhbXMocGFyYW1zKSkge1xuICAgIHNlcmlhbGl6ZWRQYXJhbXMgPSBwYXJhbXMudG9TdHJpbmcoKTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgcGFydHMgPSBbXTtcblxuICAgIHV0aWxzLmZvckVhY2gocGFyYW1zLCBmdW5jdGlvbiBzZXJpYWxpemUodmFsLCBrZXkpIHtcbiAgICAgIGlmICh2YWwgPT09IG51bGwgfHwgdHlwZW9mIHZhbCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAodXRpbHMuaXNBcnJheSh2YWwpKSB7XG4gICAgICAgIGtleSA9IGtleSArICdbXSc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YWwgPSBbdmFsXTtcbiAgICAgIH1cblxuICAgICAgdXRpbHMuZm9yRWFjaCh2YWwsIGZ1bmN0aW9uIHBhcnNlVmFsdWUodikge1xuICAgICAgICBpZiAodXRpbHMuaXNEYXRlKHYpKSB7XG4gICAgICAgICAgdiA9IHYudG9JU09TdHJpbmcoKTtcbiAgICAgICAgfSBlbHNlIGlmICh1dGlscy5pc09iamVjdCh2KSkge1xuICAgICAgICAgIHYgPSBKU09OLnN0cmluZ2lmeSh2KTtcbiAgICAgICAgfVxuICAgICAgICBwYXJ0cy5wdXNoKGVuY29kZShrZXkpICsgJz0nICsgZW5jb2RlKHYpKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgc2VyaWFsaXplZFBhcmFtcyA9IHBhcnRzLmpvaW4oJyYnKTtcbiAgfVxuXG4gIGlmIChzZXJpYWxpemVkUGFyYW1zKSB7XG4gICAgdXJsICs9ICh1cmwuaW5kZXhPZignPycpID09PSAtMSA/ICc/JyA6ICcmJykgKyBzZXJpYWxpemVkUGFyYW1zO1xuICB9XG5cbiAgcmV0dXJuIHVybDtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ3JlYXRlcyBhIG5ldyBVUkwgYnkgY29tYmluaW5nIHRoZSBzcGVjaWZpZWQgVVJMc1xuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBiYXNlVVJMIFRoZSBiYXNlIFVSTFxuICogQHBhcmFtIHtzdHJpbmd9IHJlbGF0aXZlVVJMIFRoZSByZWxhdGl2ZSBVUkxcbiAqIEByZXR1cm5zIHtzdHJpbmd9IFRoZSBjb21iaW5lZCBVUkxcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBjb21iaW5lVVJMcyhiYXNlVVJMLCByZWxhdGl2ZVVSTCkge1xuICByZXR1cm4gcmVsYXRpdmVVUkxcbiAgICA/IGJhc2VVUkwucmVwbGFjZSgvXFwvKyQvLCAnJykgKyAnLycgKyByZWxhdGl2ZVVSTC5yZXBsYWNlKC9eXFwvKy8sICcnKVxuICAgIDogYmFzZVVSTDtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxubW9kdWxlLmV4cG9ydHMgPSAoXG4gIHV0aWxzLmlzU3RhbmRhcmRCcm93c2VyRW52KCkgP1xuXG4gIC8vIFN0YW5kYXJkIGJyb3dzZXIgZW52cyBzdXBwb3J0IGRvY3VtZW50LmNvb2tpZVxuICAoZnVuY3Rpb24gc3RhbmRhcmRCcm93c2VyRW52KCkge1xuICAgIHJldHVybiB7XG4gICAgICB3cml0ZTogZnVuY3Rpb24gd3JpdGUobmFtZSwgdmFsdWUsIGV4cGlyZXMsIHBhdGgsIGRvbWFpbiwgc2VjdXJlKSB7XG4gICAgICAgIHZhciBjb29raWUgPSBbXTtcbiAgICAgICAgY29va2llLnB1c2gobmFtZSArICc9JyArIGVuY29kZVVSSUNvbXBvbmVudCh2YWx1ZSkpO1xuXG4gICAgICAgIGlmICh1dGlscy5pc051bWJlcihleHBpcmVzKSkge1xuICAgICAgICAgIGNvb2tpZS5wdXNoKCdleHBpcmVzPScgKyBuZXcgRGF0ZShleHBpcmVzKS50b0dNVFN0cmluZygpKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh1dGlscy5pc1N0cmluZyhwYXRoKSkge1xuICAgICAgICAgIGNvb2tpZS5wdXNoKCdwYXRoPScgKyBwYXRoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh1dGlscy5pc1N0cmluZyhkb21haW4pKSB7XG4gICAgICAgICAgY29va2llLnB1c2goJ2RvbWFpbj0nICsgZG9tYWluKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChzZWN1cmUgPT09IHRydWUpIHtcbiAgICAgICAgICBjb29raWUucHVzaCgnc2VjdXJlJyk7XG4gICAgICAgIH1cblxuICAgICAgICBkb2N1bWVudC5jb29raWUgPSBjb29raWUuam9pbignOyAnKTtcbiAgICAgIH0sXG5cbiAgICAgIHJlYWQ6IGZ1bmN0aW9uIHJlYWQobmFtZSkge1xuICAgICAgICB2YXIgbWF0Y2ggPSBkb2N1bWVudC5jb29raWUubWF0Y2gobmV3IFJlZ0V4cCgnKF58O1xcXFxzKikoJyArIG5hbWUgKyAnKT0oW147XSopJykpO1xuICAgICAgICByZXR1cm4gKG1hdGNoID8gZGVjb2RlVVJJQ29tcG9uZW50KG1hdGNoWzNdKSA6IG51bGwpO1xuICAgICAgfSxcblxuICAgICAgcmVtb3ZlOiBmdW5jdGlvbiByZW1vdmUobmFtZSkge1xuICAgICAgICB0aGlzLndyaXRlKG5hbWUsICcnLCBEYXRlLm5vdygpIC0gODY0MDAwMDApO1xuICAgICAgfVxuICAgIH07XG4gIH0pKCkgOlxuXG4gIC8vIE5vbiBzdGFuZGFyZCBicm93c2VyIGVudiAod2ViIHdvcmtlcnMsIHJlYWN0LW5hdGl2ZSkgbGFjayBuZWVkZWQgc3VwcG9ydC5cbiAgKGZ1bmN0aW9uIG5vblN0YW5kYXJkQnJvd3NlckVudigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgd3JpdGU6IGZ1bmN0aW9uIHdyaXRlKCkge30sXG4gICAgICByZWFkOiBmdW5jdGlvbiByZWFkKCkgeyByZXR1cm4gbnVsbDsgfSxcbiAgICAgIHJlbW92ZTogZnVuY3Rpb24gcmVtb3ZlKCkge31cbiAgICB9O1xuICB9KSgpXG4pO1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIERldGVybWluZXMgd2hldGhlciB0aGUgc3BlY2lmaWVkIFVSTCBpcyBhYnNvbHV0ZVxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSB1cmwgVGhlIFVSTCB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB0aGUgc3BlY2lmaWVkIFVSTCBpcyBhYnNvbHV0ZSwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaXNBYnNvbHV0ZVVSTCh1cmwpIHtcbiAgLy8gQSBVUkwgaXMgY29uc2lkZXJlZCBhYnNvbHV0ZSBpZiBpdCBiZWdpbnMgd2l0aCBcIjxzY2hlbWU+Oi8vXCIgb3IgXCIvL1wiIChwcm90b2NvbC1yZWxhdGl2ZSBVUkwpLlxuICAvLyBSRkMgMzk4NiBkZWZpbmVzIHNjaGVtZSBuYW1lIGFzIGEgc2VxdWVuY2Ugb2YgY2hhcmFjdGVycyBiZWdpbm5pbmcgd2l0aCBhIGxldHRlciBhbmQgZm9sbG93ZWRcbiAgLy8gYnkgYW55IGNvbWJpbmF0aW9uIG9mIGxldHRlcnMsIGRpZ2l0cywgcGx1cywgcGVyaW9kLCBvciBoeXBoZW4uXG4gIHJldHVybiAvXihbYS16XVthLXpcXGRcXCtcXC1cXC5dKjopP1xcL1xcLy9pLnRlc3QodXJsKTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxubW9kdWxlLmV4cG9ydHMgPSAoXG4gIHV0aWxzLmlzU3RhbmRhcmRCcm93c2VyRW52KCkgP1xuXG4gIC8vIFN0YW5kYXJkIGJyb3dzZXIgZW52cyBoYXZlIGZ1bGwgc3VwcG9ydCBvZiB0aGUgQVBJcyBuZWVkZWQgdG8gdGVzdFxuICAvLyB3aGV0aGVyIHRoZSByZXF1ZXN0IFVSTCBpcyBvZiB0aGUgc2FtZSBvcmlnaW4gYXMgY3VycmVudCBsb2NhdGlvbi5cbiAgKGZ1bmN0aW9uIHN0YW5kYXJkQnJvd3NlckVudigpIHtcbiAgICB2YXIgbXNpZSA9IC8obXNpZXx0cmlkZW50KS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCk7XG4gICAgdmFyIHVybFBhcnNpbmdOb2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xuICAgIHZhciBvcmlnaW5VUkw7XG5cbiAgICAvKipcbiAgICAqIFBhcnNlIGEgVVJMIHRvIGRpc2NvdmVyIGl0J3MgY29tcG9uZW50c1xuICAgICpcbiAgICAqIEBwYXJhbSB7U3RyaW5nfSB1cmwgVGhlIFVSTCB0byBiZSBwYXJzZWRcbiAgICAqIEByZXR1cm5zIHtPYmplY3R9XG4gICAgKi9cbiAgICBmdW5jdGlvbiByZXNvbHZlVVJMKHVybCkge1xuICAgICAgdmFyIGhyZWYgPSB1cmw7XG5cbiAgICAgIGlmIChtc2llKSB7XG4gICAgICAgIC8vIElFIG5lZWRzIGF0dHJpYnV0ZSBzZXQgdHdpY2UgdG8gbm9ybWFsaXplIHByb3BlcnRpZXNcbiAgICAgICAgdXJsUGFyc2luZ05vZGUuc2V0QXR0cmlidXRlKCdocmVmJywgaHJlZik7XG4gICAgICAgIGhyZWYgPSB1cmxQYXJzaW5nTm9kZS5ocmVmO1xuICAgICAgfVxuXG4gICAgICB1cmxQYXJzaW5nTm9kZS5zZXRBdHRyaWJ1dGUoJ2hyZWYnLCBocmVmKTtcblxuICAgICAgLy8gdXJsUGFyc2luZ05vZGUgcHJvdmlkZXMgdGhlIFVybFV0aWxzIGludGVyZmFjZSAtIGh0dHA6Ly91cmwuc3BlYy53aGF0d2cub3JnLyN1cmx1dGlsc1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgaHJlZjogdXJsUGFyc2luZ05vZGUuaHJlZixcbiAgICAgICAgcHJvdG9jb2w6IHVybFBhcnNpbmdOb2RlLnByb3RvY29sID8gdXJsUGFyc2luZ05vZGUucHJvdG9jb2wucmVwbGFjZSgvOiQvLCAnJykgOiAnJyxcbiAgICAgICAgaG9zdDogdXJsUGFyc2luZ05vZGUuaG9zdCxcbiAgICAgICAgc2VhcmNoOiB1cmxQYXJzaW5nTm9kZS5zZWFyY2ggPyB1cmxQYXJzaW5nTm9kZS5zZWFyY2gucmVwbGFjZSgvXlxcPy8sICcnKSA6ICcnLFxuICAgICAgICBoYXNoOiB1cmxQYXJzaW5nTm9kZS5oYXNoID8gdXJsUGFyc2luZ05vZGUuaGFzaC5yZXBsYWNlKC9eIy8sICcnKSA6ICcnLFxuICAgICAgICBob3N0bmFtZTogdXJsUGFyc2luZ05vZGUuaG9zdG5hbWUsXG4gICAgICAgIHBvcnQ6IHVybFBhcnNpbmdOb2RlLnBvcnQsXG4gICAgICAgIHBhdGhuYW1lOiAodXJsUGFyc2luZ05vZGUucGF0aG5hbWUuY2hhckF0KDApID09PSAnLycpID9cbiAgICAgICAgICAgICAgICAgIHVybFBhcnNpbmdOb2RlLnBhdGhuYW1lIDpcbiAgICAgICAgICAgICAgICAgICcvJyArIHVybFBhcnNpbmdOb2RlLnBhdGhuYW1lXG4gICAgICB9O1xuICAgIH1cblxuICAgIG9yaWdpblVSTCA9IHJlc29sdmVVUkwod2luZG93LmxvY2F0aW9uLmhyZWYpO1xuXG4gICAgLyoqXG4gICAgKiBEZXRlcm1pbmUgaWYgYSBVUkwgc2hhcmVzIHRoZSBzYW1lIG9yaWdpbiBhcyB0aGUgY3VycmVudCBsb2NhdGlvblxuICAgICpcbiAgICAqIEBwYXJhbSB7U3RyaW5nfSByZXF1ZXN0VVJMIFRoZSBVUkwgdG8gdGVzdFxuICAgICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgVVJMIHNoYXJlcyB0aGUgc2FtZSBvcmlnaW4sIG90aGVyd2lzZSBmYWxzZVxuICAgICovXG4gICAgcmV0dXJuIGZ1bmN0aW9uIGlzVVJMU2FtZU9yaWdpbihyZXF1ZXN0VVJMKSB7XG4gICAgICB2YXIgcGFyc2VkID0gKHV0aWxzLmlzU3RyaW5nKHJlcXVlc3RVUkwpKSA/IHJlc29sdmVVUkwocmVxdWVzdFVSTCkgOiByZXF1ZXN0VVJMO1xuICAgICAgcmV0dXJuIChwYXJzZWQucHJvdG9jb2wgPT09IG9yaWdpblVSTC5wcm90b2NvbCAmJlxuICAgICAgICAgICAgcGFyc2VkLmhvc3QgPT09IG9yaWdpblVSTC5ob3N0KTtcbiAgICB9O1xuICB9KSgpIDpcblxuICAvLyBOb24gc3RhbmRhcmQgYnJvd3NlciBlbnZzICh3ZWIgd29ya2VycywgcmVhY3QtbmF0aXZlKSBsYWNrIG5lZWRlZCBzdXBwb3J0LlxuICAoZnVuY3Rpb24gbm9uU3RhbmRhcmRCcm93c2VyRW52KCkge1xuICAgIHJldHVybiBmdW5jdGlvbiBpc1VSTFNhbWVPcmlnaW4oKSB7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9O1xuICB9KSgpXG4pO1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLi91dGlscycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUhlYWRlck5hbWUoaGVhZGVycywgbm9ybWFsaXplZE5hbWUpIHtcbiAgdXRpbHMuZm9yRWFjaChoZWFkZXJzLCBmdW5jdGlvbiBwcm9jZXNzSGVhZGVyKHZhbHVlLCBuYW1lKSB7XG4gICAgaWYgKG5hbWUgIT09IG5vcm1hbGl6ZWROYW1lICYmIG5hbWUudG9VcHBlckNhc2UoKSA9PT0gbm9ybWFsaXplZE5hbWUudG9VcHBlckNhc2UoKSkge1xuICAgICAgaGVhZGVyc1tub3JtYWxpemVkTmFtZV0gPSB2YWx1ZTtcbiAgICAgIGRlbGV0ZSBoZWFkZXJzW25hbWVdO1xuICAgIH1cbiAgfSk7XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG5cbi8vIEhlYWRlcnMgd2hvc2UgZHVwbGljYXRlcyBhcmUgaWdub3JlZCBieSBub2RlXG4vLyBjLmYuIGh0dHBzOi8vbm9kZWpzLm9yZy9hcGkvaHR0cC5odG1sI2h0dHBfbWVzc2FnZV9oZWFkZXJzXG52YXIgaWdub3JlRHVwbGljYXRlT2YgPSBbXG4gICdhZ2UnLCAnYXV0aG9yaXphdGlvbicsICdjb250ZW50LWxlbmd0aCcsICdjb250ZW50LXR5cGUnLCAnZXRhZycsXG4gICdleHBpcmVzJywgJ2Zyb20nLCAnaG9zdCcsICdpZi1tb2RpZmllZC1zaW5jZScsICdpZi11bm1vZGlmaWVkLXNpbmNlJyxcbiAgJ2xhc3QtbW9kaWZpZWQnLCAnbG9jYXRpb24nLCAnbWF4LWZvcndhcmRzJywgJ3Byb3h5LWF1dGhvcml6YXRpb24nLFxuICAncmVmZXJlcicsICdyZXRyeS1hZnRlcicsICd1c2VyLWFnZW50J1xuXTtcblxuLyoqXG4gKiBQYXJzZSBoZWFkZXJzIGludG8gYW4gb2JqZWN0XG4gKlxuICogYGBgXG4gKiBEYXRlOiBXZWQsIDI3IEF1ZyAyMDE0IDA4OjU4OjQ5IEdNVFxuICogQ29udGVudC1UeXBlOiBhcHBsaWNhdGlvbi9qc29uXG4gKiBDb25uZWN0aW9uOiBrZWVwLWFsaXZlXG4gKiBUcmFuc2Zlci1FbmNvZGluZzogY2h1bmtlZFxuICogYGBgXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IGhlYWRlcnMgSGVhZGVycyBuZWVkaW5nIHRvIGJlIHBhcnNlZFxuICogQHJldHVybnMge09iamVjdH0gSGVhZGVycyBwYXJzZWQgaW50byBhbiBvYmplY3RcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBwYXJzZUhlYWRlcnMoaGVhZGVycykge1xuICB2YXIgcGFyc2VkID0ge307XG4gIHZhciBrZXk7XG4gIHZhciB2YWw7XG4gIHZhciBpO1xuXG4gIGlmICghaGVhZGVycykgeyByZXR1cm4gcGFyc2VkOyB9XG5cbiAgdXRpbHMuZm9yRWFjaChoZWFkZXJzLnNwbGl0KCdcXG4nKSwgZnVuY3Rpb24gcGFyc2VyKGxpbmUpIHtcbiAgICBpID0gbGluZS5pbmRleE9mKCc6Jyk7XG4gICAga2V5ID0gdXRpbHMudHJpbShsaW5lLnN1YnN0cigwLCBpKSkudG9Mb3dlckNhc2UoKTtcbiAgICB2YWwgPSB1dGlscy50cmltKGxpbmUuc3Vic3RyKGkgKyAxKSk7XG5cbiAgICBpZiAoa2V5KSB7XG4gICAgICBpZiAocGFyc2VkW2tleV0gJiYgaWdub3JlRHVwbGljYXRlT2YuaW5kZXhPZihrZXkpID49IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgaWYgKGtleSA9PT0gJ3NldC1jb29raWUnKSB7XG4gICAgICAgIHBhcnNlZFtrZXldID0gKHBhcnNlZFtrZXldID8gcGFyc2VkW2tleV0gOiBbXSkuY29uY2F0KFt2YWxdKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHBhcnNlZFtrZXldID0gcGFyc2VkW2tleV0gPyBwYXJzZWRba2V5XSArICcsICcgKyB2YWwgOiB2YWw7XG4gICAgICB9XG4gICAgfVxuICB9KTtcblxuICByZXR1cm4gcGFyc2VkO1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBTeW50YWN0aWMgc3VnYXIgZm9yIGludm9raW5nIGEgZnVuY3Rpb24gYW5kIGV4cGFuZGluZyBhbiBhcnJheSBmb3IgYXJndW1lbnRzLlxuICpcbiAqIENvbW1vbiB1c2UgY2FzZSB3b3VsZCBiZSB0byB1c2UgYEZ1bmN0aW9uLnByb3RvdHlwZS5hcHBseWAuXG4gKlxuICogIGBgYGpzXG4gKiAgZnVuY3Rpb24gZih4LCB5LCB6KSB7fVxuICogIHZhciBhcmdzID0gWzEsIDIsIDNdO1xuICogIGYuYXBwbHkobnVsbCwgYXJncyk7XG4gKiAgYGBgXG4gKlxuICogV2l0aCBgc3ByZWFkYCB0aGlzIGV4YW1wbGUgY2FuIGJlIHJlLXdyaXR0ZW4uXG4gKlxuICogIGBgYGpzXG4gKiAgc3ByZWFkKGZ1bmN0aW9uKHgsIHksIHopIHt9KShbMSwgMiwgM10pO1xuICogIGBgYFxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259XG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gc3ByZWFkKGNhbGxiYWNrKSB7XG4gIHJldHVybiBmdW5jdGlvbiB3cmFwKGFycikge1xuICAgIHJldHVybiBjYWxsYmFjay5hcHBseShudWxsLCBhcnIpO1xuICB9O1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxudmFyIGJpbmQgPSByZXF1aXJlKCcuL2hlbHBlcnMvYmluZCcpO1xudmFyIGlzQnVmZmVyID0gcmVxdWlyZSgnaXMtYnVmZmVyJyk7XG5cbi8qZ2xvYmFsIHRvU3RyaW5nOnRydWUqL1xuXG4vLyB1dGlscyBpcyBhIGxpYnJhcnkgb2YgZ2VuZXJpYyBoZWxwZXIgZnVuY3Rpb25zIG5vbi1zcGVjaWZpYyB0byBheGlvc1xuXG52YXIgdG9TdHJpbmcgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGFuIEFycmF5XG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYW4gQXJyYXksIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0FycmF5KHZhbCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbCh2YWwpID09PSAnW29iamVjdCBBcnJheV0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGFuIEFycmF5QnVmZmVyXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYW4gQXJyYXlCdWZmZXIsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0FycmF5QnVmZmVyKHZhbCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbCh2YWwpID09PSAnW29iamVjdCBBcnJheUJ1ZmZlcl0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgRm9ybURhdGFcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhbiBGb3JtRGF0YSwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzRm9ybURhdGEodmFsKSB7XG4gIHJldHVybiAodHlwZW9mIEZvcm1EYXRhICE9PSAndW5kZWZpbmVkJykgJiYgKHZhbCBpbnN0YW5jZW9mIEZvcm1EYXRhKTtcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIHZpZXcgb24gYW4gQXJyYXlCdWZmZXJcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIHZpZXcgb24gYW4gQXJyYXlCdWZmZXIsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0FycmF5QnVmZmVyVmlldyh2YWwpIHtcbiAgdmFyIHJlc3VsdDtcbiAgaWYgKCh0eXBlb2YgQXJyYXlCdWZmZXIgIT09ICd1bmRlZmluZWQnKSAmJiAoQXJyYXlCdWZmZXIuaXNWaWV3KSkge1xuICAgIHJlc3VsdCA9IEFycmF5QnVmZmVyLmlzVmlldyh2YWwpO1xuICB9IGVsc2Uge1xuICAgIHJlc3VsdCA9ICh2YWwpICYmICh2YWwuYnVmZmVyKSAmJiAodmFsLmJ1ZmZlciBpbnN0YW5jZW9mIEFycmF5QnVmZmVyKTtcbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgU3RyaW5nXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBTdHJpbmcsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc1N0cmluZyh2YWwpIHtcbiAgcmV0dXJuIHR5cGVvZiB2YWwgPT09ICdzdHJpbmcnO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgTnVtYmVyXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBOdW1iZXIsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc051bWJlcih2YWwpIHtcbiAgcmV0dXJuIHR5cGVvZiB2YWwgPT09ICdudW1iZXInO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIHVuZGVmaW5lZFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHRoZSB2YWx1ZSBpcyB1bmRlZmluZWQsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc1VuZGVmaW5lZCh2YWwpIHtcbiAgcmV0dXJuIHR5cGVvZiB2YWwgPT09ICd1bmRlZmluZWQnO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGFuIE9iamVjdFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGFuIE9iamVjdCwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzT2JqZWN0KHZhbCkge1xuICByZXR1cm4gdmFsICE9PSBudWxsICYmIHR5cGVvZiB2YWwgPT09ICdvYmplY3QnO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgRGF0ZVxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgRGF0ZSwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzRGF0ZSh2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgRGF0ZV0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgRmlsZVxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgRmlsZSwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzRmlsZSh2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgRmlsZV0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgQmxvYlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgQmxvYiwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzQmxvYih2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgQmxvYl0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgRnVuY3Rpb25cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIEZ1bmN0aW9uLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNGdW5jdGlvbih2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgRnVuY3Rpb25dJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIFN0cmVhbVxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgU3RyZWFtLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNTdHJlYW0odmFsKSB7XG4gIHJldHVybiBpc09iamVjdCh2YWwpICYmIGlzRnVuY3Rpb24odmFsLnBpcGUpO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgVVJMU2VhcmNoUGFyYW1zIG9iamVjdFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgVVJMU2VhcmNoUGFyYW1zIG9iamVjdCwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzVVJMU2VhcmNoUGFyYW1zKHZhbCkge1xuICByZXR1cm4gdHlwZW9mIFVSTFNlYXJjaFBhcmFtcyAhPT0gJ3VuZGVmaW5lZCcgJiYgdmFsIGluc3RhbmNlb2YgVVJMU2VhcmNoUGFyYW1zO1xufVxuXG4vKipcbiAqIFRyaW0gZXhjZXNzIHdoaXRlc3BhY2Ugb2ZmIHRoZSBiZWdpbm5pbmcgYW5kIGVuZCBvZiBhIHN0cmluZ1xuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBzdHIgVGhlIFN0cmluZyB0byB0cmltXG4gKiBAcmV0dXJucyB7U3RyaW5nfSBUaGUgU3RyaW5nIGZyZWVkIG9mIGV4Y2VzcyB3aGl0ZXNwYWNlXG4gKi9cbmZ1bmN0aW9uIHRyaW0oc3RyKSB7XG4gIHJldHVybiBzdHIucmVwbGFjZSgvXlxccyovLCAnJykucmVwbGFjZSgvXFxzKiQvLCAnJyk7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIHdlJ3JlIHJ1bm5pbmcgaW4gYSBzdGFuZGFyZCBicm93c2VyIGVudmlyb25tZW50XG4gKlxuICogVGhpcyBhbGxvd3MgYXhpb3MgdG8gcnVuIGluIGEgd2ViIHdvcmtlciwgYW5kIHJlYWN0LW5hdGl2ZS5cbiAqIEJvdGggZW52aXJvbm1lbnRzIHN1cHBvcnQgWE1MSHR0cFJlcXVlc3QsIGJ1dCBub3QgZnVsbHkgc3RhbmRhcmQgZ2xvYmFscy5cbiAqXG4gKiB3ZWIgd29ya2VyczpcbiAqICB0eXBlb2Ygd2luZG93IC0+IHVuZGVmaW5lZFxuICogIHR5cGVvZiBkb2N1bWVudCAtPiB1bmRlZmluZWRcbiAqXG4gKiByZWFjdC1uYXRpdmU6XG4gKiAgbmF2aWdhdG9yLnByb2R1Y3QgLT4gJ1JlYWN0TmF0aXZlJ1xuICovXG5mdW5jdGlvbiBpc1N0YW5kYXJkQnJvd3NlckVudigpIHtcbiAgaWYgKHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIG5hdmlnYXRvci5wcm9kdWN0ID09PSAnUmVhY3ROYXRpdmUnKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIHJldHVybiAoXG4gICAgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG4gICk7XG59XG5cbi8qKlxuICogSXRlcmF0ZSBvdmVyIGFuIEFycmF5IG9yIGFuIE9iamVjdCBpbnZva2luZyBhIGZ1bmN0aW9uIGZvciBlYWNoIGl0ZW0uXG4gKlxuICogSWYgYG9iamAgaXMgYW4gQXJyYXkgY2FsbGJhY2sgd2lsbCBiZSBjYWxsZWQgcGFzc2luZ1xuICogdGhlIHZhbHVlLCBpbmRleCwgYW5kIGNvbXBsZXRlIGFycmF5IGZvciBlYWNoIGl0ZW0uXG4gKlxuICogSWYgJ29iaicgaXMgYW4gT2JqZWN0IGNhbGxiYWNrIHdpbGwgYmUgY2FsbGVkIHBhc3NpbmdcbiAqIHRoZSB2YWx1ZSwga2V5LCBhbmQgY29tcGxldGUgb2JqZWN0IGZvciBlYWNoIHByb3BlcnR5LlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fEFycmF5fSBvYmogVGhlIG9iamVjdCB0byBpdGVyYXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmbiBUaGUgY2FsbGJhY2sgdG8gaW52b2tlIGZvciBlYWNoIGl0ZW1cbiAqL1xuZnVuY3Rpb24gZm9yRWFjaChvYmosIGZuKSB7XG4gIC8vIERvbid0IGJvdGhlciBpZiBubyB2YWx1ZSBwcm92aWRlZFxuICBpZiAob2JqID09PSBudWxsIHx8IHR5cGVvZiBvYmogPT09ICd1bmRlZmluZWQnKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLy8gRm9yY2UgYW4gYXJyYXkgaWYgbm90IGFscmVhZHkgc29tZXRoaW5nIGl0ZXJhYmxlXG4gIGlmICh0eXBlb2Ygb2JqICE9PSAnb2JqZWN0Jykge1xuICAgIC8qZXNsaW50IG5vLXBhcmFtLXJlYXNzaWduOjAqL1xuICAgIG9iaiA9IFtvYmpdO1xuICB9XG5cbiAgaWYgKGlzQXJyYXkob2JqKSkge1xuICAgIC8vIEl0ZXJhdGUgb3ZlciBhcnJheSB2YWx1ZXNcbiAgICBmb3IgKHZhciBpID0gMCwgbCA9IG9iai5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgIGZuLmNhbGwobnVsbCwgb2JqW2ldLCBpLCBvYmopO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICAvLyBJdGVyYXRlIG92ZXIgb2JqZWN0IGtleXNcbiAgICBmb3IgKHZhciBrZXkgaW4gb2JqKSB7XG4gICAgICBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwga2V5KSkge1xuICAgICAgICBmbi5jYWxsKG51bGwsIG9ialtrZXldLCBrZXksIG9iaik7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi8qKlxuICogQWNjZXB0cyB2YXJhcmdzIGV4cGVjdGluZyBlYWNoIGFyZ3VtZW50IHRvIGJlIGFuIG9iamVjdCwgdGhlblxuICogaW1tdXRhYmx5IG1lcmdlcyB0aGUgcHJvcGVydGllcyBvZiBlYWNoIG9iamVjdCBhbmQgcmV0dXJucyByZXN1bHQuXG4gKlxuICogV2hlbiBtdWx0aXBsZSBvYmplY3RzIGNvbnRhaW4gdGhlIHNhbWUga2V5IHRoZSBsYXRlciBvYmplY3QgaW5cbiAqIHRoZSBhcmd1bWVudHMgbGlzdCB3aWxsIHRha2UgcHJlY2VkZW5jZS5cbiAqXG4gKiBFeGFtcGxlOlxuICpcbiAqIGBgYGpzXG4gKiB2YXIgcmVzdWx0ID0gbWVyZ2Uoe2ZvbzogMTIzfSwge2ZvbzogNDU2fSk7XG4gKiBjb25zb2xlLmxvZyhyZXN1bHQuZm9vKTsgLy8gb3V0cHV0cyA0NTZcbiAqIGBgYFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmoxIE9iamVjdCB0byBtZXJnZVxuICogQHJldHVybnMge09iamVjdH0gUmVzdWx0IG9mIGFsbCBtZXJnZSBwcm9wZXJ0aWVzXG4gKi9cbmZ1bmN0aW9uIG1lcmdlKC8qIG9iajEsIG9iajIsIG9iajMsIC4uLiAqLykge1xuICB2YXIgcmVzdWx0ID0ge307XG4gIGZ1bmN0aW9uIGFzc2lnblZhbHVlKHZhbCwga2V5KSB7XG4gICAgaWYgKHR5cGVvZiByZXN1bHRba2V5XSA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIHZhbCA9PT0gJ29iamVjdCcpIHtcbiAgICAgIHJlc3VsdFtrZXldID0gbWVyZ2UocmVzdWx0W2tleV0sIHZhbCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3VsdFtrZXldID0gdmFsO1xuICAgIH1cbiAgfVxuXG4gIGZvciAodmFyIGkgPSAwLCBsID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgIGZvckVhY2goYXJndW1lbnRzW2ldLCBhc3NpZ25WYWx1ZSk7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuLyoqXG4gKiBFeHRlbmRzIG9iamVjdCBhIGJ5IG11dGFibHkgYWRkaW5nIHRvIGl0IHRoZSBwcm9wZXJ0aWVzIG9mIG9iamVjdCBiLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBhIFRoZSBvYmplY3QgdG8gYmUgZXh0ZW5kZWRcbiAqIEBwYXJhbSB7T2JqZWN0fSBiIFRoZSBvYmplY3QgdG8gY29weSBwcm9wZXJ0aWVzIGZyb21cbiAqIEBwYXJhbSB7T2JqZWN0fSB0aGlzQXJnIFRoZSBvYmplY3QgdG8gYmluZCBmdW5jdGlvbiB0b1xuICogQHJldHVybiB7T2JqZWN0fSBUaGUgcmVzdWx0aW5nIHZhbHVlIG9mIG9iamVjdCBhXG4gKi9cbmZ1bmN0aW9uIGV4dGVuZChhLCBiLCB0aGlzQXJnKSB7XG4gIGZvckVhY2goYiwgZnVuY3Rpb24gYXNzaWduVmFsdWUodmFsLCBrZXkpIHtcbiAgICBpZiAodGhpc0FyZyAmJiB0eXBlb2YgdmFsID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBhW2tleV0gPSBiaW5kKHZhbCwgdGhpc0FyZyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGFba2V5XSA9IHZhbDtcbiAgICB9XG4gIH0pO1xuICByZXR1cm4gYTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIGlzQXJyYXk6IGlzQXJyYXksXG4gIGlzQXJyYXlCdWZmZXI6IGlzQXJyYXlCdWZmZXIsXG4gIGlzQnVmZmVyOiBpc0J1ZmZlcixcbiAgaXNGb3JtRGF0YTogaXNGb3JtRGF0YSxcbiAgaXNBcnJheUJ1ZmZlclZpZXc6IGlzQXJyYXlCdWZmZXJWaWV3LFxuICBpc1N0cmluZzogaXNTdHJpbmcsXG4gIGlzTnVtYmVyOiBpc051bWJlcixcbiAgaXNPYmplY3Q6IGlzT2JqZWN0LFxuICBpc1VuZGVmaW5lZDogaXNVbmRlZmluZWQsXG4gIGlzRGF0ZTogaXNEYXRlLFxuICBpc0ZpbGU6IGlzRmlsZSxcbiAgaXNCbG9iOiBpc0Jsb2IsXG4gIGlzRnVuY3Rpb246IGlzRnVuY3Rpb24sXG4gIGlzU3RyZWFtOiBpc1N0cmVhbSxcbiAgaXNVUkxTZWFyY2hQYXJhbXM6IGlzVVJMU2VhcmNoUGFyYW1zLFxuICBpc1N0YW5kYXJkQnJvd3NlckVudjogaXNTdGFuZGFyZEJyb3dzZXJFbnYsXG4gIGZvckVhY2g6IGZvckVhY2gsXG4gIG1lcmdlOiBtZXJnZSxcbiAgZXh0ZW5kOiBleHRlbmQsXG4gIHRyaW06IHRyaW1cbn07XG4iLCIvKiFcbiAqIERldGVybWluZSBpZiBhbiBvYmplY3QgaXMgYSBCdWZmZXJcbiAqXG4gKiBAYXV0aG9yICAgRmVyb3NzIEFib3VraGFkaWplaCA8aHR0cHM6Ly9mZXJvc3Mub3JnPlxuICogQGxpY2Vuc2UgIE1JVFxuICovXG5cbi8vIFRoZSBfaXNCdWZmZXIgY2hlY2sgaXMgZm9yIFNhZmFyaSA1LTcgc3VwcG9ydCwgYmVjYXVzZSBpdCdzIG1pc3Npbmdcbi8vIE9iamVjdC5wcm90b3R5cGUuY29uc3RydWN0b3IuIFJlbW92ZSB0aGlzIGV2ZW50dWFsbHlcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKG9iaikge1xuICByZXR1cm4gb2JqICE9IG51bGwgJiYgKGlzQnVmZmVyKG9iaikgfHwgaXNTbG93QnVmZmVyKG9iaikgfHwgISFvYmouX2lzQnVmZmVyKVxufVxuXG5mdW5jdGlvbiBpc0J1ZmZlciAob2JqKSB7XG4gIHJldHVybiAhIW9iai5jb25zdHJ1Y3RvciAmJiB0eXBlb2Ygb2JqLmNvbnN0cnVjdG9yLmlzQnVmZmVyID09PSAnZnVuY3Rpb24nICYmIG9iai5jb25zdHJ1Y3Rvci5pc0J1ZmZlcihvYmopXG59XG5cbi8vIEZvciBOb2RlIHYwLjEwIHN1cHBvcnQuIFJlbW92ZSB0aGlzIGV2ZW50dWFsbHkuXG5mdW5jdGlvbiBpc1Nsb3dCdWZmZXIgKG9iaikge1xuICByZXR1cm4gdHlwZW9mIG9iai5yZWFkRmxvYXRMRSA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2Ygb2JqLnNsaWNlID09PSAnZnVuY3Rpb24nICYmIGlzQnVmZmVyKG9iai5zbGljZSgwLCAwKSlcbn1cbiIsIi8vIHNoaW0gZm9yIHVzaW5nIHByb2Nlc3MgaW4gYnJvd3NlclxudmFyIHByb2Nlc3MgPSBtb2R1bGUuZXhwb3J0cyA9IHt9O1xuXG4vLyBjYWNoZWQgZnJvbSB3aGF0ZXZlciBnbG9iYWwgaXMgcHJlc2VudCBzbyB0aGF0IHRlc3QgcnVubmVycyB0aGF0IHN0dWIgaXRcbi8vIGRvbid0IGJyZWFrIHRoaW5ncy4gIEJ1dCB3ZSBuZWVkIHRvIHdyYXAgaXQgaW4gYSB0cnkgY2F0Y2ggaW4gY2FzZSBpdCBpc1xuLy8gd3JhcHBlZCBpbiBzdHJpY3QgbW9kZSBjb2RlIHdoaWNoIGRvZXNuJ3QgZGVmaW5lIGFueSBnbG9iYWxzLiAgSXQncyBpbnNpZGUgYVxuLy8gZnVuY3Rpb24gYmVjYXVzZSB0cnkvY2F0Y2hlcyBkZW9wdGltaXplIGluIGNlcnRhaW4gZW5naW5lcy5cblxudmFyIGNhY2hlZFNldFRpbWVvdXQ7XG52YXIgY2FjaGVkQ2xlYXJUaW1lb3V0O1xuXG5mdW5jdGlvbiBkZWZhdWx0U2V0VGltb3V0KCkge1xuICAgIHRocm93IG5ldyBFcnJvcignc2V0VGltZW91dCBoYXMgbm90IGJlZW4gZGVmaW5lZCcpO1xufVxuZnVuY3Rpb24gZGVmYXVsdENsZWFyVGltZW91dCAoKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdjbGVhclRpbWVvdXQgaGFzIG5vdCBiZWVuIGRlZmluZWQnKTtcbn1cbihmdW5jdGlvbiAoKSB7XG4gICAgdHJ5IHtcbiAgICAgICAgaWYgKHR5cGVvZiBzZXRUaW1lb3V0ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gc2V0VGltZW91dDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBkZWZhdWx0U2V0VGltb3V0O1xuICAgICAgICB9XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gZGVmYXVsdFNldFRpbW91dDtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgaWYgKHR5cGVvZiBjbGVhclRpbWVvdXQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGNsZWFyVGltZW91dDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGRlZmF1bHRDbGVhclRpbWVvdXQ7XG4gICAgICAgIH1cbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGRlZmF1bHRDbGVhclRpbWVvdXQ7XG4gICAgfVxufSAoKSlcbmZ1bmN0aW9uIHJ1blRpbWVvdXQoZnVuKSB7XG4gICAgaWYgKGNhY2hlZFNldFRpbWVvdXQgPT09IHNldFRpbWVvdXQpIHtcbiAgICAgICAgLy9ub3JtYWwgZW52aXJvbWVudHMgaW4gc2FuZSBzaXR1YXRpb25zXG4gICAgICAgIHJldHVybiBzZXRUaW1lb3V0KGZ1biwgMCk7XG4gICAgfVxuICAgIC8vIGlmIHNldFRpbWVvdXQgd2Fzbid0IGF2YWlsYWJsZSBidXQgd2FzIGxhdHRlciBkZWZpbmVkXG4gICAgaWYgKChjYWNoZWRTZXRUaW1lb3V0ID09PSBkZWZhdWx0U2V0VGltb3V0IHx8ICFjYWNoZWRTZXRUaW1lb3V0KSAmJiBzZXRUaW1lb3V0KSB7XG4gICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBzZXRUaW1lb3V0O1xuICAgICAgICByZXR1cm4gc2V0VGltZW91dChmdW4sIDApO1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICAvLyB3aGVuIHdoZW4gc29tZWJvZHkgaGFzIHNjcmV3ZWQgd2l0aCBzZXRUaW1lb3V0IGJ1dCBubyBJLkUuIG1hZGRuZXNzXG4gICAgICAgIHJldHVybiBjYWNoZWRTZXRUaW1lb3V0KGZ1biwgMCk7XG4gICAgfSBjYXRjaChlKXtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIC8vIFdoZW4gd2UgYXJlIGluIEkuRS4gYnV0IHRoZSBzY3JpcHQgaGFzIGJlZW4gZXZhbGVkIHNvIEkuRS4gZG9lc24ndCB0cnVzdCB0aGUgZ2xvYmFsIG9iamVjdCB3aGVuIGNhbGxlZCBub3JtYWxseVxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZFNldFRpbWVvdXQuY2FsbChudWxsLCBmdW4sIDApO1xuICAgICAgICB9IGNhdGNoKGUpe1xuICAgICAgICAgICAgLy8gc2FtZSBhcyBhYm92ZSBidXQgd2hlbiBpdCdzIGEgdmVyc2lvbiBvZiBJLkUuIHRoYXQgbXVzdCBoYXZlIHRoZSBnbG9iYWwgb2JqZWN0IGZvciAndGhpcycsIGhvcGZ1bGx5IG91ciBjb250ZXh0IGNvcnJlY3Qgb3RoZXJ3aXNlIGl0IHdpbGwgdGhyb3cgYSBnbG9iYWwgZXJyb3JcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRTZXRUaW1lb3V0LmNhbGwodGhpcywgZnVuLCAwKTtcbiAgICAgICAgfVxuICAgIH1cblxuXG59XG5mdW5jdGlvbiBydW5DbGVhclRpbWVvdXQobWFya2VyKSB7XG4gICAgaWYgKGNhY2hlZENsZWFyVGltZW91dCA9PT0gY2xlYXJUaW1lb3V0KSB7XG4gICAgICAgIC8vbm9ybWFsIGVudmlyb21lbnRzIGluIHNhbmUgc2l0dWF0aW9uc1xuICAgICAgICByZXR1cm4gY2xlYXJUaW1lb3V0KG1hcmtlcik7XG4gICAgfVxuICAgIC8vIGlmIGNsZWFyVGltZW91dCB3YXNuJ3QgYXZhaWxhYmxlIGJ1dCB3YXMgbGF0dGVyIGRlZmluZWRcbiAgICBpZiAoKGNhY2hlZENsZWFyVGltZW91dCA9PT0gZGVmYXVsdENsZWFyVGltZW91dCB8fCAhY2FjaGVkQ2xlYXJUaW1lb3V0KSAmJiBjbGVhclRpbWVvdXQpIHtcbiAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gY2xlYXJUaW1lb3V0O1xuICAgICAgICByZXR1cm4gY2xlYXJUaW1lb3V0KG1hcmtlcik7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIC8vIHdoZW4gd2hlbiBzb21lYm9keSBoYXMgc2NyZXdlZCB3aXRoIHNldFRpbWVvdXQgYnV0IG5vIEkuRS4gbWFkZG5lc3NcbiAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH0gY2F0Y2ggKGUpe1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgLy8gV2hlbiB3ZSBhcmUgaW4gSS5FLiBidXQgdGhlIHNjcmlwdCBoYXMgYmVlbiBldmFsZWQgc28gSS5FLiBkb2Vzbid0ICB0cnVzdCB0aGUgZ2xvYmFsIG9iamVjdCB3aGVuIGNhbGxlZCBub3JtYWxseVxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dC5jYWxsKG51bGwsIG1hcmtlcik7XG4gICAgICAgIH0gY2F0Y2ggKGUpe1xuICAgICAgICAgICAgLy8gc2FtZSBhcyBhYm92ZSBidXQgd2hlbiBpdCdzIGEgdmVyc2lvbiBvZiBJLkUuIHRoYXQgbXVzdCBoYXZlIHRoZSBnbG9iYWwgb2JqZWN0IGZvciAndGhpcycsIGhvcGZ1bGx5IG91ciBjb250ZXh0IGNvcnJlY3Qgb3RoZXJ3aXNlIGl0IHdpbGwgdGhyb3cgYSBnbG9iYWwgZXJyb3IuXG4gICAgICAgICAgICAvLyBTb21lIHZlcnNpb25zIG9mIEkuRS4gaGF2ZSBkaWZmZXJlbnQgcnVsZXMgZm9yIGNsZWFyVGltZW91dCB2cyBzZXRUaW1lb3V0XG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0LmNhbGwodGhpcywgbWFya2VyKTtcbiAgICAgICAgfVxuICAgIH1cblxuXG5cbn1cbnZhciBxdWV1ZSA9IFtdO1xudmFyIGRyYWluaW5nID0gZmFsc2U7XG52YXIgY3VycmVudFF1ZXVlO1xudmFyIHF1ZXVlSW5kZXggPSAtMTtcblxuZnVuY3Rpb24gY2xlYW5VcE5leHRUaWNrKCkge1xuICAgIGlmICghZHJhaW5pbmcgfHwgIWN1cnJlbnRRdWV1ZSkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGRyYWluaW5nID0gZmFsc2U7XG4gICAgaWYgKGN1cnJlbnRRdWV1ZS5sZW5ndGgpIHtcbiAgICAgICAgcXVldWUgPSBjdXJyZW50UXVldWUuY29uY2F0KHF1ZXVlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBxdWV1ZUluZGV4ID0gLTE7XG4gICAgfVxuICAgIGlmIChxdWV1ZS5sZW5ndGgpIHtcbiAgICAgICAgZHJhaW5RdWV1ZSgpO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gZHJhaW5RdWV1ZSgpIHtcbiAgICBpZiAoZHJhaW5pbmcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YXIgdGltZW91dCA9IHJ1blRpbWVvdXQoY2xlYW5VcE5leHRUaWNrKTtcbiAgICBkcmFpbmluZyA9IHRydWU7XG5cbiAgICB2YXIgbGVuID0gcXVldWUubGVuZ3RoO1xuICAgIHdoaWxlKGxlbikge1xuICAgICAgICBjdXJyZW50UXVldWUgPSBxdWV1ZTtcbiAgICAgICAgcXVldWUgPSBbXTtcbiAgICAgICAgd2hpbGUgKCsrcXVldWVJbmRleCA8IGxlbikge1xuICAgICAgICAgICAgaWYgKGN1cnJlbnRRdWV1ZSkge1xuICAgICAgICAgICAgICAgIGN1cnJlbnRRdWV1ZVtxdWV1ZUluZGV4XS5ydW4oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBxdWV1ZUluZGV4ID0gLTE7XG4gICAgICAgIGxlbiA9IHF1ZXVlLmxlbmd0aDtcbiAgICB9XG4gICAgY3VycmVudFF1ZXVlID0gbnVsbDtcbiAgICBkcmFpbmluZyA9IGZhbHNlO1xuICAgIHJ1bkNsZWFyVGltZW91dCh0aW1lb3V0KTtcbn1cblxucHJvY2Vzcy5uZXh0VGljayA9IGZ1bmN0aW9uIChmdW4pIHtcbiAgICB2YXIgYXJncyA9IG5ldyBBcnJheShhcmd1bWVudHMubGVuZ3RoIC0gMSk7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XG4gICAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBhcmdzW2kgLSAxXSA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBxdWV1ZS5wdXNoKG5ldyBJdGVtKGZ1biwgYXJncykpO1xuICAgIGlmIChxdWV1ZS5sZW5ndGggPT09IDEgJiYgIWRyYWluaW5nKSB7XG4gICAgICAgIHJ1blRpbWVvdXQoZHJhaW5RdWV1ZSk7XG4gICAgfVxufTtcblxuLy8gdjggbGlrZXMgcHJlZGljdGlibGUgb2JqZWN0c1xuZnVuY3Rpb24gSXRlbShmdW4sIGFycmF5KSB7XG4gICAgdGhpcy5mdW4gPSBmdW47XG4gICAgdGhpcy5hcnJheSA9IGFycmF5O1xufVxuSXRlbS5wcm90b3R5cGUucnVuID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuZnVuLmFwcGx5KG51bGwsIHRoaXMuYXJyYXkpO1xufTtcbnByb2Nlc3MudGl0bGUgPSAnYnJvd3Nlcic7XG5wcm9jZXNzLmJyb3dzZXIgPSB0cnVlO1xucHJvY2Vzcy5lbnYgPSB7fTtcbnByb2Nlc3MuYXJndiA9IFtdO1xucHJvY2Vzcy52ZXJzaW9uID0gJyc7IC8vIGVtcHR5IHN0cmluZyB0byBhdm9pZCByZWdleHAgaXNzdWVzXG5wcm9jZXNzLnZlcnNpb25zID0ge307XG5cbmZ1bmN0aW9uIG5vb3AoKSB7fVxuXG5wcm9jZXNzLm9uID0gbm9vcDtcbnByb2Nlc3MuYWRkTGlzdGVuZXIgPSBub29wO1xucHJvY2Vzcy5vbmNlID0gbm9vcDtcbnByb2Nlc3Mub2ZmID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlTGlzdGVuZXIgPSBub29wO1xucHJvY2Vzcy5yZW1vdmVBbGxMaXN0ZW5lcnMgPSBub29wO1xucHJvY2Vzcy5lbWl0ID0gbm9vcDtcbnByb2Nlc3MucHJlcGVuZExpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3MucHJlcGVuZE9uY2VMaXN0ZW5lciA9IG5vb3A7XG5cbnByb2Nlc3MubGlzdGVuZXJzID0gZnVuY3Rpb24gKG5hbWUpIHsgcmV0dXJuIFtdIH1cblxucHJvY2Vzcy5iaW5kaW5nID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3Byb2Nlc3MuYmluZGluZyBpcyBub3Qgc3VwcG9ydGVkJyk7XG59O1xuXG5wcm9jZXNzLmN3ZCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuICcvJyB9O1xucHJvY2Vzcy5jaGRpciA9IGZ1bmN0aW9uIChkaXIpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3Byb2Nlc3MuY2hkaXIgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcbnByb2Nlc3MudW1hc2sgPSBmdW5jdGlvbigpIHsgcmV0dXJuIDA7IH07XG4iXSwic291cmNlUm9vdCI6IiJ9