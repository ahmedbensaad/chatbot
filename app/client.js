/************************************************************************/
/******* CONVENIENCE METHODS AVAILABLE FOR ES6 BUILD ENVIRONMENTS *******/
/************************************************************************/

// the URL of where you've installed the component; you may need to change this:
import { Bubbles, prepHTML } from "./bubbles.js"
import senario from "./senario.js"

import axios from 'axios'

// this is a convenience script that builds all necessary HTML,
// imports all scripts and stylesheets; your container DIV will
// have a default `id="chat"`;
// you can specify a different ID with:
// `container: "my_chatbox_id"` option
prepHTML({ relative_path: "./" })

localStorage.setItem("chatSession",false);
console.log(localStorage.getItem('chatSession'))
var BOT_USER = "<strong >Bot</strong >: ";

/************************************************************************/
/********************** NGP chatbot Implementation **********************/
/************************************************************************/

var chatWindow = new Bubbles(document.getElementById("chat"), "chatWindow", {
    // the one that we care about is inputCallbackFn()
    // this function returns an object with some data that we can process from user input
    // and understand the context of it
    // this is an example function that matches the text user typed to one of the answer bubbles
    // this function does no natural language processing
    // this is where you may want to connect this script to NLC backend.
    inputCallbackFn: function (o) {
        if(localStorage.getItem("chatSession") == "false"){ // if no live session is on
            // add error conversation block & recall it if no answer matched
            var miss = function () {
                let replies = o.senario[o.standingAnswer].reply 
                console.log(replies);
                if(replies == false)
                    replies = [
                        {
                            question: "Live Agent",
                            answer: "liveagent"
                        }
                    ];
                chatWindow.talk(
                    {
                        "i-dont-get-it": {
                            says: [
                                BOT_USER + "Sorry, I don't get it 😕. type \"<strong>Live Agent</strong>\" if you want to chat with a human 👨"
                            ],
                            reply: replies
                        }
                    },
                    "i-dont-get-it"
                )
            }
            // do this if answer found
            var match = function (key) {
                setTimeout(function () {
                    chatWindow.talk(senario, key) // restart current senario from point found in the answer
                }, 600)
            }
            // sanitize text for search function
            var strip = function (text) {
                return text.toLowerCase().replace(/[\s.,\/#!$%\^&\*;:{}=\-_'"`~()]/g, "")
            }
            // search function
            var found = false
            o.senario[o.standingAnswer].reply.forEach(function (e, i) {
                strip(e.question).includes(strip(o.input)) && o.input.length > 0
                    ? (found = e.answer)
                    : found ? null : (found = false)
            })
            found ? match(found) : miss()
        }else {
            sendMessage(o.input);
        }
    }
}) // done setting up chat-bubble

window.chatWindow = chatWindow



window.liveagent = function () {
    initSession();
    chatWindow.talk(senario, "connectToLiveAgent") // the conversation can be easily restarted from here.
    console.log("Connecting...")
}

function initSession(){
    axios.get("/initSession")
    .then((response) => {
        let data = response.data;
        if(data.response.statusCode == 200){
            console.log('body',data.response.body);
            localStorage.setItem('sessionId',data.response.body.id);
            localStorage.setItem('sessionKey',data.response.body.key);
            localStorage.setItem('sessionAffinityToken',data.response.body.affinityToken);
        }else{
            console.log('error',data.response.error);
        }
    }).then(()=>{
        console.log(localStorage.getItem('sessionId'));
        axios.post("/initChatSession",{
            sessionId: localStorage.getItem('sessionId'),
            sessionKey: localStorage.getItem('sessionKey'),
            sessionAffinityToken: localStorage.getItem('sessionAffinityToken')
        })
        .then((response)=>{
            if(response.data.response.body == "OK"){
                console.log("Live Agent chat session created");
                localStorage.setItem("chatSession",true);
            }
        })
    }).then(()=>{
        pullMessages();
    })
    .catch((error) => {
        console.log(error);
    });
}

/**
 * Send Chat Message
 */
function sendMessage(message){
     axios.post("/sendChatMessage",{
        message: message,
        sessionKey: localStorage.getItem('sessionKey'),
        sessionAffinityToken: localStorage.getItem('sessionAffinityToken')
    })
    .then((response)=>{
        console.log(response.data.response);
        if(response.data.response.body == "OK"){
            console.log("Chat message sent");
        }
    })
}

/**
 * Pulling messages
 */
function pullMessages(){
    axios.get("/getChatMessages",{
        params: {
            sessionKey: localStorage.getItem('sessionKey'),
            sessionAffinityToken: localStorage.getItem('sessionAffinityToken')
        }
    })
    .then((response)=>{
        if(response.error){
            initSession();
            return;
        }
        console.log("getChatMessages",response.data)
        let messages = [];
        try{
            messages = response.data.response.body.messages;
        }catch(e){
            console.log(e.message);
        }
        console.log(messages)
        if(messages && messages.length > 0){
            senario.liveAgentMessage = {says: [], reply: []};
            messages.forEach((item)=>{
                if(item.type == "ChatEstablished"){
                    senario.liveAgentMessage.says.push(BOT_USER + "Live Chat 💬 established with <strong>"+ item.message.name+"</strong>")
                }else if(item.type == "ChatMessage"){
                    senario.liveAgentMessage.says.push("<strong>"+item.message.name+"</strong> : "+item.message.text)
                }else if(item.type == "ChatEnded"){
                    senario.liveAgentMessage.says.push(BOT_USER + "Live Chat 💬 ended ❌")
                    localStorage.setItem("chatSession",false);
                }else if(item.type == "ChatRequestFail"){
                    initSession();
                    return;
                }
            })
            chatWindow.talk(senario, "liveAgentMessage")
        }
        
        pullMessages();
    })
}


// pass JSON to your function and you're done!
chatWindow.talk(senario)