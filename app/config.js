
export const LIVEAGENT_ENDPOINT = "https://d.la1-c1-par.salesforceliveagent.com/chat/rest";
export const LIVEAGENT_API_VERSION = 42;
export const LIVEAGENT_DEPLOYMENT_ID = "5720N0000004VMf";
export const LIVEAGENT_BUTTON_ID = "5730N0000004VbL";
export const SALESFORCE_ORG_ID = "00D0N000001Njc2";