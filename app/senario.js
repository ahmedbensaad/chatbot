var BOT_USER = "<strong >Bot</strong >: ";

let senario = {
    start: {
        says: [ 
                BOT_USER + "Hi", 
                BOT_USER + "Nice project 👍. Do you already know how to finance it?"
            ],
        reply: [
            {
                question: "Yes, Thank you",
                answer: "askAnything"
            },
            {
                question: "Not Really",
                answer: "notReally"
            }
        ]
    },
    askAnything: {
        says: [BOT_USER + "Ok, Is there anything else I can do for you?"],
        reply: []
    },
    notReally: {
        says: [BOT_USER + "Do you want me to help you?"],
        reply: [
            {
                question: "Yes",
                answer: "yesHelpMe"
            },
            {
                question: "No",
                answer: "askAnything"
            }
        ]
    },
    yesHelpMe: {
        says: [
            BOT_USER + "Thank you! We can propose you a loan for the total value of you travel.",
            "For the 3 000€ you need, we can propose you a global rate of 4%",
            "255€ of monthly repayment",
            "a total cost of 64€",
            "or if you are ready to raise the challenge, we can offer you a tremendous discount.",
            "What are you up to : \"Direct offer\" or \"Challenge\"?"
        ],
        reply: [
            {
                question: "Direct offer",
                answer: "directOffer"
            },
            {
                question: "Challenge",
                answer: "challenge"
            }
        ]
    },
    challenge: {
        says: [
            BOT_USER + "Great 👌",
            "Your mission shall you accept it, is to save 1 000€ in the next 6 months. Are you ready?"
        ],
        reply: [
            {
                question: "Yes",
                answer: "yesAcceptChallengeOffer"
            },
            {
                question: "No",
                answer: "askAnything"
            }
        ]
    },
     yesAcceptChallengeOffer: {
        says: [
            BOT_USER +"Cool! I see that you have already 100€ available right now. Do you want me to spare them now?"
        ],
        reply: [
            {
                question: "Yes",
                answer: "yesSave"
            },
            {
                question: "No",
                answer: "askAnything"
            }
        ]
    },
    yesSave: {
        says: [
            BOT_USER + "Ok done. Already 10% of your challenge completed... Continue the good job 👍",
            "Is there anything else I can do for you?"
        ],
        reply: []
    },
    connectToLiveAgent:{
        says: [
            BOT_USER + "Please wait ⌛ while connecting you to a human 📡"
        ],
        reply: []
    }
}


export default senario;