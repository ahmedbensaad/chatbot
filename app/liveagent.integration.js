
import unirest from "unirest"
import {LIVEAGENT_ENDPOINT, LIVEAGENT_API_VERSION, LIVEAGENT_DEPLOYMENT_ID, LIVEAGENT_BUTTON_ID, SALESFORCE_ORG_ID} from "./config.js"

/* live agent integration */
/* guide : https://resources.docs.salesforce.com/212/latest/en-us/sfdc/pdf/live_agent_rest.pdf */

/**
 * init a session 
 */
function initSession(){
    return new Promise((resolve,reject) => {
        unirest.get(LIVEAGENT_ENDPOINT + "/System/SessionId")
        .headers({
            "X-LIVEAGENT-API-VERSION":LIVEAGENT_API_VERSION,
            "X-LIVEAGENT-AFFINITY": "null",
        })
        .end((response)=>{
            resolve(response);
        })
    });
}

/**
 * init a chat session
 */
function initChatSession(data){
    return new Promise((resolve,reject)=>{
        try{
            unirest.post(LIVEAGENT_ENDPOINT + "/Chasitor/ChasitorInit")
            .type('json')
            .headers({
                "X-LIVEAGENT-API-VERSION":LIVEAGENT_API_VERSION,
                "X-LIVEAGENT-AFFINITY": data.sessionAffinityToken,
                "X-LIVEAGENT-SESSION-KEY": data.sessionKey,
                "X-LIVEAGENT-SEQUENCE": 1
            })
            .send({
                organizationId: SALESFORCE_ORG_ID, 
                deploymentId: LIVEAGENT_DEPLOYMENT_ID, 
                buttonId: LIVEAGENT_BUTTON_ID, 
                sessionId: data.sessionId, 
                userAgent: "Chrome 41.0.2228.0", 
                language: "en-US", 
                screenResolution: "1900x1080", 
                visitorName: "Visitor", 
                prechatDetails: [{
                    label:"E-mail Address",
                    displayToAgent :true,
                    value:"info@salesforce.com",
                    entityMaps: [
                        {
                            entityName: "Contact",
                            fieldName: "Email",
                            isFastFillable: false,
                            isAutoQueryable: true,
                            isExactMatchable: true
                        }
                    ],
                    transcriptFields:[]
                }],  
                prechatEntities: [], 
                receiveQueueUpdates: true, 
                isPost: true 
            }).end((initSessionResponse)=>{
                resolve(initSessionResponse);
            });
        }catch(e){
            reject(e);
        }
    });
}

/**
 * send a chat message
 */
function sendChatMessage(data){
    return new Promise((resolve,reject)=>{
        unirest.post(LIVEAGENT_ENDPOINT + "/Chasitor/ChatMessage")
        .type('json')
        .headers({
            "X-LIVEAGENT-API-VERSION": LIVEAGENT_API_VERSION,
            "X-LIVEAGENT-AFFINITY": data.sessionAffinityToken,
            "X-LIVEAGENT-SESSION-KEY": data.sessionKey,
        })
        .send({
            text: data.message
        })
        .end((response)=>{
            resolve(response);
        })
    });
}

/**
 * retrieve a chat message
 */
function getChatMessages(data){
   return new Promise((resolve,reject)=>{
        unirest.get(LIVEAGENT_ENDPOINT + "/System/Messages")
        .headers({
            "X-LIVEAGENT-API-VERSION": LIVEAGENT_API_VERSION,
            "X-LIVEAGENT-AFFINITY": data.sessionAffinityToken,
            "X-LIVEAGENT-SESSION-KEY": data.sessionKey,
        })
        .end((response)=>{
            resolve(response);
        })
    })
}

export default {
    liveAgent:{
        initSession,
        initChatSession,
        sendChatMessage,
        getChatMessages
    }
}
