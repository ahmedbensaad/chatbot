
import express from "express"
import bodyParser from "body-parser"
import cookieParser from "cookie-parser"
import integration from "./liveagent.integration.js"

const app = express();

const jsonParser = bodyParser.json()

app.use(cookieParser());

app.use("/init",(req,res,next) => {
    console.log(req.query)
    console.log(req.cookies)
    if(req.query.username)
        res.cookie("username",req.query.username);
    if(req.query.goalId)
        res.cookie("goalId",req.query.goadId);
    res.redirect('/');
});

// service the public folder
app.use(express.static('public'));

/**
 * init session
 */
app.get("/initSession",(req,res) => {
    integration.liveAgent.initSession()
    .then((response)=>{
        res.send({
            response
        });
    });
})

/**
 * init chat session
 */
app.post("/initChatSession",jsonParser,(req,res) => {
    integration.liveAgent.initChatSession(req.body)
    .then((response)=>{
        res.send({
            response
        });
    }); 
})

/**
 * send chat message
 */
app.post("/sendChatMessage",jsonParser,(req,res) => {
    integration.liveAgent.sendChatMessage(req.body)
    .then((response)=>{
        res.send({
            response
        });
    }); 
})

/**
 * get chat messages
 */
app.get("/getChatMessages",(req,res) => {
    integration.liveAgent.getChatMessages({
        sessionKey: req.query.sessionKey,
        sessionAffinityToken: req.query.sessionAffinityToken
    })
    .then((response)=>{
        res.send({
            response
        });
    }); 
})



app.listen(3000, () => console.log('Example app listening on port 3000!'))